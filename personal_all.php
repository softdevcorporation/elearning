<?php
	require_once('class/phpmydatagrid.class.php');
	include"conexion.php";

	$objGrid = new datagrid('personal_all.php','1');
	
	$objGrid-> conectadb($local,$usu,$pass,$bd);
	$objGrid -> language('es');
	
	$objGrid -> setHeader('personal_all.php', 'js/dgscripts.js', 'css/dgstyle.css', 'js/dgcalendar.js', 'css/dgcalendar.css',  'mmscripts.js');

	$objGrid-> tabla ("empleados");
	
	$objGrid-> orderby("nombres,estado","asc,asc");
	
	$objGrid-> keyfield ("id_empleado");
		
	$objGrid-> searchby("nombres");

	/*********************************** Set filter based on requested parameters ***********************************/
	/********************************* Crear el filtro basandose en los parametros **********************************/
	
	$cedula = getvar("cedula", "cedulaajax");
	$p_apellido = getvar("p_apellido", "p_apellidoajax");
	$pdv = getvar("pdv", "pdvajax");

	 
	$conditions = array();

	if (!empty($cedula)) $conditions[] = sprintf(" (cedula = %s)", $objGrid -> magic_quote($cedula));
	if (!empty($p_apellido)) $conditions[] = sprintf(" (p_apellido = %s)", $objGrid -> magic_quote($p_apellido));
	if (!empty($pdv)) $conditions[] = sprintf(" (pdv = %s)", $objGrid -> magic_quote($pdv));

	$conditions[] = ("estado = 0");
	
	$condition = implode(" AND ", $conditions);

	$objGrid -> sqlstatement('SELECT * from empleados','select count(*) from empleados');

	$objGrid -> where($condition );
	
	$objGrid -> linkparam("&cedulaajax={$cedula}&p_apellidoajax={$p_apellido}&pdvajax={$pdv}");
	
	/****************************** Fin del ejemplo *********************************/
    /********************************* End sample ***********************************/

    $objGrid -> srconClic = "activateSearchBox()";  

	$objGrid-> buttons(FALSE,FALSE,false,FALSE,0);

	$objGrid -> nowindow = true;

	$objGrid -> ButtonWidth = '25';
	$objGrid -> toolbar = true;
     
	$objGrid-> datarows(20);
	
	$objGrid -> TituloGrid("Personal Disponible para capacitaci&oacute;n");

	$objGrid -> FormatColumn("id_franquiciado", "Raz&oacute;n Social", 0, 20, 1, "200", "left","select:select id_franquiciado,razon_social from franquiciado");
	$objGrid -> FormatColumn("id_empleado", "id", 0, 20, 2, "150", "left");
	$objGrid -> FormatColumn("pdv", "P.D.V", 0, 20, 1, "150", "left","select:select id_pdv, pdv from puntos_de_venta");
	$objGrid -> FormatColumn("p_apellido", "Primer Apellido", 0, 30, 1, "100", "left");
	$objGrid -> FormatColumn("s_apellido", "Segundo Apellido", 0, 50, 1, "150", "left");
	$objGrid -> FormatColumn("nombres", "Nombres", 0, 50, 1, "150", "left");
	$objGrid -> FormatColumn("cargo", "Cargo", 0, 50, 1, "150", "left","select:select id_cargo,cargo from cargos where id_cargo");
	$objGrid -> FormatColumn("inscripcion", "Estado", 0, 50, 1, "100", "left","check:Pendiente:Inscrito");
	$objGrid -> FormatColumn("cedula", "Cedula", 0, 50, 1, "100", "left");


	$objGrid-> Form('personal_all', true);
	
	$objGrid-> paginationmode ("links");
	
	$objGrid-> toolbar = true;
	
	$objGrid-> reload = true;
	
	$objGrid-> ajax('silent');
	
	$objGrid-> grid();

	function getvar($var1, $var2, $default=""){
		if ((isset($_GET['top_search'])?$_GET['top_search']:(isset($_POST['top_search'])?$_POST['top_search']:$default))==1){
			return addslashes((isset($_POST[$var1]) and !empty($_POST[$var1]))?$_POST[$var1]:((isset($_GET[$var1]) and !empty($_GET[$var1]))?$_GET[$var1]:$default));
		}else{
			return addslashes((isset($_POST[$var2]) and !empty($_POST[$var2]))?$_POST[$var2]:((isset($_GET[$var2]) and !empty($_GET[$var2]))?$_GET[$var2]:$default));
		}
	}
?>