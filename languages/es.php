<?php
$this->message = array( 
		'cancel' 	  => 'Cancelar',
		'close' 	  => 'Cerrar',
		'save'	 	  => 'Grabar',
		'savenew' 	  => 'Grabar &amp; Nuevo',
		'saving'      => 'Grabando . . .',
		'loading'     => 'Cargando . . .',
		'edit'	 	  => 'Editar',
		'delete'  	  => 'Borrar',
		'deletechk'   => 'Borrar selecci&oacute;n',
		'add'    	  => 'Adicionar',
		'view'    	  => 'Ver',
		'addRecord'	  => 'Adicionar registro',
		'edtRecord'	  => 'Editar registro',
		'chkRecord'	  => 'Ver registro',
		'false'       => 'No',
		'true'        => 'Si',
		'prev'        => 'Anterior',
		'next'        => 'Siguiente',
		'confirm'	  => 'Eliminar registro?',
		'confirms'	  => 'Eliminar registros seleccionados?',
		'search' 	  => 'Buscar', 
		'resetSearch' => 'Cancelar b&uacute;squeda',
		'doublefield' => 'Campo duplicado',
		'norecords'   => 'No se encontraron registros',
		'errcode'     => 'Error de datos [C&oacute;digo de verificaci&oacute;n no v&aacute;lido]',
		'noinsearch'  => 'Campo no disponible para b&uacute;squedas',
		'noformdef'   => 'Para usar la funci&oacute;n "checkable" debe definir un nombre de FORM usando la funcion "Form"',
		'cannotadd'   => 'No puede adicionar registros a esta grilla',
		'cannotedit'  => 'No puede editar registros a esta grilla',
		'cannotsearch'=> 'No puede realizar b&uacute;squedas en esta grilla',
		'cannotdel'   => 'No puede eliminar registros de esta grilla',
		'sqlerror'    => 'Error SQL encontrado en el query:',
		'errormsg'    => 'Mensaje de error:',
		'errorscript' => 'Error SQL en el archivo:',
		'display'     => 'Mostrando registros',
		'to'          => 'a',
		'of'          => 'de',
		'page'        => 'P&aacute;gina',
		'searchby'    => 'Buscar por',
		'qsearch'     => 'B&uacute;squeda r&aacute;pida',
		'calendar'    => 'Calendario',
		'sa'          => 'Ordenar ascendentemente',
		'sd'          => 'Ordenar descendentemente',
		'ssa'         => 'Tambi&eacute;n ordenar ascendentemente',
		'ssd'         => 'Tambi&eacute;n ordenar descendentemente',
		'exportButton'=> 'Exportar',
		'export'      => 'Exportar a ...',
		'sheet'       => 'Exportar a Hoja de trabajo',
		'exporP'      => 'Exportar esta p&aacute;gina',
		'exporA'      => 'Exportar todas las p&aacute;ginas',
		'exporS'      => 'Exportar filas seleccionadas',
		'pdf'         => 'Exportar a PDF',
		'csv'         => 'Exportar a CSV',
		'xml'         => 'Exportar a XML',
		'printer'     => 'Formato para impresi&oacute;n',
		'refresh'     => 'Actualizar',
		'editrows'	  => 'Registros por p&aacute;gina',
		'months'      => 'Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre',
		'days'        => 'Domingo,Lunes,Martes,Mi&eacute;rcoles,Jueves,Viernes,S&aacute;bado',
		'noimage'     => 'Imagen no encontrada!',
		'upload'	  => 'Cargar',
		'onlyimages'  => 'Solo puede cargar im&aacute;genes',
		'selectimage' => 'Por favor seleccione la im&aacute;gen a cargar',
		'norecselect' => 'Debe seleccionar al menos un registro.',
		'failedupl'   => 'No se pudo cargar el archivo',
		'nokey'       => 'Debe definir un campo llave para procesos transaccionales como Ver, Editar, Borrar o Edicion AJAX',
		'ErrorInline' => 'No se pudo completar el proceso de grabaci&oacute;n, por favor intente de nuevo, si el error persiste, comun�quese con el administrador de la p&aacute;gina',
		'arrup'       => 'Subir',
		'arrdn'       => 'Bajar',
		'first'		  => 'Primera p&aacute;gina',
		'last'		  => '&Uacute;ltima p&aacute;gina',
		'xportdetails'=> 'Incluir l&iacute;neas de detalle',
		'columns'	  => 'Columnas',
		'node'	  	  => 'Ver detalles',
		'closenode'	  => 'Ocultar detalles',
		'invalidfile' => 'Tipo de archivo no permitido'
);
?>