<?php
  define('DS', DIRECTORY_SEPARATOR);
  define('ROOT', realpath(dirname(__FILE__)) . DS);
  //define('URL', "http://elearning.sandwichcubano.com/elearning/qbano/");
  define('URL', "http://localhost/elearning/qbano/");
  // echo "El valor de DS es : " . DS . "<br>";
  // echo "El valor de __FILE__ es : " . __FILE__ . "<br>";
  // echo "El valor de dirname(__FILE__) es : " . dirname(__FILE__) . "<br>";
  // echo "El valor de realpath(dirname(__FILE__)) es : " . realpath(dirname(__FILE__)) . "<br>";
  require_once "Config/autoload.php";

  Config\autoload::run();
  require_once "Views/templates/template.php";
  // $est = new Models\Estudiante()
  // $est->set("id", 2);
  // $datos = $est->view();
  // echo $datos['nombre'];
  Config\enrutador::run(new Config\Request);
  //print_r($_GET['url']);
?>
