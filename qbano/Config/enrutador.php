<?php namespace Config;
class enrutador{

  public static function run(Request $request){
    $ctrl=$request->getControlador();
    $controlador = $request->getControlador() . "Controller";
    if($ctrl=='administrativo' || $ctrl=='estudiante' || $ctrl=='entrenador' || $ctrl=='franquiciado'){
      $controlador = "cursos" . "Controller";
    }
    //print($ctrl);
    
    $ruta = ROOT . "Controllers" . DS . $controlador . ".php";
    $metodo  = $request->getMetodo();
    if($metodo=='vistaadmin' || $metodo=='verfranquiciado'){
      $metodo='ver';
    }
    $argumento = $request->getArgumento();

    if(is_readable($ruta)){
      require_once $ruta;
      $mostrar =  "Controllers\\" . $controlador;
      $controlador = new $mostrar;
      if(!isset($argumento)){
        $datos = call_user_func(array($controlador, $metodo));
      }else{
        $datos = call_user_func_array(array($controlador, $metodo),$argumento);
      }
    }
    //cargar la Vista
    $ruta = ROOT ."Views" . DS . $request->getControlador() .DS . $request->getMetodo() . ".php";
    if(is_readable($ruta)){
      require_once $ruta;
    }else{
      print "NO se encontro la ruta";
    }
  }
}

?>
