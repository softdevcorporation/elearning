<?php namespace Config;
class Request{
  //atributos
  private $controlador;
  private $metodo;
  private $argumento;

  //metodos

  public function __construct(){
    if(isset($_GET['url'])){//isset define si hay algo si existe algo en la url lo tome
      $ruta = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_URL);
      $ruta = explode("/", $ruta);//explode nos permite coger una cadena de texto y segmentarla
      $ruta = array_filter($ruta);
      $this->controlador = strtolower(array_shift($ruta));//strtolower sirve para convertir la cadena en minuscula
      $this->metodo = strtolower(array_shift($ruta));

      if(!$this->metodo){
        $this->metodo = "index";
      }
      $this->argumento = $ruta;
    }else{
      $this->controlador = "cuentas";
      $this->metodo = "index";
    }
    }
    public function getControlador(){ //se hace la function porque lo atributos son privados para acceder a ellos los necesitamos
      return $this->controlador;
    }
    public function getMetodo(){
      return $this->metodo;
    }
    public function getArgumento(){
      return $this->argumento;
    }
}

?>
