<?php 
require('../Models/Conexion.php');
/**
* URL - es la del raiz del servidor
**/
define('URL', "http://localhost/qbano/");

      $nombre=$_POST['nombreCurso'];
      $descripcion=$_POST['descripcion'];
      $isCertificado=$_POST['certifica'];
      $fechaInicio=$_POST['fechainicio'];
      $fechaTerminacion=$_POST['fechafin'];
      $fechaPublicacion=$_POST['fechapublicacion'];
      $idTipoCurso=$_POST['tipoCurso'];
      $nombreModulo=$_POST['nombreModulo'];
      $nombreTema=$_POST['nombreTema'];
      $sendTo=json_decode(stripslashes($_POST['sendTo']));
      $responsesQuestion=json_decode(stripslashes($_POST['responsesQuestion']));
      $inputFileImage=$_FILES['inputFileImage'];
      $filedraft=$_FILES['filedraft'];
      $fileguiaformato=$_FILES['fileguiaformato'];
      $fileevaluacion=$_FILES['fileevaluacion'];
      $filetextoayuda=$_FILES['filetextoayuda'];
      $listaDiapositivas=json_decode($_POST['listaDiapositivas']);
      // print_r ($inputFileImage);
      // print_r ($filedraft);
      // print_r ($fileguiaformato);
      // print_r ($fileevaluacion);
      // print_r ($filetextoayuda);
      echo 'hla php';
      // echo $listaDiapositivas;
      // print_r ($listaDiapositivas[0]->diapositiva);
      //zecho '<script>alert("hola mundoo")</script>';
      

    $conn = new Models\Conexion();

    

    
    $sql = "insert into cursos(nombre, descripcion, is_certificated, fecha_inicio, fecha_terminacion, fecha_publicacion, id_tipo_curso) 
            values('{$nombre}','{$descripcion}',{$isCertificado},'{$fechaInicio}','{$fechaTerminacion}','{$fechaPublicacion}', {$idTipoCurso})";
    
    if($conn->consultaSimple($sql)){
            $idCurso=getUltimoCurso($conn);
            for($i=0;$i<count($sendTo);$i++){
              $sql = "insert into cursos_has_tipo_usuarios_cursos(id_curso, id_tipo_usuario_curso) 
                values({$idCurso},{$sendTo[$i]})";
              $conn->consultaSimple($sql);
            }            
            $sql = "insert into modulos(nombre, id_curso)
            values('{$nombreModulo}', {$idCurso})";
            if($conn->consultaSimple($sql)){
              if(guardarImagenAvatar($idCurso, $inputFileImage, $conn)){
                if(guardarDocumentoCurso($idCurso, $filedraft, 0,$conn)){
                  if(guardarDocumentoCurso($idCurso, $fileguiaformato, 1,$conn)){
                    if(guardarDocumentoCurso($idCurso, $fileevaluacion, 2,$conn)){
                      if(guardarDocumentoCurso($idCurso, $filetextoayuda, 3,$conn)){
                    
                      }
                    }
                  }
                }
              }        

                    $idModulo=getUltimoModulo($conn);
                    $sql = "insert into temas(nombre, id_modulo) 
                    values('{$nombreTema}','{$idModulo}')";
                if($conn->consultaSimple($sql)){
                  $idTema=getUltimoTema($conn);              
                  $sql = "insert into evaluaciones(nombre, id_tema)
                  values('{$nombreTema}',{$idTema})";
                  if($conn->consultaSimple($sql) && guardarDiapositivas($listaDiapositivas,$idTema,$conn )){                    
                      $idEvaluacion=getUltimaEvaluacion($conn,$idTema);                     
                      for($i=0;$i<count($responsesQuestion);$i++){
                                //  echo 'cont '.$i;                
                        $strpregunta=$responsesQuestion[$i]->textQuestion;
                        $strtipopregunta=$responsesQuestion[$i]->typeQuestion;
                        // echo '<script>alert("hola miguel")</script>'.$strpregunta.$strtipopregunta;
                        $sql = "insert into preguntas(descripcion, id_tipo_pregunta, id_evaluacion)
                        values('{$strpregunta}',{$strtipopregunta},{$idEvaluacion})";
                        if($conn->consultaSimple($sql)){
                           $idPregunta=getUltimaPregunta($conn, $idEvaluacion);  
                          //  echo '$idEvaluacion '.$idEvaluacion;
                          //  echo 'idPregunta '.$idPregunta;
                           $responsesQuestionList=$responsesQuestion[$i]->responsesQuestionList;
                          // print_r ($responsesQuestionList);
                          if($strtipopregunta==1){
                              $sql = "insert into repuestas_preguntas(isVerdadera, id_pregunta)
                              values('{$responsesQuestionList}',{$idPregunta})";
                              $conn->consultaSimple($sql);
                          }else if($strtipopregunta==2){
                           for($j=0;$j<count($responsesQuestionList);$j++){
                              $resp=$responsesQuestionList[$j]->responsesQuestion;
                              $valueResp=$responsesQuestionList[$j]->valueResponsesQuestion;
                              //  print_r ($idPregunta);
                              $sql = "insert into repuestas_preguntas(descripcion, id_pregunta, isVerdadera)
                              values('{$resp}',{$idPregunta},{$valueResp})";
                              $conn->consultaSimple($sql);
                             }  
   
                           }
                        }
                      }
                  }
                }
            }
    }



  function getUltimoCurso($conn){
    $sql = "select * from cursos order by id_curso desc limit 1";
    $datos = $conn->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row['id_curso'];
  }

  function getUltimoModulo($conn){
    $sql = "select * from modulos order by id_modulo desc limit 1";
    $datos = $conn->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row['id_modulo'];
  }

  function getUltimoTema($conn){
    $sql = "select * from temas order by id_tema desc limit 1";
    $datos = $conn->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row['id_tema'];
  }

  function getUltimaEvaluacion($conn,$idTema){
    $sql = "select * from evaluaciones where id_tema = {$idTema} order by id_evaluacion desc limit 1";
    $datos = $conn->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row['id_evaluacion'];
  }

  function getUltimaPregunta($conn, $idEvaluacion){
    $sql = "select * from preguntas where id_evaluacion = {$idEvaluacion} order by id_pregunta desc limit 1";
    $datos = $conn->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row['id_pregunta'];
  }


  function guardarDiapositivas($listaDiapositivas,$idTema,$conn){
          print_r($listaDiapositivas);

    for($i=0;$i<count($listaDiapositivas);$i++){
      $diapositiva=$listaDiapositivas[$i]->diapositiva;
      $diapositiva=str_replace('\"','\\\"',$diapositiva);
      $diapositiva=str_replace('\'','\\\'',$diapositiva);
      $sql = "insert into diapositivas_tema(contenido, id_tema)
      values('{$diapositiva}',{$idTema})";
      if(!($conn->consultaSimple($sql))){
        // return false;
      }
    }    
      return true;
  }

  function guardarImagenAvatar($idCurso, $archivo, $conn){
    $carpetaImagenes = '../imagenes/curso'.$idCurso.'/';
      

      $type=explode("/", $archivo['type']);
      $src = $carpetaImagenes.'avatar_'.$idCurso.'.'.$type[1];
      $sql = "update cursos  set imagen='{$src}' where id_curso={$idCurso}";                

    if ($archivo['type'] != 'image/jpg' && $archivo['type'] != 'image/jpeg' && $archivo['type'] != 'image/png' && $archivo['type'] != 'image/gif'){
                echo "Error, el archivo no es una imagen"; 
              }else if ($archivo['size'] > 1024*1024){
                echo "Error, el tamaño máximo permitido es un 1MB";
              }
              // else if ($width > 500 || $height > 500){
              //   echo "Error la anchura y la altura maxima permitida es 500px";
              // }else if($width < 60 || $height < 60){
              //   echo "Error la anchura y la altura mínima permitida es 60px";
              // }
              else if($conn->consultaSimple($sql)){    
                if (!file_exists($carpetaImagenes)) {
                  echo 'no existe';
                  mkdir($carpetaImagenes, 0777, true);
                }               
                move_uploaded_file($archivo['tmp_name'], $src);
                echo $src;
                return true;
              }else{
                echo 'error al guardar imagen de curso';
              }
              return false;
  }

    function guardarDocumentoCurso($idCurso, $archivo, $tipo, $conn){
      $tipoDocumento;
      $type=explode("/", $archivo['type']);
      $src;
      $sql;
      $carpetaAdjuntos = '../adjuntos/curso'.$idCurso.'/';

      switch ($tipo) {
      case 0:
        $tipoDocumento='draft';
        $src = $carpetaAdjuntos.$tipoDocumento.'_'.$idCurso.'.'.$type[1];
        $sql = "update cursos  set draft_url='{$src}' where id_curso={$idCurso}";
        break;
      case 1:
        $tipoDocumento='guia';
        $src = $carpetaAdjuntos.$tipoDocumento.'_'.$idCurso.'.'.$type[1];
        $sql = "update cursos  set guia_url='{$src}' where id_curso={$idCurso}";
        break;
      case 2:
       $tipoDocumento='evaluacion';
       $src = $carpetaAdjuntos.$tipoDocumento.'_'.$idCurso.'.'.$type[1];
       $sql = "update cursos  set evaluacion_url='{$src}' where id_curso={$idCurso}";
        break;
      case 3:
       $tipoDocumento='textoayuda';
       $src = $carpetaAdjuntos.$tipoDocumento.'_'.$idCurso.'.'.$type[1];
       $sql = "update cursos  set textoayuda_url='{$src}' where id_curso={$idCurso}";
        break;
      default:
        return false;
      }  

       

    if ($archivo['type'] != 'application/pdf' ){
                echo "Error, el archivo no es un documento valido"; 
              }else if ($archivo['size'] > 1024*20480){
                echo "Error, el tamaño máximo permitido es un 20MB";
              }
              // else if ($width > 500 || $height > 500){
              //   echo "Error la anchura y la altura maxima permitida es 500px";
              // }else if($width < 60 || $height < 60){
              //   echo "Error la anchura y la altura mínima permitida es 60px";
              // }
              else if($conn->consultaSimple($sql)){    
                if (!file_exists($carpetaAdjuntos)) {
                  echo 'no existe';
                  mkdir($carpetaAdjuntos, 0777, true);
                }              
                move_uploaded_file($archivo['tmp_name'], $src);
                echo $src;
                return true;
              }else{
                echo 'error al guardar archivo de curso';
              }
              return false;
  }




?>