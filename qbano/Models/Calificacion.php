<?php namespace Models;
class Calificacion{
  //atributos
  private $id;
  private $evaluacion;
  private $con;
  private $isAprobado;
  private $numeroIntento;
  private $nota;
  private $numeroAciertos;

  //metodos
  public function __construct(){
    $this->con = new Conexion();
  }

   public function add(){
    $sql = "insert into calificaciones (id_evaluacion,is_aprobada,numero_intentos) values({$this->evaluacion->get("id")},{$this->isAprobado},{$this->numeroIntento})";
    //echo $sql;
    $isSave=$this->con->consultaSimple($sql);
    return $isSave;
  }

  public function set($atributo, $contenido){
    $this->$atributo = $contenido;
  }

  public function get($atributo){
    return $this->$atributo;
  }


  public function listar(){
    $sql = "select  * from calificaciones";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }

  public function edit(){
    $sql = "update calificaciones set id_evaluacion = {$this->evaluacion->get("id")}, is_aprobada = {$this->isAprobado},
    numero_intentos = {$this->numeroIntento} where id_calificacion = {$this->id}";
    //echo $sql;
    $this->con->consultaSimple($sql);
  }

  public function editNota(){
    $sql = "update calificaciones set nota = {$this->nota}, numero_aciertos = {$this->numeroAciertos}
    where id_calificacion = {$this->id}";
    //echo $sql;
    $this->con->consultaSimple($sql);
  }

  /**public function getUltimo(){
    $sql = "select * from modulos order by id_modulo desc limit 1";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row['id_modulo'];
  }**/

  public function view(){
    
    $sql = "select * from calificaciones e where id_calificacion= {$this->id}";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row;
  }

  public function findByEvaluacion(){    
    $sql = "select * from calificaciones e where id_evaluacion= {$this->evaluacion->get("id")}";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row;
  }

  public function findByEvaluacionAndUsuario(){   
      //añadir id_usuario al where para que traiaga solo la evaluacion del usuario 
    $sql = "select * from calificaciones e where id_evaluacion= {$this->evaluacion->get("id")}";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row;
  }

  public function listarPorEvaluacion($idEvaluacion){
    $sql = "select * from calificaciones e where id_evaluacion= {$idEvaluacion}";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }


}
?>
