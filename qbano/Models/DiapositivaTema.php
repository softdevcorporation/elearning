<?php namespace Models;
class DiapositivaTema{
  //atributos
  private $id;
  private $contenido;
  private $tema;
  private $con;

  //metodos
  public function __construct(){
    $this->con = new Conexion();
  }

  public function set($atributo, $contenido){
    $this->$atributo = $contenido;
  }

  public function get($atributo){
    return $this->$atributo;
  }

  public function add(){    
    $sql = "insert into diapositivas_tema(contenido, id_tema)
      values('{$this->contenido}',{$this->tema->get("id")})";
         $isSave=$this->con->consultaSimple($sql); 
         $this->id=$this->getUltimo();
    return $isSave;
  }

  public function listar(){
    $sql = "select  * from diapositivas_tema";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }

  public function view(){
    $sql = "select * from diapositivas_tema e where id_diapositivas_tema= {$this->id}";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row;
  }

  public function edit(){
    $sql = "update diapositivas_tema set contenido = '{$this->contenido}' where id_diapositiva_tema = {$this->get('id')}";
    // echo $sql;
    $this->con->consultaSimple($sql);
  }

  public function listarPorTema($idTema){
    $sql = "select * from diapositivas_tema e where id_tema= {$idTema}";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }

  public function getUltimo(){
    $sql = "select * from diapositivas_tema order by id_diapositiva_tema desc limit 1";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row['id_diapositiva_tema'];
  }
}
?>
