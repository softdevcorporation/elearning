<?php namespace Models;

class Qboletin{

  private $id;
  private $contenido;
  private $isPrincipal;
  private $fecharegistro;
  private $con;

  public function __construct(){
    $this->con = new Conexion();
  }

  public function set($atributo, $contenido){
    $this->$atributo = $contenido;
  }

  public function get($atributo){
    return $this->$atributo;
  }

  public function add(){
    $currentDate=date("Y-m-d");
    echo 'fecha'.$currentDate;
    $sql = "insert into qboletin (contenido, isPrincipal, fecha_registro)   values('{$this->contenido}',{$this->isPrincipal},'{$currentDate}')";
         $isSave=$this->con->consultaSimple($sql);
    return $isSave;
  }

  public function listar(){
    $sql = "select * from qboletin where isPrincipal=true order by id_qboletin desc limit 1";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }

  public function listarSecond(){
    $sql = "select * from qboletin where isPrincipal=false order by id_qboletin desc";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }



  public function view(){
    $sql = "select * from qboletin";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }

public function delete(){
  $sql = "delete from qboletin where id_qboletin = {$this->id}";
  $this->con->consultaSimple($sql);
}

  public function edit(){
    $sql = "update diapositivas_tema set contenido = '{$this->contenido}' where id_diapositiva_tema = {$this->get('id')}";
    // echo $sql;
    $this->con->consultaSimple($sql);
  }

  public function getUltimo(){
    $sql = "select * from qboletin order by id_qboletin desc limit 1";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row['id_diapositiva_tema'];
  }

}


 ?>
