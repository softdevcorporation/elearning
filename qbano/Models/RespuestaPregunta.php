<?php namespace Models;
use Models\Pregunta as Pregunta;
class RespuestaPregunta{
  //atributos
  private $id;
  private $descripcion;
  private $isVerdadera;
  private $pregunta;
  private $con;

  //metodos
  public function __construct(){
    $this->con = new Conexion();
  }

  public function set($atributo, $contenido){
    $this->$atributo = $contenido;
  }

  public function get($atributo){
    return $this->$atributo;
  }

  public function add(){
    $idPregunta=$this->pregunta->get("id");
    $sql = "insert into repuestas_preguntas(descripcion, id_pregunta, isVerdadera)
                              values('{$this->descripcion}',{$idPregunta},{$this->isVerdadera})";
    $isSave=$this->con->consultaSimple($sql);
    $this->id=$this->getUltimo();
    return $isSave;
  }

  public function listar(){
    $sql = "select  * from repuestas_preguntas";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }

  public function view(){
    $sql = "select * from repuestas_preguntas e where id_repuesta_pregunta= {$this->id}";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row;
  }

  public function listarPorPregunta($idPregunta){
    $sql = "select * from repuestas_preguntas e where id_pregunta= {$idPregunta}";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }

  public function getUltimo(){
    $sql = "select * from repuestas_preguntas order by id_repuesta_pregunta desc limit 1";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row['id_repuesta_pregunta'];
  }
}
?>
