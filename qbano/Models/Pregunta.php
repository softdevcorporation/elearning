<?php namespace Models;
use Models\RespuestaPregunta as RespuestaPregunta;
class Pregunta{
  //atributos
  private $id;
  private $descripcion;
  private $idTipoPregunta;
  private $listaRespuestas;
  private $evaluacion;
  private $con;

  //metodos
  public function __construct(){
    $this->con = new Conexion();
  }

  public function set($atributo, $contenido){
    $this->$atributo = $contenido;
  }

  public function get($atributo){
    return $this->$atributo;
  }

  public function add(){
    $sql = "insert into preguntas(descripcion, id_tipo_pregunta, id_evaluacion)
                        values('{$this->descripcion}',{$this->idTipoPregunta},{$this->evaluacion->get("id")})";
    $isSave=$this->con->consultaSimple($sql);
    $this->id=$this->getUltimo();
    $this->guardarRespuestas();
    return $isSave;
  }

  public function listar(){
    $sql = "select  * from preguntas";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }

  public function view(){
    $sql = "select * from preguntas e where id_preguntas= {$this->id}";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row;
  }

  public function listarPorEvaluacion($idEvaluacion){
    $sql = "select * from preguntas e where id_evaluacion= {$idEvaluacion}";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }

  public function getUltimo(){
    $sql = "select * from preguntas order by id_pregunta desc limit 1";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row['id_pregunta'];
  }

   private function guardarRespuestas(){
if($this->idTipoPregunta==1){
  $respuesta=new RespuestaPregunta();
  $respuesta->set("isVerdadera",$this->listaRespuestas);
  $respuesta->set("pregunta",$this);
  $respuesta->add();
}else if($this->idTipoPregunta==2){
        for($j=0;$j<count($this->listaRespuestas);$j++){
          $resp=$this->listaRespuestas[$j]->responsesQuestion;
          $valueResp=$this->listaRespuestas[$j]->valueResponsesQuestion;
          $respuesta=new RespuestaPregunta();
          $respuesta->set("isVerdadera",$valueResp);
          $respuesta->set("pregunta",$this);
          $respuesta->set("descripcion",$resp);
          $respuesta->add();

        }  
     }
  }

  public function deteleByEvaluacion(){
    //$sql = "delete from preguntas where id_evaluacion = {$this->evaluacion->get("id")}";
    $sql = "select * from preguntas where id_evaluacion = {$this->evaluacion->get("id")}";
    print_r($sql);
    $preguntas=$this->con->consultaRetorno($sql);
    while($pregunta=$preguntas->fetch(\PDO::FETCH_ASSOC)){
      $sql = "delete from repuestas_preguntas where id_pregunta = {$pregunta['id_pregunta']}";
      $this->con->consultaRetorno($sql);
      print_r($pregunta);
    }
    $sql = "delete from preguntas where id_evaluacion = {$this->evaluacion->get("id")}";
    $this->con->consultaRetorno($sql);

    
  }
}
?>
