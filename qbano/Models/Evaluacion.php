<?php namespace Models;
class Evaluacion{
  //atributos
  private $id;
  private $nombre;
  private $calificacion;
  private $idTipoEvaluacion;
  private $tema;
  private $con;

  //metodos
  public function __construct(){
    $this->con = new Conexion();
  }

  public function set($atributo, $contenido){
    $this->$atributo = $contenido;
  }

  public function get($atributo){
    return $this->$atributo;
  }

  public function add(){    
    $sql = "insert into evaluaciones(nombre, id_tema)
                  values('{$this->tema->get("nombre")}',{$this->tema->get("id")})";
         $isSave=$this->con->consultaSimple($sql); 
         $this->id=$this->getUltimo();
         $this->nombre=$this->tema->get("nombre");
    return $isSave;
  }

  public function listar(){
    $sql = "select  * from evaluaciones";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }

  public function view(){
    $sql = "select * from evaluaciones e where id_evaluacion= {$this->id}";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row;
  }

  public function listarPorTema($idTema){
    $sql = "select * from evaluaciones e where id_tema= {$idTema} limit 1";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row;
  }

  public function getUltimo(){
    $sql = "select * from evaluaciones order by id_evaluacion desc limit 1";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row['id_evaluacion'];
  }
}
?>
