<?php namespace Models;
use Models\Tema as Tema;

class Curso{
  //atributos
  private $id;
  private $nombre;
  private $descripcion;
  private $fechaInicio;
  private $fechaTerminacion;
  private $fechaPublicacion;
  private $imagen;
  private $idTipoCurso;
  private $isCertificado;
  private $con;
  private $modulo;
  private $sendTo;
  private $draft;
  private $guia;
  private $evaluacion;
  private $textoAyuda;
  private $estado;


  //metodos
  public function __construct(){
    $this->con = new Conexion();

  }

  public function set($atributo, $contenido){
    $this->$atributo = $contenido;
  }

  public function get($atributo){
    return $this->$atributo;
  }

  public function listar(){
    $sql = "select * from cursos";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }

  public function add(){

    $sql = "insert into cursos(nombre, descripcion, is_certificated, fecha_inicio, fecha_terminacion, fecha_publicacion, id_tipo_curso, estado)
            values('{$this->nombre}','{$this->descripcion}',{$this->isCertificado},'{$this->fechaInicio}','{$this->fechaTerminacion}','{$this->fechaPublicacion}', {$this->idTipoCurso}, {$this->estado})";
echo $sql;
    $isSave=$this->con->consultaSimple($sql);
    $this->id=$this->getUltimo();

    if($this->enviarImagen() && $this->eviarDocumento($this->draft, 0)
    && $this->eviarDocumento($this->guia, 1) && $this->eviarDocumento($this->evaluacion, 2)
    && $this->eviarDocumento($this->textoAyuda, 3)){
        $this->addSendTo();
      return $isSave;
    }
  }



      function eviarDocumento($archivo, $tipo){
        echo 'archivo+++++++';
        if($archivo!=null){
      $tipoDocumento;
      $type=$archivo['type'];
      $src;
      $sql;
      $carpetaAdjuntos = 'adjuntos/curso'.$this->nombre.'/';
      if($type=="application/vnd.openxmlformats-officedocument.wordprocessingml.document"){
        $type="docx";
      }else if($type=="application/msword"){
        $type="doc";
      }else if($type=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
        $type="xlsx";
      }else if($type=="application/vnd.ms-excel"){
        $type="xls";
      }else if($type=="application/pdf"){
        $type="pdf";
      }

      echo 'tipo...............................';
      echo $type;

      switch ($tipo) {
      case 0:
        $tipoDocumento='draft';
        $src = $carpetaAdjuntos.$tipoDocumento.'_'.$this->nombre.'.'.$type;
        $sql = "update cursos  set draft_url='{$src}' where id_curso={$this->id}";
        echo $sql;
        break;
      case 1:
        $tipoDocumento='guia';
        $src = $carpetaAdjuntos.$tipoDocumento.'_'.$this->nombre.'.'.$type;
        $sql = "update cursos  set guia_url='{$src}' where id_curso={$this->id}";
        break;
      case 2:
       $tipoDocumento='evaluacion';
       $src = $carpetaAdjuntos.$tipoDocumento.'_'.$this->nombre.'.'.$type;
       $sql = "update cursos  set evaluacion_url='{$src}' where id_curso={$this->id}";
        break;
      case 3:
       $tipoDocumento='textoayuda';
       $src = $carpetaAdjuntos.$tipoDocumento.'_'.$this->nombre.'.'.$type;
       $sql = "update cursos  set textoayuda_url='{$src}' where id_curso={$this->id}";
        break;
      default:
        return false;
      }


    echo "tamaño del archivo".$archivo['size'];
    if ($archivo['type']!='application/vnd.openxmlformats-officedocument.wordprocessingml.document' && $archivo['type']!='application/msword'
&& $archivo['type']!='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && $archivo['type']!='application/vnd.ms-excel' && $archivo['type']!='application/pdf'){
                echo "Error, el archivo no es un documento valido";
              }
               else if ($archivo['size'] > 2097152){
                 echo "Error, el tamaño máximo permitido es un 2MB";
              }
              // else if ($width > 500 || $height > 500){
              //   echo "Error la anchura y la altura maxima permitida es 500px";
              // }else if($width < 60 || $height < 60){
              //   echo "Error la anchura y la altura mínima permitida es 60px";
              // }
              else if($this->con->consultaSimple($sql)){
                if (!file_exists($carpetaAdjuntos)) {
                  echo 'no existe';
                  mkdir($carpetaAdjuntos, 0777, true);
                }
                move_uploaded_file($archivo['tmp_name'], $src);
                return true;
              }else{
                echo 'error al guardar archivo de curso';
              }
              return false;
        }else{
          return true;
        }
  }

  private function enviarImagen(){
    if($this->imagen!=null){
      $carpetaImagenes = 'imagenes/curso'.$this->id.'/';


      $type=explode("/", $this->imagen['type']);
      $src = $carpetaImagenes.'avatar_'.$this->id.'.'.$type[1];
      $sql = "update cursos  set imagen='{$src}' where id_curso={$this->id}";

    if ($this->imagen['type'] != 'image/jpg' && $this->imagen['type'] != 'image/jpeg' && $this->imagen['type'] != 'image/png' && $this->imagen['type'] != 'image/gif'){
                echo "Error, el imagen no es una imagen";
              }
              // else if ($this->imagen['size'] > 1024*1024){
              //   echo "Error, el tamaño máximo permitido es un 1MB";
              // }

              // else if ($width > 500 || $height > 500){
              //   echo "Error la anchura y la altura maxima permitida es 500px";
              // }else if($width < 60 || $height < 60){
              //   echo "Error la anchura y la altura mínima permitida es 60px";
              // }
              else if($this->con->consultaSimple($sql)){
                if (!file_exists($carpetaImagenes)) {
                  echo 'no existe';
                  mkdir($carpetaImagenes, 0777, true);
                }
                move_uploaded_file($this->imagen['tmp_name'], $src);
                return true;
              }else{
                echo 'error al guardar imagen de curso';
              }
              return false;
    }else{
      return true;
    }

  }



  public function addSendTo(){
    for($i=0;$i<count($this->sendTo);$i++){
              $sql = "insert into cargos_has_cursos(id_curso, id_cargo)
                values({$this->id},{$this->sendTo[$i]})";
              $this->con->consultaSimple($sql);
    }
  }

  public function getUltimo(){
    $sql = "select * from cursos order by id_curso desc limit 1";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row['id_curso'];
  }



  public function cambiarEstado(){
    $sql = "update cursos set estado = {$this->estado} where id_curso = {$this->id}";
    echo $sql;
    $this->con->consultaSimple($sql);
  }

  public function editInfoCurso(){
   $sql = "update cursos set nombre = '{$this->nombre}', descripcion = '{$this->descripcion}', is_certificated = {$this->isCertificado}, fecha_inicio = '{$this->fechaInicio}', fecha_terminacion = '{$this->fechaTerminacion}', fecha_publicacion = '{$this->fechaPublicacion}' where id_curso = {$this->id}";
   echo $sql;
   $this->con->consultaSimple($sql);
 }

  // public function delete(){
  //   $sql = "delete from estudiantes where id = {$this->id}";
  //   $this->con->consultaSimple($sql);
  // }
  public function view(){
    $sql = "select * from cursos e where id_curso= {$this->id}";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row;
  }
}
?>
