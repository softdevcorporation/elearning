<?php namespace Models;
class Tema{
  //atributos
  private $id;
  private $nombre;
  private $modulo;
  private $con;
  

  //metodos
  public function __construct(){
    $this->con = new Conexion();
  }

  public function set($atributo, $contenido){
    $this->$atributo = $contenido;
  }

  public function get($atributo){
    return $this->$atributo;
  }

  public function add(){    
    $sql = "insert into temas(nombre, id_modulo) 
            values('{$this->nombre}','{$this->modulo->get("id")}')";
         $isSave=$this->con->consultaSimple($sql); 
         $this->id=$this->getUltimo();
    return $isSave;
  }

  public function listar(){
    $sql = "select  * from temas";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }

  public function view(){
    $sql = "select * from temas e where id_tema= {$this->id}";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row;
  }

  public function edit(){
    $sql = "update temas set nombre = '{$this->nombre}' where id_tema = {$this->id}";
    // echo $sql;
    $this->con->consultaSimple($sql);
  }

  public function getUltimo(){
    $sql = "select * from temas order by id_tema desc limit 1";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row['id_tema'];
  }

  public function listarPorModulo($idModulo){
    $sql = "select * from temas e where id_modulo= {$idModulo}";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }
}
?>
