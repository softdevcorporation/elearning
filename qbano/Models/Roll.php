<?php namespace Models;
class Roll{
  //atributos

  private $id;
  private $roll;
  private $permiso;
  private $con;

  //metodos
  public function __construct(){
    $this->con = new Conexion();
  }
  public function set($atributo, $contenido){
    $this->$atributo = $contenido;
  }

  public function get($atributo){
    return $this->$atributo;
  }
  public function listar(){
    $sql = "select * from roll";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }
  public function add(){
    $sql = "insert into roll(roll,permisos) values('{$this->roll}',{$this->permiso})";
    $this->con->consultaSimple($sql);
  }
  public function edit(){

    $sql = "update roll set permisos = {$this->permiso} where id_roll = {$this->id}";
    $this->con->consultaSimple($sql);
  }
  public function delete(){
    $sql = "delete from roll where id_roll = {$this->id}";
    $this->con->consultaSimple($sql);
  }
  public function view(){
    $sql = "select * from roll  where id_roll = {$this->id}";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row;
  }
}
?>
