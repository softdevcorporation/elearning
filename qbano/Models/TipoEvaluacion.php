<?php namespace Models;
class TipoEvaluacion{
  //atributos
  private $id;
  private $nombre;
  private $con;

  //metodos
  public function __construct(){
    $this->con = new Conexion();
  }

  public function set($atributo, $contenido){
    $this->$atributo = $contenido;
  }

  public function get($atributo){
    return $this->$atributo;
  }

  public function listar(){
    $sql = "select  * from tipo_evaluaciones";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }

  public function view(){
    $sql = "select * from tipo_evaluaciones e where id_tipo_evaluacion= {$this->id}";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row;
  }
}
?>
