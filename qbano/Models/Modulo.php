<?php namespace Models;
class Modulo{
  //atributos
  private $id;
  private $nombre;
  private $con;
  private $curso;

  //metodos
  public function __construct(){
    $this->con = new Conexion();
  }

   public function add(){
    $sql = "insert into modulos (nombre,id_curso)
            values('{$this->nombre}','{$this->curso->get("id")}')";
            $isSave=$this->con->consultaSimple($sql);
            $this->id=$this->getUltimo();
    return $isSave;
  }

  public function set($atributo, $contenido){
    $this->$atributo = $contenido;
  }

  public function get($atributo){
    return $this->$atributo;
  }


  public function listar(){
    $sql = "select  * from modulos";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }

  public function edit(){
    $sql = "update modulos set nombre = '{$this->nombre}' where id_modulo = {$this->id}";
    echo $sql;
    $this->con->consultaSimple($sql);
  }

  public function getUltimo(){
    $sql = "select * from modulos order by id_modulo desc limit 1";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row['id_modulo'];
  }

  public function view(){
    
    $sql = "select * from modulos e where id_modulo= {$this->id}";
    echo $sql;
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row;
  }

  public function listarPorCurso($idCurso){
    $sql = "select * from modulos e where id_curso= {$idCurso}";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }


}
?>
