<?php namespace Models;
class Seccion{
  //atributos

  private $id;
  private $nombre;
  private $con;

  //metodos
  public function __construct(){
    $this->con = new Conexion();
  }
  public function set($atributo, $contenido){
    $this->$atributo = $contenido;
  }

  public function get($atributo){
    return $this->$atributo;
  }
  public function listar(){
    $sql = "select * from secciones order by nombre asc";
    $datos = $this->con->consultaRetorno($sql);
    return $datos;
  }
  public function add(){
    $sql = "insert into secciones(nombre) values('{$this->nombre}')";
    $this->con->consultaSimple($sql);
  }
  public function edit(){

    $sql = "update secciones set nombre = '{$this->nombre}' where id = {$this->id}";
    $this->con->consultaSimple($sql);
  }
  public function delete(){
    $sql = "delete from secciones where id = {$this->id}";
    $this->con->consultaSimple($sql);
  }
  public function view(){
    $sql = "select * from secciones s where s.id= {$this->id}";
    $datos = $this->con->consultaRetorno($sql);
    $row = $datos->fetch(\PDO::FETCH_ASSOC);
    return $row;
  }
}
?>
