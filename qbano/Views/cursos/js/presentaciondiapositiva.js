var listaDiapositivas;
function diapositivaInit() {

    listDiapositivasEdit = listDiapositivasEdit;
    console.log('lista diapo' + listDiapositivasEdit + 'tamaño' + listDiapositivasEdit.length);
     $('#buttonback').attr("disabled", true);
    if (listDiapositivasEdit.length == 1) {
        $('#buttonnext').attr("disabled", true);
    }
}

var cont = 0;
function backDiapositiva() {
    if ((cont - 1) >= 0) {
        cont--;
        console.log(cont);
        // refreshEditor();
        console.log(listDiapositivasEdit[cont].contenido);
        tinymce.get('multimediaeditor').setContent(listDiapositivasEdit[cont].contenido);
    }
    if (cont == 0) {
        $('#buttonback').attr("disabled", true);
        $('#buttonnext').attr("disabled", false);
    }
    tinyMCE.activeEditor.focus();

}

function nextDiapositiva() {

    if (cont < (listDiapositivasEdit.length - 1)) {
        cont++;
        console.log(cont);
        // refreshEditor();
        console.log(listDiapositivasEdit[cont].contenido);
        tinymce.get('multimediaeditor').setContent(listDiapositivasEdit[cont].contenido);
        $('#buttonback').attr("disabled", false);
    } else {
        $('#buttonnext').attr("disabled", true);

    }
    tinyMCE.activeEditor.focus();

}

function refreshEditor() {
    tinymce.get('multimediaeditor').setContent('');
    tinymce.remove();
    tinymce.init({
        selector: 'textarea.multimediaeditor',
        height: 500,
        theme: 'modern',
        readonly: true,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });
    tinyMCE.activeEditor.focus();
}
