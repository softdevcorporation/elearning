    <script type="text/javascript" src="<?php echo URL; ?>js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>Views/cursos/js/admincursos.js"></script>


<?php $listaUsuarios=$cursos->listarTipoUsuarios();
      $listaTiposCursos=$cursos->listarTipoCurso();
?>

  <!--<form  action="" method="post" enctype="multipart/form-data">-->
   <form action="" method="post" enctype="multipart/form-data" onsubmit="enviarDatosCursos(); return false">
    <section class="container-createCourse">
      <section name="valorescurso" class="form-createCurso" >

          <div class="firstContainer">
            <div class="Course">
              <div class="inputFormValidation">
                  <label for="nombreCurso">Nombre del Curso:</label>
                  <input type="text" id="nombreCurso" name="nombreCurso" onchange="onchangeInputValidation(this)">
              </div>
              <div class="inputFormValidation">
                <label class="descrip" for="Descripcion">Descripción:</label>
                <textarea id="Descripcion" name="descripcion" onchange="onchangeInputValidation(this)"></textarea>
              </div>
            </div>

            <div class="loadImage">
                <label for="cargarImagen">Cargar Imagen:</label>
                <input type="file" id="avatarcurso" name="avatarcurso" multiple="">
            </div>
          </div>

            <div class="certificate">
                <label>Certifica:</label>
                <input type="radio" name="certifica" value="0" onchange="onchangeInputValidation(this)" checked="checked">Si
                <input type="radio" name="certifica" value="1" onchange="onchangeInputValidation(this)">No
            </div>


            <div class="times">
              <div class="inputFormValidation">
                <label for="fechaInicio">Fecha de Inicio</label>
                <input type="date" id="fechaInicio" name="fechainicio" onchange="onchangeInputValidation(this)">
              </div>
              <div class="inputFormValidation">
                <label for="fechaFinal">Fecha de Final</label>
                <input type="date" id="fechaFinal" name="fechafin" onchange="onchangeInputValidation(this)">
              </div>
              <div class="inputFormValidation">
                <label for="fechaPublicaion">Fecha de Publicaión</label>
                <input type="date" id="fechaPublicaion" name="fechapublicacion" onchange="onchangeInputValidation(this)">
              </div>
            </div>

            <div class="sendTo">
              <label>Dirigido a:</label>
              <div class="inputFormValidation">
              <ul class="sendToList">
              <?php while($row = $listaUsuarios->fetch(\PDO::FETCH_ASSOC))
                {
              ?>
                <li><input class="sendTo-item"type="checkbox" onchange="onchangeOptionValidation(this)" name="sendTo[]" value="<?php echo $row['id_tipo_usuario_curso'];?>"><p class="sendTo-name"><?php echo $row['nombre'];?></p></li>
              <?php
                }
              ?>
              </ul>
              </div>
            </div>


            <div class="typeCourse">
              <label>Tipo de Cursos:</label>
              <?php
              $cont=0;
              while($row = $listaTiposCursos->fetch(\PDO::FETCH_ASSOC))
                {
                  $cont++;
                  if($cont==1){
              ?>
                <input type="radio" name="tipoCurso" checked="checked"  value="<?php echo $row['id_tipo_curso'];?>"><?php echo $row['nombre'];?>
              <?php
                  }else{
                    ?>
                <input type="radio" name="tipoCurso"   value="<?php echo $row['id_tipo_curso'];?>"><?php echo $row['nombre'];?>

              <?php
                  }
                }
              ?>
            </div>

            <div class="search">
              <label class="proyecto" for="draft">Draft:</label>
              <input type="file" id="filedraft" name="" multiple="">

              <label class="formato" for="guiaFormato">Guia del Formato:</label>
              <input type="file" id="fileguiaformato" name="" multiple="">

              <label class="examen" for="Evaluacion">Evaluacion:</label>
              <input type="file" id="fileevaluacion" name="" multiple="">

              <label class="ayuda" for="textoAyuda">Texto Ayuda:</label>
              <input type="file" id="filetextoayuda" name="" multiple="">
            </div>

            <div class="namesModuleTheme">
              <div class="inputFormValidation">
                <label for="nombreModulo">Nombre del Modulo:</label>
                <input type="text" id="nombreModulo" name="nombremodulo" onchange="onchangeInputValidation(this)">
              </div>
              <div class="inputFormValidation">
                <label for="nombreTema">Nombre del Tema:</label>
                <input type="text" id="nombreTema" name="nombretema" onchange="onchangeInputValidation(this)">
              </div>
            </div>
        </section>
      </section>

          <section id="SeccionMultimedia">
          <section id="multimediaviews">
            <article id="viewdiapo_0" class="multimediaviews-View" onclick="changedViewDiapositiva(this)" style="cursor:pointer">
                1
            </article>
            <article id="viewadd" class="multimediaviews-View" >
                <a onclick="addDiapositiva()">añadir</a>
            </article>
          </section>
          <div id="ContenedorEditor">
      <textarea id="multimediaeditor" class="multimediaeditor" >

        <p style="text-align: center; font-size: 15px;"><img title="TinyMCE Logo" src="//www.tinymce.com/images/glyph-tinymce@2x.png" alt="TinyMCE Logo" width="110" height="97" />
  </p>
  <h1 style="text-align: center;">Welcome to the TinyMCE & Community Plugins demo!</h1>
  <h5 style="text-align: center;">Note, this is not an "enterprise/premium" demo.<br>Visit the <a href="https://www.tinymce.com/pricing/#demo-enterprise">pricing page</a> to demo our premium plugins.</h5>
  <p>Please try out the features provided in this full featured example.</p>
  <p>Note that any <b>MoxieManager</b> file and image management functionality in this example is part of our commercial offering – the demo is to show the integration.</h2>

  <h2>Got questions or need help?</h2>
  <ul>
    <li>Our <a href="//www.tinymce.com/docs/">documentation</a> is a great resource for learning how to configure TinyMCE.</li>
    <li>Have a specific question? Visit the <a href="http://community.tinymce.com/forum/">Community Forum</a>.</li>
    <li>We also offer enterprise grade support as part of <a href="http://tinymce.com/pricing">TinyMCE Enterprise</a>.</li>
  </ul>

  <h2>A simple table to play with</h2>
  <table style="text-align: center;">
    <thead>
      <tr>
        <th>Product</th>
        <th>Cost</th>
        <th>Really?</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>TinyMCE</td>
        <td>Free</td>
        <td>YES!</td>
      </tr>
      <tr>
        <td>Plupload</td>
        <td>Free</td>
        <td>YES!</td>
      </tr>
    </tbody>
  </table>

  <h2>Found a bug?</h2>
  <p>If you think you have found a bug please create an issue on the <a href="https://github.com/tinymce/tinymce/issues">GitHub repo</a> to report it to the developers.</p>

  <h2>Finally ...</h2>
  <p>Don't forget to check out our other product <a href="http://www.plupload.com" target="_blank">Plupload</a>, your ultimate upload solution featuring HTML5 upload support.</p>
  <p>Thanks for supporting TinyMCE! We hope it helps you and your users create great content.<br>All the best from the TinyMCE team.</p>

      </textarea>

      <input type="button" onclick="get_editor_content()" value="Get content"></input>


      </div>
      </section>



        <section class="containerTest">
          <section class="test" id="testContainer">
            <div class="question" id="questionTheme_0">
              <select class="typeQuestion" name="typequestion" onchange="isSelectedTypeQuestion(this)">
                <option value="" disabled>Elegir pregunta</option>
                <option value="1">Falso/Verdadero</option>
                <option value="2">Selección Multiple</option>
              </select>
              <input type="text" placeholder="Escribe tu Pregunta" name="question" >
              <div class="testTo">
                <ul class="testList">
                    <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_0[]" checked ><p class="testName">Si</p></li>
                    <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_0[]" ><p class="testName">No</p></li>
                </ul>
              </div>
            </div>
          </section>
          <button type="button" onclick="addQuestion()">Añadir Pregunta</button>
        </section>
        <button type="submit">Guardar</button>
      </form>
    <script type="text/javascript" src="<?php echo URL; ?>plugins/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>plugins/tinymce/init-tinymce.js"></script>

      <script>
tinymce.init({
  selector: 'textarea.multimediaeditor',
  height: 500,
  theme: 'modern',
  language : "es_MX",
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
  ],
  toolbar1: 'undo redo | insert | styleselect | Imagenes Sonidos | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons | codesample',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
   setup: function(editor) {
    editor.addButton('Imagenes', {
      type: 'menubutton',
      text: 'Imagenes',
      icon: false,
      menu: [{
        text: 'Cargar Imagen',
        onclick: function() {
          editor.insertContent('&nbsp;<strong>Menu item 1 here!</strong>&nbsp;');
        }
      }, {
        text: 'Cargar Imagen Gif',
        onclick: function() {
          editor.insertContent('&nbsp;<em>Menu item 2 here!</em>&nbsp;');
        }
      }]
    });
    editor.addButton('Sonidos', {
      text: 'Sonidos',
      icon: false,
      onclick: function () {
        editor.insertContent('&nbsp;<b>It\'s my button!</b>&nbsp;');
      }
    });
  },
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });
</script>
<script>
        function get_editor_content() {

            // alert(tinyMCE.get('multimediaeditor').getContent());
            // console.log(tinyMCE.get('multimediaeditor').getContent());
            console.log($('#multimediaeditor')[0].value);

        }
    </script>
