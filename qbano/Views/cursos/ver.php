  <script type="text/javascript" src="<?php echo URL; ?>js/jquery.min.js"></script>
<?php
  $listaCursos = $cursos->index();
  $curso = $datos;
  $modulos =  $cursos->getModules($curso['id_curso']);
?>
<script>
  var idCursoPHP = "<?php echo $curso['id_curso'] ?>";
</script>

<h2 class="title">Información del Curso</h2>
<section class="containerInformationCurso">
    <div class="informationCurso">
      <ul class="listInformationCurso">
        <li><label for="">Nombre Curso:</label><input type="text" name="nombreCurso" value="<?php echo $curso['nombre'] ?>" disabled></li>
        <li><label for="">Descripcion:</label><textarea  name="descripcion" disabled><?php echo $curso['descripcion'] ?></textarea></li>
        <li>
                <label>Certifica:</label>
                <?php
                  if($curso['is_certificated']==false){
                ?>
                <input type="radio" name="certifica" value="0" disabled checked>Si
                <input type="radio" name="certifica" value="1" disabled>No
                <?php
                  }else if($curso['is_certificated']==true){
                ?>
                <input type="radio" name="certifica" value="0" disabled >Si
                <input type="radio" name="certifica" value="1" disabled checked>No
                <?php
                  }
                ?>
                
         </li>
        <li><label for="">Fecha Inicio:</label><input type="text" name="fechainicio" value="<?php echo $curso['fecha_inicio'] ?>" disabled></li>
        <li><label for="">Fecha Fin:</label><input type="text" name="fechafin" value="<?php echo $curso['fecha_terminacion'] ?>" disabled></li>
        <li><label for="">Fecha Publicación:</label><input type="text" name="fechapublicacion" value="<?php echo $curso['fecha_publicacion'] ?>" disabled></li>
      </ul>
    </div>

    <div class="imageContainer">
      <div class="imageFigure">
        <img class="imageFigure-img" src="<?php echo URL. $curso['imagen'] ?>" />
      </div>
      <a href="<?php echo URL. $curso['draft_url'] ?>">Draft</a>
      <a href="<?php echo URL. $curso['guia_url'] ?>">Guia de Apoyo</a>
      <a href="<?php echo URL. $curso['evaluacion_url'] ?>">Evaluacion</a>
      <a href="<?php echo URL. $curso['textoayuda_url'] ?>">Texto de Ayuda</a>
    </div>
</section>

<?php
  while($row = $modulos->fetch(\PDO::FETCH_ASSOC)){

?>

<div>
 
<section class="containerModuleTheme">
  <div class="ModuleTheme">
    <label for="">Nombre Modulo:</label><input type="text" name="nombreModulo" class="nameModule" value="<?php echo $row['nombre'] ?>" disabled>

  </div>
</section>
</div>

<?php
  if($curso['id_tipo_curso']==1){
  $temas =  $cursos->getTemas($row['id_modulo']);
  while($row = $temas->fetch(\PDO::FETCH_ASSOC)){
        $diapositivas = $cursos->getDiapositivas($row['id_tema']);
        $listDiapositivasEdit=array();
        while($diapo = $diapositivas->fetch(\PDO::FETCH_ASSOC)){
              $listDiapositivasEdit[]=$diapo;

        }
        // print_r($listDiapositivasEdit[0]);

        ?>
          <script>
           var listDiapositivasEdit = <?php echo json_encode($listDiapositivasEdit) ?>;
          </script>
        
<section class="containerModuleTheme">
  <div class="ModuleTheme">
    <label for="">Nombre Tema:</label><input type="text" name="nameTheme" class="nameTheme" value="<?php echo $row['nombre'] ?>" disabled>
    <a onclick="showPresentation();">Ver Presentacion</a>
  </div>
</section>

<div>
   <section  id="SeccionMultimedia" style="text-align:center;">
          <!--
          <section id="multimediaviews">
            <article id="viewdiapo_0" class="multimediaviews-View" onclick="changedViewDiapositiva(this)" style="cursor:pointer">
                1
            </article>
            <article id="viewadd" class="multimediaviews-View" >
                <a onclick="addDiapositiva()">añadir</a>
            </article>
          </section>-->
          <div id="ContenedorEditor">
      <textarea id="multimediaeditor" class="multimediaeditor" >
        <?php
          $cont=$listDiapositivasEdit[0];
          print_r($cont['contenido']);
        ?>

      </textarea>
      <section>

      <a id="buttonback" onclick="backDiapositiva()" >Atras</a>
      <a id="buttonnext" onclick="nextDiapositiva()">Siguiente</a>

      </section>
      
      </div>
      </section>
</div>


    </div>
  <?php
    }
  ?>
<?php
    }else if($curso['id_tipo_curso']==2){
      $temas =  $cursos->getTemas($row['id_modulo']);
      
      while($row = $temas->fetch(\PDO::FETCH_ASSOC)){
        ?>
      <section class="containerModuleTheme">
      <div class="ModuleTheme">
      <label for="">Nombre Tema:</label><input type="text" name="nameTheme" class="nameTheme" value="<?php echo $row['nombre'] ?>" disabled>
      <a href="http://localhost/qbano/temas/ver/<?php echo $row['id_tema'] ?>">Ver</a>
      </div>
      </section>
<?php
      }
    }
  }
?>
    <script type="text/javascript" src="<?php echo URL; ?>plugins/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>plugins/tinymce/init-tinymce.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>Views/cursos/js/presentaciondiapositiva.js"></script>

    <script type="text/javascript">
      diapositivaInit();
    </script>



<script>
tinymce.init({
  selector: 'textarea.multimediaeditor',
  height: 500,
  theme: 'modern',
  readonly:true,
  language : "es_MX",
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
  ],
  toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });

 function showPresentation(){
  $('#SeccionMultimedia').slideToggle();
  }
</script>
