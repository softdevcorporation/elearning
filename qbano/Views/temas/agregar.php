<script type="text/javascript" src="<?php echo URL; ?>js/jquery.min.js"></script>
<?php
//print_r('id curso');
  //print_r($datos);
  $tema = $datos;
  //$diapositivas = $temas->getDiapositivas($tema['id_tema']);
  //$listDiapositivasEdit=array();
  //print_r($tema);
  $idModulo=$temas->getModulo($tema['id_modulo']);
 ?>
 <script>
   var idCursoPHP = "<?php echo $datos['id_curso'] ?>";
 </script>
 <a href="<?php echo URL; ?>administrativo/editar/<?php echo $idModulo['id_curso']?>" class="controlVolver btn btn-info">volver</a>

<form action="" method="post" enctype="multipart/form-data" onsubmit="enviarDatosTema(); return false">
<div class="namesModuleTheme">
  <div class="inputFormValidation">
    <label for="nombreTema">Nombre del Tema:</label>
    <input type="text" id="nombreTema" name="nombretema" onchange="onchangeInputValidation(this)">
  </div>
</div>

<section id="SeccionMultimedia">
<section id="multimediaviews">
  <article id="viewdiapo_0" class="multimediaviews-View" onclick="changedViewDiapositiva(this)" style="cursor:pointer">
      1
  </article>
  <article id="viewadd" class="multimediaviews-View" >
      <a onclick="addDiapositiva()">añadir</a>
  </article>
</section>
<div id="ContenedorEditor">
<textarea id="multimediaeditor" class="multimediaeditor" >

</textarea>

</div>
</section>

<section class="containerTest">
  <section class="test" id="testContainer">

  </section>
  <button type="button" onclick="addQuestion()" class="btn btn-info">Añadir Pregunta</button>
</section>
<button type="submit" class="btn btn-success">Guardar</button>
</form>
 <input name="image" type="file" id="upload" class="hidden" onchange="" style="display:none">
     <script type="text/javascript" src="<?php echo URL; ?>Config/configuracion.js"></script>
 <script type="text/javascript" src="<?php echo URL; ?>Views/administrativo/js/admintemas.js"></script>
<script type="text/javascript" src="<?php echo URL; ?>plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo URL; ?>plugins/tinymce/init-tinymce.js"></script>

      <script>
tinymce.init({
  selector: 'textarea.multimediaeditor',
  height: 500,
  theme: 'modern',
  language : "es_MX",
  paste_data_images: true,
  statusbar: false,
  media_live_embeds: true,
  plugins: [
    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars fullscreen",
    "insertdatetime media nonbreaking save table contextmenu directionality",
    "template paste textcolor colorpicker textpattern"
  ],
  toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons | fondo | Sonidos | grabadora | Juegos",
  image_advtab: true,
  audio_template_callback: function(data) {
    console.log('hola ');
   return '<audio controls>' + '\n<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + '</audio>';
 },
  file_picker_callback: function(callback, value, meta) {
    if (meta.filetype == 'image') {
      $('#upload').trigger('click');
      $('#upload').on('change', function() {
        var file = this.files[0];
        console.log(file);
        var reader = new FileReader();
        reader.onload = function(e) {
          callback(e.target.result, {
            alt: ''
          });
        };
        reader.readAsDataURL(file);
        console.log(reader);
      });
    }else if(meta.filetype == 'media'){      
        $('#uploadSound').trigger('click');
      $('#uploadSound').on('change', function() {
        var file = this.files[0];
        console.log(file);
        var reader = new FileReader();
        reader.onload = function(e) {
          callback(e.target.result, {
            alt: ''
          });
        };
        reader.readAsDataURL(file);
        console.log(reader);
      });
    }
  },
  templates: [{
    title: 'Test template 1',
    content: 'Test 1'
  }, {
    title: 'Test template 2',
    content: 'Test 2'
  }],
  setup: function (editor) {
    editor.addButton('fondo', {
      text: 'Fondo',
      icon: false,
      onclick: function () {
        editor.windowManager.open({
          title: 'Colorpicker',
          body: [{
        type: 'colorpicker',
        name: 'colorpicker',
            }],
            onsubmit: function(e) {
                    // Insert content when the window form is submitted
                    editor.dom.setStyle('tinymce', 'background-color', e.data.colorpicker);
                },
          width: 400,
          height: 200,
        });
        }
    });
    editor.addButton('Sonidos', {
      text: 'Sonidos',
      icon: false,
      onclick: function () {
        editor.windowManager.open({
          title: 'Sonidos',
          body: [{
        type: 'textbox', 
        subtype: 'file',
        name: 'sound',
            }],
            onsubmit: function(e) {
                    // Insert content when the window form is submitted
                    //editor.dom.setStyle('tinymce', 'background-color', e.data.colorpicker);
                    console.log(e.data);
                },
          width: 400,
          height: 200,
        });
        }
    });
    editor.addButton('grabadora', {
      text: 'Grabadora',
      icon: false,
      onclick: function () {
        editor.windowManager.open({
          title: 'Grabadora',
          url: 'http://localhost/elearning/qbano/Views/administrativo/voz/index/example_simple_exportwav.html',
            onsubmit: function(e) {
                    // Insert content when the window form is submitted
                    console.log('parametro tynice');
                    console.log(e.data.grab);
                },
          width: 400,
          height: 200,
          buttons: [{
          text: 'Close',
          onclick: function(e) {
                    // Insert content when the window form is submitted
                    console.log('parametro tynice');
                    console.log(editor.windowManager.windows);
                }
        }]
        },{
          arg2: "Hello world"
        });
        }

    });
    editor.addButton('Juegos', {
      text: 'Juegos',
      icon: false,
      onclick: function () {
        tinymce.activeEditor.setContent('<iframe src="http://localhost/elearning/juegos/juego1/HTML5/" ></iframe>');
        alert('Juegos');
        }

    });
  },



  content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ]
});
</script>
