<script type="text/javascript" src="<?php echo URL; ?>js/jquery.min.js"></script>
<?php
  $tema = $datos;
  $diapositivas = $temas->getDiapositivas($tema['id_tema']);
  $idTemaCurso=$tema['id_tema'];
  $listDiapositivasEdit=array();
  //print_r($tema);
  $idModulo=$temas->getModulo($tema['id_modulo']);
  //print_r($idModulo['id_curso']);
  //print_r('corte tema');
  //print_r($diapositivas);
  while($diapo = $diapositivas->fetch(\PDO::FETCH_ASSOC)){
        $listDiapositivasEdit[]=$diapo;
        // print_r($diapo);
  }
?>
<script>
 var listDiapositivasEdit = <?php echo json_encode($listDiapositivasEdit) ?>;
</script>
<a href="<?php echo URL; ?>administrativo/editar/<?php echo $idModulo['id_curso']?>" class="controlVolver btn btn-info">volver</a>
<section class="containerModuleTheme">
  <div class="ModuleTheme">
  <div class="inputFormValidation">
    <label for="">Nombre Tema:</label>
    <input type="text" name="nameTheme" class="nameTheme" value="<?php echo $tema['nombre'] ?>" onchange="onchangeInputValidation(this)" disabled>
    <button onclick="activarDatosTema(this, <?php echo $tema['id_tema'] ?>)" class="btn btn-info">Editar</button>
  </div>
  </div>
</section>


<section  id="SeccionMultimedia" style="text-align:center;">
       <section id="multimediaviews">
         <article id="viewdiapo_0" class="multimediaviews-View" onclick="changedViewDiapositiva(this)" style="cursor:pointer">
             1
         </article>
         <article id="viewadd" class="multimediaviews-View" >
             <a onclick="addDiapositiva()">añadir</a>
         </article>
       </section>
       <div id="ContenedorEditor">
   <textarea id="multimediaeditor" class="multimediaeditor" >
     <?php
       $cont=$listDiapositivasEdit[0];
       //print_r($cont['contenido']);
     ?>

   </textarea>
   <button onclick="activarDiapositiva(this)" class="btn btn-info">Editar</button>
   </div>
   </section>

   <div>
   <section class="containerTest">
  <div>
    <h2 class="title">Evaluación <?php echo $tema['nombre'] ?></h2>
  </div>
  <section>
     <div class="test" id="testContainer">

       <?php
       $evaluacion=$temas->getEvaluacion($tema['id_tema']);
       $preguntas =  $temas->getPreguntas($evaluacion['id_evaluacion']);
       $contadorPreguntas=0;
       while($row = $preguntas->fetch(\PDO::FETCH_ASSOC)){
       ?>
       <div class="question" id="questionTheme_<?php echo $contadorPreguntas ?>">
       <span class="questionDelete" onclick="eliminarPregunta(this);">X</span>
         <select class="typeQuestion form-control" name="typequestion" onchange="isSelectedTypeQuestion(this)" disabled>
           <option value="" disabled>Elegir pregunta</option>
           <?php
             if($row['id_tipo_pregunta']==1){
            ?>
           <option value="1" selected >Falso/Verdadero</option>
           <option value="2">Selección Multiple</option>
           <?php
             }else if($row['id_tipo_pregunta']==2){
           ?>
           <option value="1" >Falso/Verdadero</option>
           <option value="2" selected>Selección Multiple</option>
           <?php
             }
           ?>
         </select>
         <input type="text" class="form-control" placeholder="Escribe tu Pregunta" name="question" value="<?php echo $row['descripcion'] ?>" disabled />
         <div class="testTo">
           <ul class="testList">
           <?php
           if($row['id_tipo_pregunta']==1){
                 $respuestas =  $temas->getRespuestas($row['id_pregunta']);
                 $res = $respuestas->fetch(\PDO::FETCH_ASSOC);
                 if($res['isVerdadera']==0){
                   //echo "false";
             ?>
               <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_<?php echo $contadorPreguntas ?>[]"  disabled/><p class="testName">Si</p></li>
               <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_<?php echo $contadorPreguntas ?>[]" checked disabled/><p class="testName">No</p></li>
                 <?php
                 }else if($res['isVerdadera']==1){
                   //echo 'true';
                  ?>
               <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_<?php echo $contadorPreguntas ?>[]" checked disabled/><p class="testName">Si</p></li>
               <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_<?php echo $contadorPreguntas ?>[]"  disabled/><p class="testName">No</p></li>
             <?php
                 }
             }else if($row['id_tipo_pregunta']==2){
               $respuestas =  $temas->getRespuestas($row['id_pregunta']);
               while($row = $respuestas->fetch(\PDO::FETCH_ASSOC)){
                 if($row['isVerdadera']){
             ?>

               <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]" disabled checked/><p class="testName"><input type="text" placeholder="Respuesta" value="<?php echo $row['descripcion'] ?>" disabled/></p></li>
               <?php
                 }else{
               ?>
               <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]" disabled /><p class="testName"><input type="text" placeholder="Respuesta" value="<?php echo $row['descripcion'] ?>" disabled/></p></li>
           <?php
                 }
               }
             }
           ?>
           </ul>

           </div>
           <button>Editar Pregunta</button>
          </div>

         <?php
         $contadorPreguntas++;
             }
         ?>
          <script>
        var numeroPreguntas = "<?php echo $contadorPreguntas ?>";
    </script>
           </div>
         </section>
          <button type="button" onclick="addQuestion()" class="btn btn-info">Añadir Pregunta</button>
      <button onclick="editarEvaluacion(<?php echo $idTemaCurso ?>)" class="btn btn-success">Guardar</button>
            </div>

         <script type="text/javascript" src="<?php echo URL; ?>plugins/tinymce/tinymce.min.js"></script>
         <script type="text/javascript" src="<?php echo URL; ?>plugins/tinymce/init-tinymce.js"></script>
         <script type="text/javascript" src="<?php echo URL; ?>Views/administrativo/js/editarcurso.js"></script>

         <script type="text/javascript">
           diapositivaInit();
         </script>
         <input name="image" type="file" id="upload" class="hidden" onchange="" style="display:none">



     <script>
     tinymce.init({
  selector: 'textarea.multimediaeditor',
  height: 500,
  theme: 'modern',
  language : "es_MX",
  readonly:true,
  paste_data_images: true,
  statusbar: false,
  media_live_embeds: true,
  plugins: [
    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars fullscreen",
    "insertdatetime media nonbreaking save table contextmenu directionality",
    "template paste textcolor colorpicker textpattern"
  ],
  toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons | fondo | Sonidos  | grabadora | Juegos",
  image_advtab: true,
  audio_template_callback: function(data) {
    console.log('hola ');
   return '<audio controls>' + '\n<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + '</audio>';
 },
  file_picker_callback: function(callback, value, meta) {
    if (meta.filetype == 'image') {
      $('#upload').trigger('click');
      $('#upload').on('change', function() {
        var file = this.files[0];
        console.log(file);
        var reader = new FileReader();
        reader.onload = function(e) {
          callback(e.target.result, {
            alt: ''
          });
        };
        reader.readAsDataURL(file);
        console.log(reader);
      });
    }else if(meta.filetype == 'media'){      
        $('#uploadSound').trigger('click');
      $('#uploadSound').on('change', function() {
        var file = this.files[0];
        console.log(file);
        var reader = new FileReader();
        reader.onload = function(e) {
          callback(e.target.result, {
            alt: ''
          });
        };
        reader.readAsDataURL(file);
        console.log(reader);
      });
    }
  },
  templates: [{
    title: 'Test template 1',
    content: 'Test 1'
  }, {
    title: 'Test template 2',
    content: 'Test 2'
  }],
  setup: function (editor) {
    editor.addButton('fondo', {
      text: 'Fondo',
      icon: false,
      onclick: function () {
        editor.windowManager.open({
          title: 'Colorpicker',
          body: [{
        type: 'colorpicker',
        name: 'colorpicker',
            }],
            onsubmit: function(e) {
                    // Insert content when the window form is submitted
                    editor.dom.setStyle('tinymce', 'background-color', e.data.colorpicker);
                },
          width: 400,
          height: 200,
        });
        }
    });
    editor.addButton('Sonidos', {
      text: 'Sonidos',
      icon: false,
      onclick: function () {
        editor.windowManager.open({
          title: 'Sonidos',
          body: [{
        type: 'textbox', 
        subtype: 'file',
        name: 'sound',
            }],
            onsubmit: function(e) {
                    // Insert content when the window form is submitted
                    //editor.dom.setStyle('tinymce', 'background-color', e.data.colorpicker);
                    console.log(e.data);
                },
          width: 400,
          height: 200,
        });
        }
    });
    editor.addButton('grabadora', {
      text: 'Grabadora',
      icon: false,
      onclick: function () {
        editor.windowManager.open({
          title: 'Grabadora',
          url: 'http://localhost/elearning/qbano/Views/administrativo/voz/index/example_simple_exportwav.html',
            onsubmit: function(e) {
                    // Insert content when the window form is submitted
                    console.log('parametro tynice');
                    console.log(e.data.grab);
                },
          width: 400,
          height: 200,
          buttons: [{
          text: 'Close',
          onclick: function(e) {
                    // Insert content when the window form is submitted
                    console.log('parametro tynice');
                    console.log(editor.windowManager.windows);
                }
        }]
        },{
          arg2: "Hello world"
        });
        }

    });
    editor.addButton('Juegos', {
      text: 'Juegos',
      icon: false,
      onclick: function () {
        tinymce.activeEditor.setContent('<iframe src="http://localhost/elearning/juegos/juego1/HTML5/" ></iframe>');
        alert('Juegos');
        }

    });
  },



  content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ]
});

      function showPresentation(){
       $('#SeccionMultimedia').slideToggle();
     }
     </script>
