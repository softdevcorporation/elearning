  <script type="text/javascript" src="<?php echo URL; ?>js/jquery.min.js"></script>
<?php
  $listaCursos = $cursos->index();
  $curso = $datos;
  $modulos =  $cursos->getModules($curso['id_curso']);
?>
<script>
  var idCursoPHP = "<?php echo $curso['id_curso'] ?>";
</script>

<a href="<?php echo URL; ?>administrativo" class="controlVolver btn btn-info">volver</a>
<h2 class="title">Información del Curso</h2>
<section class="containerInformationCurso row">
    <div class="informationCurso col-md-6">
      <ul class="listInformationCurso">

        <li><label>Nombre:</label><span><?php echo $curso['nombre'] ?></span></li>
        <li><label>Descripcion:</label><span><?php echo $curso['descripcion'] ?></span></li>
        <li>
        <div class="certificate">
                <label>Certifica:</label>
                <?php
                  if($curso['is_certificated']==false){
                ?>
                <input type="radio" name="certifica" value="0" disabled checked><span>Si</span>
                <input type="radio" name="certifica" value="1" disabled><span>No</span>
                <?php
                  }else if($curso['is_certificated']==true){
                ?>
                <input type="radio" name="certifica" value="0" disabled ><span>Si</span>
                <input type="radio" name="certifica" value="1" disabled checked><span>No</span>
                <?php
                  }
                ?>
            </div>
         </li>
        <li><label for="">Fecha Inicio:</label><span><?php echo $curso['fecha_inicio'] ?></span></li>
        <li><label for="">Fecha Fin:</label><span><?php echo $curso['fecha_terminacion'] ?></span></li>
        <li><label for="">Fecha Publicación:</label><span><?php echo $curso['fecha_publicacion'] ?></span></li>
      </ul>
    </div>

    <div class="imageContainer col-md-6" >
      <div class="imageFigure">
        
        <img class="imageFigure-img img-thumbnail" src="<?php echo URL. $curso['imagen'] ?>" />
      </div>
      <span>Archivos Adjuntos:</span>
      <a href="<?php echo URL. $curso['draft_url'] ?>" target="_blank">Draft</a>
      <a href="<?php echo URL. $curso['guia_url'] ?>" target="_blank">Guia de Apoyo</a>
      <a href="<?php echo URL. $curso['evaluacion_url']?>" target="_blank">Evaluacion</a>
      <a href="<?php echo URL. $curso['textoayuda_url']?>" target="_blank">Texto de Ayuda</a>
    </div>
</section>

<?php
  while($row = $modulos->fetch(\PDO::FETCH_ASSOC)){

?>

<div>

<section class="containerModuleTheme">
  <div class="ModuleTheme">
    <span><?php echo $row['nombre'] ?></span>
  </div>
</section>
</div>

<?php
  if($curso['id_tipo_curso']==1){
  $temas =  $cursos->getTemas($row['id_modulo']);
  while($row = $temas->fetch(\PDO::FETCH_ASSOC)){
        $diapositivas = $cursos->getDiapositivas($row['id_tema']);
        $listDiapositivasEdit=array();
        while($diapo = $diapositivas->fetch(\PDO::FETCH_ASSOC)){
              $listDiapositivasEdit[]=$diapo;

        }
        // print_r($listDiapositivasEdit[0]);

        ?>
          <script>
           var listDiapositivasEdit = <?php echo json_encode($listDiapositivasEdit) ?>;
          </script>

<section class="containerModuleTheme">
  <div class="ModuleTheme">
    <span><?php echo $row['nombre'] ?></span>
    <!--<a onclick="showPresentation();">Ver Presentacion</a>-->
  </div>
</section>

<div>
   <section  id="SeccionMultimedia" style="text-align:center;">
          <!--
          <section id="multimediaviews">
            <article id="viewdiapo_0" class="multimediaviews-View" onclick="changedViewDiapositiva(this)" style="cursor:pointer">
                1
            </article>
            <article id="viewadd" class="multimediaviews-View" >
                <a onclick="addDiapositiva()">añadir</a>
            </article>
          </section>-->
          <div id="ContenedorEditor">
      <textarea id="multimediaeditor" class="multimediaeditor" >
        <?php
          $cont=$listDiapositivasEdit[0];
          print_r($cont['contenido']);
        ?>

      </textarea>
      <section>

      <a id="buttonback" onclick="backDiapositiva()" class="controlsBtn btn btn-info">Atras</a>
      <a id="buttonnext" onclick="nextDiapositiva()" class="controlsBtn btn btn-info">Siguiente</a>

      </section>

      </div>
      </section>
</div>


    </div>
  <?php
    }
  ?>
<?php
    }else if($curso['id_tipo_curso']==2){
      $temas =  $cursos->getTemas($row['id_modulo']);

      while($row = $temas->fetch(\PDO::FETCH_ASSOC)){
        ?>
      <section class="containerModuleTheme">
      <div class="ModuleTheme">
      <label for="">Nombre Tema:</label><input type="text" name="nameTheme" class="nameTheme" value="<?php echo $row['nombre'] ?>" disabled>
      <a href="http://localhost/qbano/temas/ver/<?php echo $row['id_tema'] ?>">Ver</a>
      </div>
      </section>
<?php
      }
    }
  }
?>
    <script type="text/javascript" src="<?php echo URL; ?>plugins/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>plugins/tinymce/init-tinymce.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>Views/administrativo/js/presentaciondiapositiva.js"></script>

    <script type="text/javascript">
      diapositivaInit();
    </script>



<script>

  tinymce.init({
    selector: 'textarea.multimediaeditor',
    height: 500,
    readonly : true,
    toolbar: false,
    menubar: false,
    statusbar: false,
    theme: 'modern',
    content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ]
   });

 function showPresentation(){
  $('#SeccionMultimedia').slideToggle();
  }
</script>
