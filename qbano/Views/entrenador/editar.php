  <script type="text/javascript" src="<?php echo URL; ?>js/jquery.min.js"></script>
<?php
  $listaCursos = $cursos->index();
  $curso = $datos;
  $modulos =  $cursos->getModules($curso['id_curso']);
  $fechaInicio=$curso['fecha_inicio'];
$fechaFin=$curso['fecha_terminacion'];
$fechaActual= date('Y-m-d');
?>
<script>
  var idCursoPHP = "<?php echo $curso['id_curso'] ?>";
</script>


<a href="<?php echo URL; ?>administrativo" class="controlVolver btn btn-info">volver</a>

<h2 class="title">Información del Curso</h2>
<section class="containerInformationCurso row">
    <div class="informationCurso col-md-6">
      <ul class="listInformationCurso">
        <li><label for="">Nombre Curso:</label><input type="text" name="nombreCurso" value="<?php echo $curso['nombre'] ?>" disabled></li>
        <li><label for="">Descripcion:</label></li>
        <li><textarea class="listInformationCurso-texarea" name="descripcion" disabled><?php echo $curso['descripcion'] ?></textarea></li>
        <li>
          <div class="certificate">
                <label>Certifica:</label>
                <?php
                  if($curso['is_certificated']==false){
                ?>
                <input type="radio" name="certifica" value="0" disabled checked><span>Si</span>
                <input type="radio" name="certifica" value="1" disabled><span>No</span>
                <?php
                  }else if($curso['is_certificated']==true){
                ?>
                <input type="radio" name="certifica" value="0" disabled ><span>Si</span>
                <input type="radio" name="certifica" value="1" disabled checked><span>No</span>
                <?php
                  }
                ?>
            </div>
         </li>
        <li><label for="">Fecha Inicio:</label><input type="text" name="fechainicio" value="<?php echo $curso['fecha_inicio'] ?>" disabled></li>
        <li><label for="">Fecha Fin:</label><input type="text" name="fechafin" value="<?php echo $curso['fecha_terminacion'] ?>" disabled></li>
        <li><label for="">Fecha Publicación:</label><input type="text" name="fechapublicacion" value="<?php echo $curso['fecha_publicacion'] ?>" disabled></li>
      </ul>
    </div>

    <div class="imageContainer col-md-6">
      <div class="imageFigure" style="margin-bottom:1em">
        <img class="imageFigure-img" src="<?php echo URL. $curso['imagen'] ?>" />
      </div>
      <span>Archivos Adjuntos</span>
      <div class="containerDraft">
        <a href="<?php echo URL. $curso['draft_url'] ?>" target="_blank">Draft</a>
        <input type="file" disabled id="filedraft" name="draft" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" multiple="">
      </div>

      <div class="containerGuia">
        <a href="<?php echo URL. $curso['guia_url'] ?>" target="_blank">Guia de Apoyo</a>
        <input type="file" disabled id="fileguiaformato" name="guia" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" name="" multiple="">
      </div>

      <div class="containerEvaluacion">
        <a href="<?php echo URL. $curso['evaluacion_url'] ?>" target="_blank">Evaluacion</a>
        <input type="file" disabled id="fileevaluacion" name="evaluacion"  accept="application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple="">
      </div>

      <div class="containerAyuda">
        <a href="<?php echo URL. $curso['textoayuda_url'] ?>" target="_blank">Texto de Ayuda</a>
        <input type="file" disabled id="filetextoayuda" name="ayuda" accept="application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"  multiple="">
      </div>

    </div>
    <?php
    if($fechaInicio<=$fechaActual && $fechaActual<=$fechaFin){

        ?>
       <button onclick="cursoMarcha()" class="buttonEditarInfo btn btn-info">Editar</button>
      <?php
}else{
  ?>
 <button onclick="activarDatosCursos(this) " class="buttonEditarInfo btn btn-info">Editar</button>
      <?php
}
?>
</section>

<?php





  if($curso['id_tipo_curso']==2){
 ?>
 <div id="contenedoradministradormodulos">
<a  name="modal" data-toggle="modal" data-target="#modalNewModulo">Nuevo Módulo</a>



<!-- Modal -->
<div class="modal fade" id="modalNewModulo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Nuevo Módulo</h4>
      </div>
      <div class="modal-body">
        <div id="newmodulo" name="newmodulo" class="NewModulo">
  <span >Nombre Módulo: </span><br>
  <input class="NewModuloInput form-control"  name="newmoduloinput" /><br>
  <button onclick="guardarNuevoModulo()" class="btn btn-info">Guardar</button>
</div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<?php
  }
?>
<?php
  while($row = $modulos->fetch(\PDO::FETCH_ASSOC)){

?>

<div>

<section class="containerModuleTheme">
  <div class="ModuleTheme ModuleThemeEdit">
    <label for="">Nombre Módulo:</label><input type="text" name="nombreModulo" class="nameModule" value="<?php echo $row['nombre'] ?>" disabled>
    <?php
    if($fechaInicio<=$fechaActual && $fechaActual<=$fechaFin){

        ?>
       <button onclick="cursoMarcha()" class="ModuleThemeEdit-Btn btn btn-info">Editar</button>
      <?php
    }else{
    ?>
    <button onclick="activarDatosModulo(this, <?php echo $row['id_modulo'] ?>)" class="ModuleThemeEdit-Btn btn btn-info">Editar</button>
    <div style="clear:both"></div>
      <?php
      }
    ?>
    <?php
    if($curso['id_tipo_curso']==2){
      ?>
      <a href="<?php echo URL ?>temas/agregar/<?php echo $row['id_modulo'] ?>">Nuevo Tema</a>
      <?php
        }
      ?>
  </div>
</section>

</div>

<?php
  if($curso['id_tipo_curso']==1){
  $temas =  $cursos->getTemas($row['id_modulo']);
  while($row = $temas->fetch(\PDO::FETCH_ASSOC)){
        $diapositivas = $cursos->getDiapositivas($row['id_tema']);
        $idTemaCurso=$row['id_tema'];

        $listDiapositivasEdit=array();
        while($diapo = $diapositivas->fetch(\PDO::FETCH_ASSOC)){
              $listDiapositivasEdit[]=$diapo;

        }
        // print_r($listDiapositivasEdit[0]);

        ?>
          <script>
           var listDiapositivasEdit = <?php echo json_encode($listDiapositivasEdit) ?>;
          </script>
        <?php
          if($curso['id_tipo_curso']==2){
        ?>

<a href="">Nuevo Tema</a>
        <?php
          }
        ?>
<section class="containerModuleTheme">
  <div class="ModuleTheme ModuleThemeEdit">
    <label for="">Nombre Tema:</label><input type="text" name="nameTheme" class="nameTheme" value="<?php echo $row['nombre'] ?>" disabled>
    <?php
    $existPreguntas=$cursos->existPreguntas($row['id_tema'] );
        if(!$existPreguntas){
      ?>
      <span class="fa fa-exclamation-circle"></span>
      <?php
        }
      ?>
    <a onclick="showPresentation();">Ver Presentacion</a>
    <?php
    if($fechaInicio<=$fechaActual && $fechaActual<=$fechaFin){

        ?>
       <button onclick="cursoMarcha()" class="ModuleThemeEdit-Btn btn btn-info">Editar</button>
      <?php
    }else{
    ?>
    <button onclick="activarDatosTema(this, <?php echo $row['id_tema'] ?>)" class="ModuleThemeEdit-Btn btn btn-info">Editar</button>
    <div style="clear:both"></div>
      <?php
      }
    ?>
    
    </span>
  </div>
</section>

   <section  id="SeccionMultimedia" style="text-align:center;display:none">
          <section id="multimediaviews">
            <article id="viewdiapo_0" class="multimediaviews-View" onclick="changedViewDiapositiva(this)" style="cursor:pointer">
                1
            </article>
            <article id="viewadd" class="multimediaviews-View" >
                <a onclick="addDiapositiva()">añadir</a>
            </article>
          </section>
          <div id="ContenedorEditor">
      <textarea id="multimediaeditor" class="multimediaeditor" >
        <?php
          $cont=$listDiapositivasEdit[0];
          print_r($cont['contenido']);
        ?>

      </textarea>
      <button onclick="activarDiapositiva(this)" class="btn btn-info">Editar Diapositiva</button>
      </div>
      </section>


<section class="containerTest">
  <div>
    <h2 class="title">Evaluación <?php echo $row['nombre'] ?></h2>
  </div>
  <section>
  <div class="test" id="testContainer">

    <?php
    $evaluacion=$cursos->getEvaluacion($row['id_tema']);
    $preguntas =  $cursos->getPreguntas($evaluacion['id_evaluacion']);
    $contadorPreguntas=0;
    while($row = $preguntas->fetch(\PDO::FETCH_ASSOC)){
    ?>
    <div class="question" id="questionTheme_0">
    <span class="questionDelete" onclick="eliminarPregunta(this);">X</span>
      <select class="typeQuestion form-control" name="typequestion" onchange="isSelectedTypeQuestion(this)" disabled>
        <option value="" disabled>Elegir pregunta</option>
        <?php
          if($row['id_tipo_pregunta']==1){
         ?>
        <option value="1" selected >Falso/Verdadero</option>
        <option value="2">Selección Multiple</option>
        <?php
          }else if($row['id_tipo_pregunta']==2){
        ?>
        <option value="1" >Falso/Verdadero</option>
        <option value="2" selected>Selección Multiple</option>
        <?php
          }
        ?>
      </select>
      <input type="text" class="form-control" placeholder="Escribe tu Pregunta" name="question" value="<?php echo $row['descripcion'] ?>" disabled />
      <div class="testTo">
        <ul class="testList">
        <?php
        if($row['id_tipo_pregunta']==1){
              $respuestas =  $cursos->getRespuestas($row['id_pregunta']);
              $res = $respuestas->fetch(\PDO::FETCH_ASSOC);
              if($res['isVerdadera']==0){
          ?>
            <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_<?php echo $row['id_pregunta'] ?>[]"  disabled/><p class="testName">Si</p></li>
            <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_<?php echo $row['id_pregunta'] ?>[]" checked disabled/><p class="testName">No</p></li>
              <?php
              }else if($res['isVerdadera']==1){
               ?>
            <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_<?php echo $row['id_pregunta'] ?>[]"  checked disabled/><p class="testName">Si</p></li>
            <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_<?php echo $row['id_pregunta'] ?>[]"  disabled/><p class="testName">No</p></li>
          <?php
              }
          }else if($row['id_tipo_pregunta']==2){
            $respuestas =  $cursos->getRespuestas($row['id_pregunta']);
            while($row = $respuestas->fetch(\PDO::FETCH_ASSOC)){
              if($row['isVerdadera']==true){
          ?>

            <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]" checked disabled/><p class="testName"><input type="text" placeholder="Respuesta" value="<?php echo $row['descripcion'] ?>" disabled/></p></li>

        <?php
              }else if($row['isVerdadera']==false){
                ?>
            <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]"  disabled/><p class="testName"><input type="text" placeholder="Respuesta" value="<?php echo $row['descripcion'] ?>" disabled/></p></li>
       <?php
              }
            }
          }
        ?>
        </ul>

        </div>
        <button onclick="" class="btn btn-info">Editar Pregunta</button>
      </div>

      <?php
      $contadorPreguntas++;
          }
          //echo $contadorPreguntas;
      ?>
      <script>
        var numeroPreguntas = "<?php echo $contadorPreguntas ?>";
    </script>

        </div>
        </section>
        <?php
if($fechaInicio<=$fechaActual && $fechaActual<=$fechaFin){

        ?>
       <button type="button" onclick="cursoMarcha()" class="btn btn-info">Añadir Pregunta</button>
      <button onclick="cursoMarcha()" class="btn btn-info">Guardar</button>
      <?php
}else{
  ?>
 <button type="button" onclick="addQuestion()" class="btn btn-info">Añadir Pregunta</button>
      <button onclick="editarEvaluacion(<?php echo $idTemaCurso ?>)" class="btn btn-success">Guardar</button>
      <?php
}
      ?>
      </section>
  <?php
    }
  ?>
<?php
    }else if($curso['id_tipo_curso']==2){
      $temas =  $cursos->getTemas($row['id_modulo']);


      while($row = $temas->fetch(\PDO::FETCH_ASSOC)){
        $existPreguntas=$cursos->existPreguntas($row['id_tema'] );
        ?>
      <section id="contenedortemasunidad" class="containerModuleTheme">
      <div class="ModuleTheme">
      <label for="">Nombre Tema:</label><input type="text" name="nameTheme" class="nameTheme" value="<?php echo $row['nombre'] ?>" disabled/>
      <?php
        if(!$existPreguntas){
      ?>
      <span class="fa fa-exclamation-circle"></span>
      <?php
        }
      ?>
      <a href="<?php echo URL ?>temas/editar/<?php echo $row['id_tema'] ?>" class="btn btn-info">Editar</a>

      <!-- <span class="fa fa-file"></span> -->
      </div>
      </section>

<?php
      }
    }
  }
?>
</div>
<input name="image" type="file" id="upload" class="hidden" onchange="" style="display:none">


<script type="text/javascript">
function modal(){
  $("#dialog").dialog();

}

</script>


    <script type="text/javascript" src="<?php echo URL; ?>Config/configuracion.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>plugins/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>plugins/tinymce/init-tinymce.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>Views/administrativo/js/editarcurso.js"></script>

    <script type="text/javascript">
      diapositivaInit();
    </script>



<script>
tinymce.init({
  selector: 'textarea.multimediaeditor',
  height: 500,
  theme: 'modern',
  readonly:true,
  language : "es_MX",
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars  fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'template paste textcolor colorpicker textpattern imagetools codesample toc'
  ],
  toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });

 function showPresentation(){
  $('#SeccionMultimedia').slideToggle();
  }
</script>
