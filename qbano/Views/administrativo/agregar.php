<?php
//phpinfo();
session_start();
//echo 'session';
//echo $_SESSION['roll'];
$rol=$cursos->getRol($_SESSION['roll']);
$estadoCursoPhp=0;
if($rol['id_roll']==3){
  $estadoCursoPhp=0;
}else if($rol['id_roll']==1){
  $estadoCursoPhp=1;
}

//print_r($rol);
?>
<script>
  var estadoCursoPhp = "<?php echo $estadoCursoPhp; ?>";
</script>
<?php $listaUsuarios=$cursos->listarCargos();
      $listaTiposCursos=$cursos->listarTipoCurso();
?>
<!--<input type="file" id="prueba" />-->
<!--<script>
  function a(){
    console.log($('#prueba'));
  }
  </script>-->

  <!--<form  action="" method="post" enctype="multipart/form-data">-->
   <form action="" method="post" enctype="multipart/form-data" onsubmit="enviarDatosCursos(); return false">
    <section class="container-createCourse">
      <section name="valorescurso" class="form-createCurso" >

          <section class="Container-Course row">
            <div class="Course col-md-6">
              <div class="inputFormValidation form-group">
                  <label for="nombreCurso" >Nombre del Curso:</label>
                  <input type="text" id="nombreCurso" name="nombreCurso" class="NombreCursoInput form-control" onchange="onchangeInputValidation(this)">
              </div>
              <div class="inputFormValidation form-group">
                <label class="descrip" for="Descripcion">Descripción:</label>
                <textarea id="Descripcion" name="descripcion" class="form-control" onchange="onchangeInputValidation(this)"></textarea>
              </div>

            <div class="loadImage">
                <label for="cargarImagen">Cargar Imagen:</label>
                <input type="file" id="avatarcurso" name="avatarcurso" accept="image/*" multiple="">
            </div>
            <div class="inputFormValidation form-group">
                <label for="fechaInicio">Fecha de Inicio</label>
                <input type="date" id="fechaInicio" name="fechainicio" class="form-control" onchange="onchangeInputValidation(this)" placeholder="yyyy-mm-dd" max="2050-12-31" onlyread>
              </div>
              <div class="inputFormValidation form-group">
                <label for="fechaFinal">Fecha de Final</label>
                <input type="date" id="fechaFinal" name="fechafin" class="form-control" onchange="onchangeInputValidation(this)" placeholder="yyyy-mm-dd" max="2050-12-31" onlyread>
              </div>
              <div class="inputFormValidation form-group">
                <label for="fechaPublicaion">Fecha de Publicación</label>
                <input type="date" id="fechaPublicaion" name="fechapublicacion" class="form-control" onchange="onchangeInputValidation(this)" placeholder="yyyy-mm-dd" max="2050-12-31" onlyread>
              </div>
            </div>

            <div class="Course Course2 col-md-6" >
              <div class="certificate">
                <label>Certifica:</label>
                <input type="radio" name="certifica" value="0" onchange="onchangeInputValidation(this)" checked="checked"/><span>Si</span>
                <input type="radio" name="certifica" value="1" onchange="onchangeInputValidation(this)"/><span>No</span>
            </div>
            <div class="sendTo">
              <label>Dirigido a:</label>
              <div class="inputFormValidation">
              <ul class="sendToList">
              <?php while($row = $listaUsuarios->fetch(\PDO::FETCH_ASSOC))
                {
              ?>
                <li><input class="sendTo-item" type="checkbox" onchange="onchangeOptionValidation(this)" name="sendTo[]" value="<?php echo $row['id_cargo'];?>"><p class="sendTo-name"><?php echo $row['cargo'];?></p></li>
              <?php
                }
              ?>
              </ul>
              </div>
            </div>
            <div class="typeCourse">
              <label>Tipo de Cursos:</label>
              <?php
              $cont=0;
              while($row = $listaTiposCursos->fetch(\PDO::FETCH_ASSOC))
                {
                  $cont++;
                  if($cont==1){
              ?>
                <input type="radio" name="tipoCurso" checked="checked"  value="<?php echo $row['id_tipo_curso'];?>"><span><?php echo $row['nombre'];?></span>
              <?php
                  }else{
                    ?>
                <input type="radio" name="tipoCurso"   value="<?php echo $row['id_tipo_curso'];?>"><span><?php echo $row['nombre'];?></span>

              <?php
                  }
                }
              ?>
            </div>
            <div class="search">
            <div class="search-File">
              <label class="proyecto" for="draft">Draft:</label>
              <input type="file" id="filedraft" name="" onclick="hideValidacion(this)" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf" multiple="">
              <span style="display:none;">Mensaje de error</span>
            </div>
            <div class="search-File">
              <label class="formato" for="guiaFormato">Guia del Formato:</label>
              <input type="file" id="fileguiaformato" onclick="hideValidacion(this)" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf" name="" multiple="">
              <span style="display:none;">Mensaje de error</span>
            </div>
            <div class="search-File">
              <label class="examen" for="Evaluacion">Evaluacion:</label>
              <input type="file" id="fileevaluacion" name="" onclick="hideValidacion(this)" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf" multiple="">
              <span style="display:none;">Mensaje de error</span>
            </div>
            <div class="search-File">
              <label class="ayuda" for="textoAyuda">Texto Ayuda:</label>
              <input type="file" id="filetextoayuda" name="" onclick="hideValidacion(this)" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf"  multiple="">
              <span style="display:none;">Mensaje de error</span>
            </div>
            </div>
            </div>
            </section>

            <div class="namesModuleTheme row">
              <div class="inputFormValidation form-gruop col-md-6">
                <label for="nombreModulo">Nombre del Modulo:</label>
                <input type="text" id="nombreModulo" name="nombremodulo" class="form-control" onchange="onchangeInputValidation(this)">
              </div>
              <div class="inputFormValidation form-gruop col-md-6">
                <label for="nombreTema">Nombre del Tema:</label>
                <input type="text" id="nombreTema" name="nombretema" class="form-control" onchange="onchangeInputValidation(this)">
              </div>
            </div>
        </section>
      </section>

          <section id="SeccionMultimedia">
          <section id="multimediaviews">
            <article id="viewdiapo_0" class="multimediaviews-View" onclick="changedViewDiapositiva(this)" style="cursor:pointer">
                1
            </article>
            <article id="viewadd" class="multimediaviews-View" >
              <button type="button" name="button" onclick="addDiapositiva()">Añadir</button>
              <!-- <a onclick="addDiapositiva()">Añadir</a> -->
            </article>
          </section>
          <div id="ContenedorEditor">
      <textarea id="multimediaeditor" class="multimediaeditor" >

      </textarea>

    <!--  <input type="button" onclick="get_editor_content()" value="Get content"></input>-->


      </div>
      </section>
       <input name="image" type="file" id="upload" class="hidden" onchange="" style="display:none">
       <input name="image" type="file" id="uploadSound" accept="audio/*" class="hidden" onchange="" style="display:none">


        <section class="containerTest">
          <h2>Crear Evaluación</h2>
          <section class="test" id="testContainer">

          </section>
          <button type="button" class="btn btn-info" onclick="addQuestion()">Añadir Pregunta</button>
        </section>
        <button type="submit" class="btn btn-lg btn-success">Guardar</button>
      </form>
    <script type="text/javascript" src="<?php echo URL; ?>js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>Config/configuracion.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>Views/administrativo/js/admincursos.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>plugins/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>plugins/tinymce/init-tinymce.js"></script>




      <script>

tinymce.init({
  selector: 'textarea.multimediaeditor',
  height: 500,
  theme: 'modern',
  language : "es_MX",
  paste_data_images: true,
  statusbar: false,
  media_live_embeds: true,
  plugins: [
    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars fullscreen",
    "insertdatetime media nonbreaking save table contextmenu directionality",
    "template paste textcolor colorpicker textpattern"
  ],
  toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons | fondo | Sonidos | grabadora | Juegos",
  image_advtab: true,
  audio_template_callback: function(data) {
    console.log('hola ');
   return '<audio controls>' + '\n<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + '</audio>';
 },
  file_picker_callback: function(callback, value, meta) {
    if (meta.filetype == 'image') {
      $('#upload').trigger('click');
      $('#upload').on('change', function() {
        var file = this.files[0];
        console.log(file);
        var reader = new FileReader();
        reader.onload = function(e) {
          callback(e.target.result, {
            alt: ''
          });
        };
        reader.readAsDataURL(file);
        console.log(reader);
      });
    }else if(meta.filetype == 'media'){
        $('#uploadSound').trigger('click');
      $('#uploadSound').on('change', function() {
        var file = this.files[0];
        console.log(file);
        var reader = new FileReader();
        reader.onload = function(e) {
          callback(e.target.result, {
            alt: ''
          });
        };
        reader.readAsDataURL(file);
        console.log(reader);
      });
    }
  },
  templates: [{
    title: 'Test template 1',
    content: 'Test 1'
  }, {
    title: 'Test template 2',
    content: 'Test 2'
  }],
  setup: function (editor) {
    editor.addButton('fondo', {
      text: 'Fondo',
      icon: false,
      onclick: function () {
        editor.windowManager.open({
          title: 'Colorpicker',
          body: [{
        type: 'colorpicker',
        name: 'colorpicker',
            }],
            onsubmit: function(e) {
                    // Insert content when the window form is submitted
                    editor.dom.setStyle('tinymce', 'background-color', e.data.colorpicker);
                },
          width: 400,
          height: 200,
        });
        }
    });
    editor.addButton('Sonidos', {
      text: 'Sonidos',
      icon: false,
      onclick: function () {
        editor.windowManager.open({
          title: 'Sonidos',
          body: [{
        type: 'textbox',
        subtype: 'file',
        name: 'sound',
            }],
            onsubmit: function(e) {
                    // Insert content when the window form is submitted
                    //editor.dom.setStyle('tinymce', 'background-color', e.data.colorpicker);
                    console.log(e.data);
                },
          width: 400,
          height: 200,
        });
        }
    });
    editor.addButton('grabadora', {
      text: 'Grabadora',
      icon: false,
      onclick: function () {
        editor.windowManager.open({
          title: 'Grabadora',
          url: 'http://localhost/elearning/qbano/Views/administrativo/voz/index/example_simple_exportwav.html',
            onsubmit: function(e) {
                    // Insert content when the window form is submitted
                    console.log('parametro tynice');
                    console.log(e.data.grab);
                },
          width: 400,
          height: 200,
          buttons: [{
          text: 'Close',
          onclick: function(e) {
                    // Insert content when the window form is submitted
                    console.log('parametro tynice');
                    console.log(editor.windowManager.windows);
                }
        }]
        },{
          arg2: "Hello world"
        });
        }

    });
    editor.addButton('Juegos', {
      text: 'Juegos',
      icon: false,
      onclick: function () {
        tinymce.activeEditor.setContent('<iframe src="http://localhost/elearning/juegos/juego1/HTML5/"></iframe>');
        alert('Juegos');
        }

    });
  },



  content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ]
});





</script>
<script>



        function get_editor_content() {

            // alert(tinyMCE.get('multimediaeditor').getContent());
            // console.log(tinyMCE.get('multimediaeditor').getContent());
            console.log($('#multimediaeditor')[0].value);

        }

    </script>
