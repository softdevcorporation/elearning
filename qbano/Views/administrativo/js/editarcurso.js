function activarDatosCursos(button) {
    nomIn = $('input[name=nombreCurso]');
    nomIn.attr('disabled', false);
    descripcionIn = $('textarea[name=descripcion]');
    descripcionIn.attr('disabled', false);
    certificaIn = $('input[name=certifica]')
    certificaIn.attr('disabled', false);
    fechainicioIn = $('input[name=fechainicio]')
    fechainicioIn.attr('disabled', false);
    fechafinIn = $('input[name=fechafin]')
    fechafinIn.attr('disabled', false);
    fechapublicacionIn = $('input[name=fechapublicacion]')
    fechapublicacionIn.attr('disabled', false);
    draftIn = $('input[name=draft]');
    draftIn.attr('disabled', false)
    guiaIn = $('input[name=guia]');
    guiaIn.attr('disabled', false)
    evaluacionIn = $('input[name=evaluacion]');
    evaluacionIn.attr('disabled', false)
    ayudaIn = $('input[name=ayuda]');
    ayudaIn.attr('disabled', false)
    var boton = $(button);
    boton.replaceWith('<button onclick="enviarDatosCursos(this)" class="buttonEditarInfo btn btn-info">Guardar Cambios</button>');
}

function enviarDatosCursos(button) {
    nomIn = $('input[name=nombreCurso]');
    nombreCurso = nomIn[0].value;
    nomIn.attr('disabled', false);
    descripcionIn = $('textarea[name=descripcion]');
    descripcion = descripcionIn[0].value;
    descripcionIn.attr('disabled', false);
    certificaIn = $('input[name=certifica]')
    certifica = 0;
    draftIn = $('#filedraft');
    draft = draftIn.prop('files')[0]
    draftIn.attr('disabled', true)
    guiaIn = $('#fileguiaformato');
    guia = guiaIn.prop('files')[0]
    guiaIn.attr('disabled', true)
    evaluacionIn = $('#fileevaluacion');
    evaluacion = evaluacionIn.prop('files')[0]
    evaluacionIn.attr('disabled', true)
    ayudaIn = $('#filetextoayuda');
    ayuda = ayudaIn.prop('files')[0]
    ayudaIn.attr('disabled', true)

    console.log(certificaIn[0]);
    console.log(certificaIn[1]);
    if (certificaIn[0].checked) {
        certifica = 0;
    } else {
        certifica = 1;
    }
    //alert('certifica'+certifica);
    certificaIn.attr('disabled', false);
    fechainicioIn = $('input[name=fechainicio]')
    fechainicio = fechainicioIn[0].value;
    fechainicioIn.attr('disabled', false);
    fechafinIn = $('input[name=fechafin]')
    fechafin = fechafinIn[0].value;
    fechafinIn.attr('disabled', false);
    fechapublicacionIn = $('input[name=fechapublicacion]')
    fechapublicacion = fechapublicacionIn[0].value;
    fechapublicacionIn.attr('disabled', false);
    var formpost = new FormData();

    formpost.append('action', "saveInfoCurso");
    formpost.append('nombreCurso', nombreCurso);
    formpost.append('descripcion', descripcion);
    formpost.append('certifica', certifica);
    formpost.append('fechainicio', fechainicio);
    formpost.append('fechafin', fechafin);
    formpost.append('fechapublicacion', fechapublicacion);
    formpost.append('draft',draft);
    formpost.append('guia',guia);
    formpost.append('evaluacion',evaluacion);
    formpost.append('ayuda',ayuda);

    //alert('id curso' + idCursoPHP);
    //alert(URL+"cursos/editar/"+idCursoPHP);
    $.ajax({
        type: 'POST',
        url: URL+"cursos/editar/"+idCursoPHP,
        data:
        formpost
        ,
        contentType: false,
        processData: false,

        success: function (data, textStatus, jqXHR) {
             console.log(data);
             alert('Datos Guardados Exitosamente');
        },
        error: function (jqXHR, textStatus, errorThrown) {
/*            alert("didn't work!");
*/        }
    });
    restablecerDatosCursos(button);
}

function restablecerDatosCursos(button) {
    nomIn = $('input[name=nombreCurso]');
    nomIn.attr('disabled', true);
    descripcionIn = $('textarea[name=descripcion]');
    descripcionIn.attr('disabled', true);
    certificaIn = $('input[name=certifica]')
    certificaIn.attr('disabled', true);
    fechainicioIn = $('input[name=fechainicio]')
    fechainicioIn.attr('disabled', true);
    fechafinIn = $('input[name=fechafin]')
    fechafinIn.attr('disabled', true);
    fechapublicacionIn = $('input[name=fechapublicacion]')
    fechapublicacionIn.attr('disabled', true);
    var boton = $(button);
    boton.replaceWith('<button onclick="activarDatosCursos(this)" class="buttonEditarInfo btn btn-info">Editar</button>');
}

function activarDatosModulo(button, id) {
    modulo = $(button).parent().find('input[name=nombreModulo]');
    modulo.attr('disabled', false);
    var boton = $(button);
    boton.replaceWith('<button onclick="enviarDatosModulo(this, ' + id + ')" class="ModuleThemeEdit-Btn btn btn-info">Guardar Cambios</button>');
}

function enviarDatosModulo(button, id) {
    modulo = $(button).parent().find('input[name=nombreModulo]');
    moduloValue = modulo[0].value;

    var formpost = new FormData();

    formpost.append('action', "saveInfoModulo");
    formpost.append('modulo', moduloValue);
    formpost.append('idModulo', id);

    $.ajax({
        type: 'POST',
        url: URL+"cursos/editar/"+idCursoPHP,
        data:
        formpost
        ,
        contentType: false,
        processData: false,

        success: function (data, textStatus, jqXHR) {
            // alert(data);
            alert('Datos Guardados Exitosamente');
        },
        error: function (jqXHR, textStatus, errorThrown) {
/*            alert("didn't work!");
*/        }
    });
    restablecerDatosModulo($(button), id);
}

function restablecerDatosModulo(button, id) {
    modulo = $(button).parent().find('input[name=nombreModulo]');
    modulo.attr('disabled', true);
    var boton = $(button);
    var a = id;
    a = 'onclick="activarDatosModulo(this,' + a + ' )"';
    boton.replaceWith('<button ' + a + ' class="ModuleThemeEdit-Btn btn btn-info">Editar</button>');
}

function activarDatosTema(button, id) {
    tema = $(button).parent().find('input[name=nameTheme]');
    tema.attr('disabled', false);
    var boton = $(button);
    boton.replaceWith('<button onclick="enviarDatosTema(this, ' + id + ')" class="ModuleThemeEdit-Btn btn btn-info">Guardar Cambios</button>');
}

function enviarDatosTema(button, id) {
    tema = $(button).parent().find('input[name=nameTheme]');
    temaValue = tema[0].value;
    //alert('miguel' + temaValue);

    var formpost = new FormData();

    formpost.append('action', "saveInfoTema");
    formpost.append('nombre', temaValue);
    formpost.append('idTema', id);
    if(!isEmpty(tema) & !validateQuestions()){
    $.ajax({
        type: 'POST',
        url: URL+"temas/editar/"+id,
        data:
        formpost
        ,
        contentType: false,
        processData: false,

        success: function (data, textStatus, jqXHR) {
            alert('Datos Guardados Exitosamente');
        },
        error: function (jqXHR, textStatus, errorThrown) {
/*            alert("didn't work!");
*/        }
        });
    }


    restablecerDatosTema($(button), id);
}

function isEmpty(input) {
                var str = input[0].value;
                str = str.trim();
                // alert(str);
                if (str == null || str == "" || str == undefined) {
                        input[0].focus();
                        var parent = $(input[0]).parent(".inputFormValidation");
                        $(parent).children("span").remove();
                        $(parent).append('<span style="color:red;display:block">Por Favor Rellene Este Campo</span>');
                        return true;
                }
        }

         function validateQuestions() {
                var cajaTest = $('#testContainer');
                cajapregunta = cajaTest.find('.question');
                console.log('preguntaaas');
                console.log(cajapregunta);
                for (var i = 0; i < cajapregunta.length; i++) {
                        cajapreguntaitem = cajapregunta[i];
                        console.log(cajapreguntaitem);
                        tipopregunta = $(cajapreguntaitem).find('select[name=typequestion]');
                        tipopregunta = tipopregunta[0];
                        tipopregunta = tipopregunta.value;
                        console.log('tipopregunta');
                        console.log(tipopregunta);
                        pregunta = $(cajapreguntaitem).find('input[name=question]');
                        console.log('text pregunta')
                        console.log(pregunta[0]);
                        console.log(pregunta[0].value);
                        respuesta = $(cajapreguntaitem).find('.questionAsk');
                        console.log('respuestas')
                        console.log(respuesta);


                        // console.log(cajapregunta[i]);

                        if (pregunta[0].value == null || pregunta[0].value == undefined || (pregunta[0].value).trim() == "") {
                                console.log('valor texto pregunta')
                                return true;
                        }

                        console.log(respuesta[0].checked);
                        console.log(respuesta[1].checked);
                        if (tipopregunta == 1) {
                                if (!respuesta[0].checked && !respuesta[1].checked) {
                                        console.log('llene uno de los campos');
                                        return true;
                                }
                        } else if (tipopregunta == 2) {
                                strrespuesta = $(cajapreguntaitem).find('input[name=strrespuesta]');
                                console.log(strrespuesta);
                                for(var x=0;x<strrespuesta.length;x++){
                                        if (strrespuesta[x].value == null || strrespuesta[x].value == undefined || (strrespuesta[x].value).trim() == "") {
                                                console.log('valor texto respuesta')
                                                return true;
                                        }
                                }
                                console.log('tipo 2');
                                if (!respuesta[0].checked && !respuesta[1].checked
                                        && !respuesta[2].checked && !respuesta[3].checked) {
                                        console.log('llene uno de los campos');
                                        return true;
                                }
                        }
                        console.log('--------------------------------------------------------');
                }
                return false;
        }

        function onchangeInputValidation(input) {
        var parent = $(input).parent(".inputFormValidation");
        $(parent).children("span").remove();
}

function restablecerDatosTema(button, id) {
    tema = $(button).parent().find('input[name=nameTheme]');
    tema.attr('disabled', true);
    var boton = $(button);
    var a = id;
    a = 'onclick="activarDatosTema(this,' + a + ' )"';
    boton.replaceWith('<button ' + a + ' class="ModuleThemeEdit-Btn btn btn-info">Editar</button>');
}

var countDiapositivas = 0;
var viewsdiapositivas = [];
var viewActivo = 0;
var isDiapositivaBlock = true;
var idTemaDiapositiva;


function diapositivaInit() {

    var caja = $('#multimediaviews');
    caja.empty();
    listDiapositivasEdit = listDiapositivasEdit;
    console.log('lista diapo' + listDiapositivasEdit + 'tamaño' + listDiapositivasEdit.length);
    for (var i = 0; i < listDiapositivasEdit.length; i++) {
        viewsdiapositivas[i] = listDiapositivasEdit[i].contenido;
        idTemaDiapositiva = listDiapositivasEdit[i].id_tema;
        countDiapositivas = listDiapositivasEdit.length - 1;
        caja.append(`<article id="viewdiapo_` + i + `" class="multimediaviews-View" onclick="changedViewDiapositiva(this)" style="cursor:pointer">
                ` + (i + 1) + `
            </article>`);
    }
    caja.append(`<article id="viewadd" class="multimediaviews-View" >
                <a onclick="addDiapositiva()">añadir</a>
            </article>`);
    // alert(viewsdiapositivas.length);
}

function getContentActiveEditor() {
    // console.log(tinyMCE.activeEditor.getContent());
    return tinyMCE.activeEditor.getContent();
}

function updateviewsdiapositivas() {
    viewsdiapositivas[viewActivo] = getContentActiveEditor();
}

function changedViewDiapositiva(str) {
    updateviewsdiapositivas();
    var id = $(str).attr("id");
    id = id.split('_');
    id = id[1];
/*    alert(id);
*/    viewActivo = id;
    refreshEditor();
    tinymce.get('multimediaeditor').setContent(viewsdiapositivas[id]);
}

function refreshEditor() {
    tinymce.get('multimediaeditor').setContent('');
    tinymce.remove();
         tinymce.init({
  selector: 'textarea.multimediaeditor',
  height: 500,
  theme: 'modern',
  language : "es_MX",
  paste_data_images: true,
  statusbar: false,
  media_live_embeds: true,
  plugins: [
    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars fullscreen",
    "insertdatetime media nonbreaking save table contextmenu directionality",
    "template paste textcolor colorpicker textpattern"
  ],
  toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons | fondo | Sonidos | grabadora | Juegos",
  image_advtab: true,
  audio_template_callback: function(data) {
    console.log('hola ');
   return '<audio controls>' + '\n<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + '</audio>';
 },
  file_picker_callback: function(callback, value, meta) {
    if (meta.filetype == 'image') {
      $('#upload').trigger('click');
      $('#upload').on('change', function() {
        var file = this.files[0];
        console.log(file);
        var reader = new FileReader();
        reader.onload = function(e) {
          callback(e.target.result, {
            alt: ''
          });
        };
        reader.readAsDataURL(file);
        console.log(reader);
      });
    }else if(meta.filetype == 'media'){      
        $('#uploadSound').trigger('click');
      $('#uploadSound').on('change', function() {
        var file = this.files[0];
        console.log(file);
        var reader = new FileReader();
        reader.onload = function(e) {
          callback(e.target.result, {
            alt: ''
          });
        };
        reader.readAsDataURL(file);
        console.log(reader);
      });
    }
  },
  templates: [{
    title: 'Test template 1',
    content: 'Test 1'
  }, {
    title: 'Test template 2',
    content: 'Test 2'
  }],
  setup: function (editor) {
    editor.addButton('fondo', {
      text: 'Fondo',
      icon: false,
      onclick: function () {
        editor.windowManager.open({
          title: 'Colorpicker',
          body: [{
        type: 'colorpicker',
        name: 'colorpicker',
            }],
            onsubmit: function(e) {
                    // Insert content when the window form is submitted
                    editor.dom.setStyle('tinymce', 'background-color', e.data.colorpicker);
                },
          width: 400,
          height: 200,
        });
        }
    });
    editor.addButton('Sonidos', {
      text: 'Sonidos',
      icon: false,
      onclick: function () {
        editor.windowManager.open({
          title: 'Sonidos',
          body: [{
        type: 'textbox', 
        subtype: 'file',
        name: 'sound',
            }],
            onsubmit: function(e) {
                    // Insert content when the window form is submitted
                    //editor.dom.setStyle('tinymce', 'background-color', e.data.colorpicker);
                    console.log(e.data);
                },
          width: 400,
          height: 200,
        });
        }
    });
    editor.addButton('grabadora', {
      text: 'Grabadora',
      icon: false,
      onclick: function () {
        editor.windowManager.open({
          title: 'Grabadora',
          url: 'http://localhost/elearning/qbano/Views/administrativo/voz/index/example_simple_exportwav.html',
            onsubmit: function(e) {
                    // Insert content when the window form is submitted
                    console.log('parametro tynice');
                    console.log(e.data.grab);
                },
          width: 400,
          height: 200,
          buttons: [{
          text: 'Close',
          onclick: function(e) {
                    // Insert content when the window form is submitted
                    console.log('parametro tynice');
                    console.log(editor.windowManager.windows);
                }
        }]
        },{
          arg2: "Hello world"
        });
        }

    });
    editor.addButton('Juegos', {
      text: 'Juegos',
      icon: false,
      onclick: function () {
        tinymce.activeEditor.setContent('<iframe src="http://localhost/elearning/juegos/juego1/HTML5/" ></iframe>');
/*        alert('Juegos');
*/        }

    });
  },



  content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ]
});

    var fileupload=$('#upload');
    $('#upload').replaceWith(fileupload.clone(true));



}

function addDiapositiva() {
    if (isDiapositivaBlock == false) {
        countDiapositivas++;
        viewsdiapositivas[viewActivo] = getContentActiveEditor();
        viewActivo = countDiapositivas;
        var view = `<article id="viewdiapo_` + countDiapositivas + `" class="multimediaviews-View" onclick="changedViewDiapositiva(this)" style="cursor:pointer">
                `+ (countDiapositivas + 1) + `
            </article>`;
        var viewadd = $('#multimediaviews').find('#viewadd');
        refreshEditor();
        // console.log('array diapo');
        // console.log(viewsdiapositivas);
        viewadd.remove();
        $('#multimediaviews').append(view);
        $('#multimediaviews').append(viewadd);
    }
}

function activarDiapositiva(button) {
    isDiapositivaBlock = false;
    refreshEditor();
    tinymce.get('multimediaeditor').setContent(viewsdiapositivas[viewActivo]);
    console.log(button);
    $(button).replaceWith('<button onclick="desactivarDiapositiva(this)" class="btn btn-info">Guardar Cambios</button>');
}
function desactivarDiapositiva(button) {
    isDiapositivaBlock = true;
    updateviewsdiapositivas();
    refreshEditor();
    tinymce.get('multimediaeditor').setContent(viewsdiapositivas[viewActivo]);

    var formpost = new FormData();
    formpost.append('action', "saveInfoDiapositivas");
    formpost.append('listadiapositivas', JSON.stringify(viewsdiapositivas));
    formpost.append('idTema', idTemaDiapositiva);
    console.log(JSON.stringify(viewsdiapositivas));
   /* alert('tamaño de la lista de envio');
    alert(viewsdiapositivas.length);*/

    $.ajax({
        type: 'POST',
        url: URL+"temas/editar/" + idTemaDiapositiva,
        data:
        formpost
        ,
        contentType: false,
        processData: false,

        success: function (data, textStatus, jqXHR) {
            console.log(data);
            $(button).replaceWith('<button onclick="activarDiapositiva(this)" class="controlVolver btn btn-info">Editar</button> ');
        },
        error: function (jqXHR, textStatus, errorThrown) {
/*            alert("didn't work!");
*/        }
    });

}

function guardarNuevoModulo(){
  var modulo=$('#newmodulo').find('.NewModuloInput');
  var moduloIn=modulo[0].value;
  console.log(modulo);
  console.log(moduloIn);
  var formpost = new FormData();

  formpost.append('action', "saveInfoModulo");
  formpost.append('nombre', moduloIn);
  console.log('id'+idCursoPHP);
  $.ajax({
      type: 'POST',
      url: URL+"modulos/agregar/"+idCursoPHP,
      data:formpost,
      contentType: false,
      processData: false,

      success: function (data, textStatus, jqXHR) {
          console.log(data);
          $(modulo)[0].value="";
          $.ajax({
                  url: URL+'scriptphp/getultimomodulo.php',
                  type: 'GET',
                  data: 'idCurso='+idCursoPHP,
                  success: function (data) {
                          // console.log(data);
                          $('#contenedoradministradormodulos').append(`<section class="containerModuleTheme">
                            <div class="ModuleTheme">
                              <label for="">Nombre Modulo:</label><input type="text" name="nombreModulo" class="nameModule" value="`+moduloIn+`" disabled>
                              <button onclick="activarDatosModulo(this, `+data+`)" class="btn btn-info" data-dismiss="modal" >Editar</button>
                            </div>
                          </section>
                          <a href="http://localhost/qbano/temas/agregar/`+data+`">Nuevo Tema</a>`);
                  },
                  error: function (e) {
                          //called when there is an error
                          //console.log(e.message);
                          console.log('mal get');
                  }
          });

      },
      error: function (jqXHR, textStatus, errorThrown) {
/*          alert("didn't work!");
*/      }
  });
}

var contq = numeroPreguntas;
var cont2= contq;
/*alert(cont2);
*/function addQuestion() {
  console.log('añadir');

        if (cont2 < 5) {
                var html = `<div class="question" id="questionTheme_` + contq + `">
                <span class="questionDelete" onclick="eliminarPregunta(this);">X</span>
              <select class="typeQuestion form-control" name="typequestion" onchange="isSelectedTypeQuestion(this)">
                <option value="" disabled>Elegir pregunta</option>
                <option value="1">Falso/Verdadero</option>
                <option value="2">Selección Multiple</option>
              </select>
              <input type="text" class="form-control" placeholder="Escribe tu Pregunta" name="question" >
              <div class="testTo">
                <ul class="testList">
                        <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_`+ contq + `[]" checked><p class="testName">Si</p></li>
                        <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_`+ contq + `[]" ><p class="testName">No</p></li>
                </ul>
              </div>
            </div>`
                $('#testContainer').append(html);
        }
        contq++;
        if(cont2<5){
            cont2++;
        }
}

function isSelectedTypeQuestion(str) {
        var CajaQuestion = str.parentNode;
        if (str.value == 1) {
                var id = $(CajaQuestion).attr('id');
                id = id.split('_');
                id = id[1];
                // alert(id);
                $(CajaQuestion).children(".testTo").remove();
                var html = `<div class="testTo">
                <ul class="testList">
                        <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_0[]" checked ><p class="testName">Si</p></li>
                        <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_0[]" ><p class="testName">No</p></li>
                </ul>
              </div>`
                $(CajaQuestion).append(html);
        } else if (str.value == 2) {
                $(CajaQuestion).children(".testTo").remove();
                var html = `<div class="testTo">
                <ul class="testList">
                    <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]" ><p class="testName"><input type="text" placeholder="Respuesta"/></p></li>
                    <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]" ><p class="testName"><input type="text" placeholder="Respuesta"/></p></li>
                    <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]" ><p class="testName"><input type="text" placeholder="Respuesta"/></p></li>
                    <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]" ><p class="testName"><input type="text" placeholder="Respuesta"/></p></li>
                </ul>
              </div>`
                $(CajaQuestion).append(html);
        }
}


function editarEvaluacion(id){
    console.log('pregunta');
  console.log(id);
  var testContainer = $('#testContainer').children('.question');
  var strquestions = []
  console.log('tamaño container');
  console.log(testContainer.length);
  for (var i = 0; i < testContainer.length; i++) {
/*      alert('for');
*/      console.log('dentro del for');
          var typeQuestion = $(testContainer[i]).find('.typeQuestion')[0].value;
          // alert('tipo'+typeQuestion);
          var textQuestion = $(testContainer[i]).find('input[name=question]')[0].value;
          // alert('text'+textQuestion);
          var ValueResponsesQuestion;
          var responsesQuestion;
          if (typeQuestion == 1) {
                  responsesQuestion = $(testContainer[i]).find('.questionAsk');
          } else if (typeQuestion == 2) {
                  responsesQuestion = $(testContainer[i]).find('.testName');
                  ValueResponsesQuestion = $(testContainer[i]).find('input[name="checkboxquestiontrue[]"]');
                  // console.log(ValueResponsesQuestion);

          }
          // alert('res'+responsesQuestion.length);
          var responsesQuestionList = []
          if (typeQuestion == 1) {
                  if (responsesQuestion[0].checked == true) {
/*                          alert('yeah');
*/                          strquestions.push({ typeQuestion: typeQuestion, textQuestion: textQuestion, responsesQuestionList: 1 });
                  }
                  if (responsesQuestion[1].checked == true) {
/*                          alert('no');
*/                          strquestions.push({ typeQuestion: typeQuestion, textQuestion: textQuestion, responsesQuestionList: 0 });
                  }
          } else if (typeQuestion == 2) {
                  for (var j = 0; j < responsesQuestion.length; j++) {
                          responsesQuestionList.push({ responsesQuestion: responsesQuestion[j].firstChild.value, valueResponsesQuestion: (ValueResponsesQuestion[j].checked) ? 1 : 0 });
                  }
                  strquestions.push({ typeQuestion: typeQuestion, textQuestion: textQuestion, responsesQuestionList: responsesQuestionList });
          }
  }
  console.log('lista preguntas');
  console.log(strquestions);

  var formpost = new FormData();

  formpost.append('action', 'editarEvaluacion');
  formpost.append('idTema', id);
  formpost.append('responsesQuestion', JSON.stringify(strquestions));
/*  alert('antes de validad');
*/if(!validateQuestions()){
/*    alert('entro a guardar');
*/    console.log('ajax');
  $.ajax({
          url: '',
          type: 'POST',
          data: formpost,
          contentType: false,
          processData: false,
          success: function (data) {
            console.log(data);
            alert('Guardado Con Exito');
         },
          error: function (e) {
                  //called when there is an error
                  //console.log(e.message);
                  console.log('mal get');
          }
  });
}

}

function eliminarPregunta(elem){
    console.log('eleminar pregunta');
    console.log(elem);
    var elemento=$(elem).parent('.question');
    console.log($(elemento));
    $(elemento).remove();
    cont2--;
/*    alert(cont2);
*/}

function cursoMarcha(){
    alert('Este Curso no se Puede Editar porque se Encuentra dentro de las Fechas de Estudio');
}
