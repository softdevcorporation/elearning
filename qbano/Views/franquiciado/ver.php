<?php
  $listaCursos = $cursos->index();
  $curso = $datos;
  $modulos =  $cursos->getModules($curso['id_curso']);
?>
<script>
  var idCursoPHP = "<?php echo $curso['id_curso'] ?>";
</script>

<!-- <a href="<?php echo URL; ?>estudiante" class="controlVolver btn btn-info">volver</a>
 -->
<?php
  while($row = $modulos->fetch(\PDO::FETCH_ASSOC)){

?>

<!-- <div>
<section class="containerModuleTheme">
    <div class="ModuleTheme">
      <label for="">Nombre Modulo:</label><input type="text" name="nombreModulo" class="nameModule" value="<?php echo $row['nombre'] ?>" disabled>
    </div>
</section>
</div> -->

<?php
  if($curso['id_tipo_curso']==1){
  $temas =  $cursos->getTemas($row['id_modulo']);
  while($row = $temas->fetch(\PDO::FETCH_ASSOC)){
    $existPreguntas=$cursos->existPreguntas($row['id_tema']);
    if($existPreguntas){
      ?>
      <script>
        var existPreguntas = true;
      </script>
      <?php
    }else{
      ?>
      <script>
      var existPreguntas = false;
    </script>
      <?php
    }
    ?>
    <script>
      var idTemaExpress = "<?php echo $row['id_tema']?>";
    </script>
    <?php
        $isCalificacion=$cursos->getCalificaciones($row['id_tema']);
        $isCalificacion=$isCalificacion->fetch(\PDO::FETCH_ASSOC);
        //print_r($isCalificacion);
        if($isCalificacion==null){
          $isCalificacion['numero_intentos']=0;
        }
        //print_r($isCalificacion);
        $diapositivas = $cursos->getDiapositivas($row['id_tema']);
        $listDiapositivasEdit=array();
        while($diapo = $diapositivas->fetch(\PDO::FETCH_ASSOC)){
              $listDiapositivasEdit[]=$diapo;

        }
        // print_r($listDiapositivasEdit[0]);

        ?>
          <script>
          var numeroIntentosCalificacion = <?php echo $isCalificacion['numero_intentos'] ?>;
           var listDiapositivasEdit = <?php echo json_encode($listDiapositivasEdit) ?>;
          </script>

<div style="text-align:center;">
<div id="divevaluacion" style="text-align:center;display:inline-block">
</div>
<?php
  if($isCalificacion['numero_intentos']>=3){
?>
<span>Ya has Finalizado el Tema</span>
<?php
  }
?>
</div>
<div>
   <section  id="SeccionMultimedia" class="SeccionMultimediaEditorView" style="text-align:center;">
          <!--
          <section id="multimediaviews">
            <article id="viewdiapo_0" class="multimediaviews-View" onclick="changedViewDiapositiva(this)" style="cursor:pointer">
                1
            </article>
            <article id="viewadd" class="multimediaviews-View" >
                <a onclick="addDiapositiva()">añadir</a>
            </article>
          </section>-->
          <div id="ContenedorEditor">
      <textarea id="multimediaeditor" class="multimediaeditor" >
        <?php
          $cont=$listDiapositivasEdit[0];
          print_r($cont['contenido']);
        ?>

      </textarea>
      <section>

      <a id="buttonback" onclick="backDiapositiva()" class="btn btn-info" style="margin-top:1.5em;">Atras</a>
      <a id="buttonnext" onclick="nextDiapositiva()" class="btn btn-info" style="margin-top:1.5em;">Siguiente</a>

      </section>

      </div>
      </section>
</div>


    </div>
  <?php
    }
  ?>
<?php
    }else if($curso['id_tipo_curso']==2){
      $temas =  $cursos->getTemas($row['id_modulo']);

      while($row = $temas->fetch(\PDO::FETCH_ASSOC)){
        ?>
      <section class="containerModuleTheme">
      <div class="ModuleTheme">
      <label for="">Nombre Tema:</label><input type="text" name="nameTheme" class="nameTheme" value="<?php echo $row['nombre'] ?>" disabled>
      <a href="<?php echo URL ?>temas/verfranquiciado/<?php echo $row['id_tema'] ?>">Ver</a>
      </div>
      </section>
<?php
      }
    }
  }
?>
  <script type="text/javascript" src="<?php echo URL; ?>js/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo URL; ?>Config/configuracion.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>plugins/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>plugins/tinymce/init-tinymce.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>Views/franquiciado/js/presentaciondiapositiva.js"></script>

    <script type="text/javascript">
      diapositivaInit();
    </script>



<script>

  tinymce.init({
    selector: 'textarea.multimediaeditor',
    height: 500,
    readonly : true,
    toolbar: false,
    menubar: false,
    statusbar: false,
    theme: 'modern',
    content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ]
   });

 function showPresentation(){
  $('#SeccionMultimedia').slideToggle();
  }
</script>
