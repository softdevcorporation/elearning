function enviarDatosTema() {
        //div donde se mostrará lo resultados
        //divResultado = document.getElementById('resultado');
        //recogemos los valores de los inputs

        nombreTemmaIn = $('input[name=nombretema]')
        nombreTema = nombreTemmaIn[0].value;
        sendToIn = $('input[name="sendTo[]"]')

        diapositivasTemasIn = $('.multimediaeditor');

        var testContainer = $('#testContainer').children('.question');
        var strquestions = []
        // alert(testContainer.length);
        for (var i = 0; i < testContainer.length; i++) {
                var typeQuestion = $(testContainer[i]).find('.typeQuestion')[0].value;
                // alert('tipo'+typeQuestion);
                var textQuestion = $(testContainer[i]).find('input[name=question]')[0].value;
                // alert('text'+textQuestion);
                var ValueResponsesQuestion;
                var responsesQuestion;
                if (typeQuestion == 1) {
                        responsesQuestion = $(testContainer[i]).find('.questionAsk');
                } else if (typeQuestion == 2) {
                        responsesQuestion = $(testContainer[i]).find('.testName');
                        ValueResponsesQuestion = $(testContainer[i]).find('input[name="checkboxquestiontrue[]"]');
                        // console.log(ValueResponsesQuestion);

                }
                // alert('res'+responsesQuestion.length);
                var responsesQuestionList = []
                if (typeQuestion == 1) {
                        if (responsesQuestion[0].checked == true) {
                                // console.log('yeah');
                                strquestions.push({ typeQuestion: typeQuestion, textQuestion: textQuestion, responsesQuestionList: 1 });
                        }
                        if (responsesQuestion[1].checked == true) {
                                // console.log('no');
                                strquestions.push({ typeQuestion: typeQuestion, textQuestion: textQuestion, responsesQuestionList: 0 });
                        }
                } else if (typeQuestion == 2) {
                        for (var j = 0; j < responsesQuestion.length; j++) {
                                responsesQuestionList.push({ responsesQuestion: responsesQuestion[j].firstChild.value, valueResponsesQuestion: (ValueResponsesQuestion[j].checked) ? 1 : 0 });
                        }
                        strquestions.push({ typeQuestion: typeQuestion, textQuestion: textQuestion, responsesQuestionList: responsesQuestionList });
                }
        }
        updateviewsdiapositivas();
        var listaDiapositivas = []
        for (var i = 0; i < viewsdiapositivas.length; i++) {

                // alert('content');
                // alert(viewsdiapositivas[i]);
                listaDiapositivas.push({ diapositiva: viewsdiapositivas[i] });
        }
        // console.log('diapositivas');
        // console.log(listaDiapositivas);



        // if (!isEmpty(nombreTemmaIn) && !isEmpty(nombreModuloIn) && !isEmpty(tipoCursoIn) && !isEmpty(fechapublicacionIn) && !isEmpty(fechafinIn) && !isEmpty(fechainicioIn) && !isArrayEmpty(sendToIn)
        //         && !isEmpty(certificaIn) && !isEmpty(descripcionIn) && !isEmpty(nomIn)) {

        var formpost = new FormData();

        formpost.append('nombreTema', nombreTema);
        formpost.append('responsesQuestion', JSON.stringify(strquestions));
        formpost.append('listaDiapositivas', JSON.stringify(listaDiapositivas));








        $.ajax({
                type: 'POST',
                url: "",
                data:
                formpost
                ,
                contentType: false,
                processData: false,

                success: function (data, textStatus, jqXHR) {
                  console.log(data);
                      location.href = "http://localhost/qbano/administrativo/editar/"+idCursoPHP;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                        alert("didn't work!");
                }
        });
        // }

        function isEmpty(input) {
                var str = input[0].value;
                str = str.trim();
                // alert(str);
                if (str == null || str == "" || str == undefined) {
                        input[0].focus();
                        var parent = $(input[0]).parent(".inputFormValidation");
                        $(parent).children("span").remove();
                        $(parent).append('<span style="color:red;display:block">Por Favor Rellene Este Campo</span>');
                }
        }

        function isArrayEmpty(arr) {
                lst = new Array();

                for (var i = 0; i < arr.length; i++) {
                        if (arr[i].checked) {
                                lst.push(arr[i].value);
                        }
                }
                if (lst == null || lst.length <= 0) {
                        arr[0].focus();
                        var parent = $(arr).parents(".inputFormValidation");
                        $(parent).children("span").remove();
                        $(parent).append('<span style="color:red;display:block">Por Favor Seleccione Una Opcion</span>');
                }
        }



}


function isSelectedTypeQuestion(str) {
        var CajaQuestion = str.parentNode;
        if (str.value == 1) {
                var id = $(CajaQuestion).attr('id');
                id = id.split('_');
                id = id[1];
                // alert(id);
                $(CajaQuestion).children(".testTo").remove();
                var html = `<div class="testTo">
                <ul class="testList">
                        <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_0[]" checked ><p class="testName">Si</p></li>
                        <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_0[]" ><p class="testName">No</p></li>
                </ul>
              </div>`
                $(CajaQuestion).append(html);
        } else if (str.value == 2) {
                $(CajaQuestion).children(".testTo").remove();
                var html = `<div class="testTo">
                <ul class="testList">
                    <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]" ><p class="testName"><input type="text" placeholder="Respuesta"/></p></li>
                    <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]" ><p class="testName"><input type="text" placeholder="Respuesta"/></p></li>
                    <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]" ><p class="testName"><input type="text" placeholder="Respuesta"/></p></li>
                    <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]" ><p class="testName"><input type="text" placeholder="Respuesta"/></p></li>
                </ul>
              </div>`
                $(CajaQuestion).append(html);
        }
}

var contq = 0;
function addQuestion() {
        contq++;
        if (contq < 5) {
                var html = `<div class="question" id="questionTheme_` + contq + `">
              <select class="typeQuestion" name="typequestion" onchange="isSelectedTypeQuestion(this)">
                <option value="" disabled>Elegir pregunta</option>
                <option value="1">Falso/Verdadero</option>
                <option value="2">Selección Multiple</option>
              </select>
              <input type="text" placeholder="Escribe tu Pregunta" name="question" >
              <div class="testTo">
                <ul class="testList">
                        <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_`+ contq + `[]" checked><p class="testName">Si</p></li>
                        <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_`+ contq + `[]" ><p class="testName">No</p></li>
                </ul>
              </div>
            </div>`
                $('#testContainer').append(html);
        }
}

function onchangeInputValidation(input) {
        var parent = $(input).parent(".inputFormValidation");
        $(parent).children("span").remove();
}

function onchangeOptionValidation(arr) {
        var parent = $(arr).parents(".inputFormValidation");
        $(parent).children("span").remove();
}

var countDiapositivas = 0;
var viewsdiapositivas = [];
var viewActivo = 0;

function updateviewsdiapositivas() {
        viewsdiapositivas[viewActivo] = getContentActiveEditor();
}

function addDiapositiva() {
        countDiapositivas++;
        viewsdiapositivas[viewActivo] = getContentActiveEditor();
        viewActivo = countDiapositivas;
        var view = `<article id="viewdiapo_` + countDiapositivas + `" class="multimediaviews-View" onclick="changedViewDiapositiva(this)" style="cursor:pointer">
                `+ (countDiapositivas + 1) + `
            </article>`;
        var viewadd = $('#multimediaviews').find('#viewadd');
        refreshEditor();
        // console.log('array diapo');
        // console.log(viewsdiapositivas);
        viewadd.remove();
        $('#multimediaviews').append(view);
        $('#multimediaviews').append(viewadd);
}


function changedViewDiapositiva(str) {
        updateviewsdiapositivas();
        var id = $(str).attr("id");
        id = id.split('_');
        id = id[1];
        alert(id);
        viewActivo = id;
        refreshEditor();
        tinymce.get('multimediaeditor').setContent(viewsdiapositivas[id]);
}


function getContentActiveEditor() {
        // console.log(tinyMCE.activeEditor.getContent());
        return tinyMCE.activeEditor.getContent();
}

function refreshEditor() {
        tinymce.get('multimediaeditor').setContent('');
        tinymce.remove();
        tinymce.init({
                selector: 'textarea.multimediaeditor',
                height: 500,
                theme: 'modern',
                language : "es_MX",
                paste_data_images: true,
                statusbar: false,
                plugins: [
                  "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                  "searchreplace wordcount visualblocks visualchars code fullscreen",
                  "insertdatetime media nonbreaking save table contextmenu directionality",
                  "emoticons template paste textcolor colorpicker textpattern"
                ],
                toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                toolbar2: "print preview media | forecolor backcolor emoticons",
                image_advtab: true,
                file_picker_callback: function(callback, value, meta) {
                  if (meta.filetype == 'image') {
                    $('#upload').trigger('click');
                    $('#upload').on('change', function() {
                      var file = this.files[0];
                      console.log(file);
                      var reader = new FileReader();
                      reader.onload = function(e) {
                        callback(e.target.result, {
                          alt: ''
                        });
                      };
                      reader.readAsDataURL(file);
                      console.log(reader);
                    });
                  }
                },
                templates: [{
                  title: 'Test template 1',
                  content: 'Test 1'
                }, {
                  title: 'Test template 2',
                  content: 'Test 2'
                }]
        });
}
