function activarDatosCursos(button) {
    nomIn = $('input[name=nombreCurso]');
    nomIn.attr('disabled', false);
    descripcionIn = $('textarea[name=descripcion]');
    descripcionIn.attr('disabled', false);
    certificaIn = $('input[name=certifica]')
    certificaIn.attr('disabled', false);
    fechainicioIn = $('input[name=fechainicio]')
    fechainicioIn.attr('disabled', false);
    fechafinIn = $('input[name=fechafin]')
    fechafinIn.attr('disabled', false);
    fechapublicacionIn = $('input[name=fechapublicacion]')
    fechapublicacionIn.attr('disabled', false);
    var boton = $(button);
    boton.replaceWith('<button onclick="enviarDatosCursos(this)">Guardar Cambios</button>');
}

function enviarDatosCursos(button) {
    nomIn = $('input[name=nombreCurso]');
    nombreCurso = nomIn[0].value;
    nomIn.attr('disabled', false);
    descripcionIn = $('textarea[name=descripcion]');
    descripcion = descripcionIn[0].value;
    descripcionIn.attr('disabled', false);
    certificaIn = $('input[name=certifica]')
    certifica = 0;
    console.log(certificaIn[0]);
    console.log(certificaIn[1]);
    if (certificaIn[0].checked) {
        certifica = 0;
    } else {
        certifica = 1;
    }
    alert('certifica'+certifica);
    certificaIn.attr('disabled', false);
    fechainicioIn = $('input[name=fechainicio]')
    fechainicio = fechainicioIn[0].value;
    fechainicioIn.attr('disabled', false);
    fechafinIn = $('input[name=fechafin]')
    fechafin = fechafinIn[0].value;
    fechafinIn.attr('disabled', false);
    fechapublicacionIn = $('input[name=fechapublicacion]')
    fechapublicacion = fechapublicacionIn[0].value;
    fechapublicacionIn.attr('disabled', false);
    var formpost = new FormData();

    formpost.append('action', "saveInfoCurso");
    formpost.append('nombreCurso', nombreCurso);
    formpost.append('descripcion', descripcion);
    formpost.append('certifica', certifica);
    formpost.append('fechainicio', fechainicio);
    formpost.append('fechafin', fechafin);
    formpost.append('fechapublicacion', fechapublicacion);
    alert('id curso' + idCursoPHP);
    $.ajax({
        type: 'POST',
        url: "http://localhost/qbano/cursos/editar/".idCursoPHP,
        data:
        formpost
        ,
        contentType: false,
        processData: false,

        success: function (data, textStatus, jqXHR) {
            // console.log(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("didn't work!");
        }
    });
    restablecerDatosCursos(button);
}

function restablecerDatosCursos(button) {
    nomIn = $('input[name=nombreCurso]');
    nomIn.attr('disabled', true);
    descripcionIn = $('textarea[name=descripcion]');
    descripcionIn.attr('disabled', true);
    certificaIn = $('input[name=certifica]')
    certificaIn.attr('disabled', true);
    fechainicioIn = $('input[name=fechainicio]')
    fechainicioIn.attr('disabled', true);
    fechafinIn = $('input[name=fechafin]')
    fechafinIn.attr('disabled', true);
    fechapublicacionIn = $('input[name=fechapublicacion]')
    fechapublicacionIn.attr('disabled', true);
    var boton = $(button);
    boton.replaceWith('<button onclick="activarDatosCursos(this)">Editar</button>');
}

function activarDatosModulo(button, id) {
    modulo = $(button).parent().find('input[name=nombreModulo]');
    modulo.attr('disabled', false);
    var boton = $(button);
    boton.replaceWith('<button onclick="enviarDatosModulo(this, ' + id + ')">Guardar Cambios</button>');
}

function enviarDatosModulo(button, id) {
    modulo = $(button).parent().find('input[name=nombreModulo]');
    moduloValue = modulo[0].value;

    var formpost = new FormData();

    formpost.append('action', "saveInfoModulo");
    formpost.append('modulo', moduloValue);
    formpost.append('idModulo', id);

    $.ajax({
        type: 'POST',
        url: "http://localhost/qbano/cursos/editar/".idCursoPHP,
        data:
        formpost
        ,
        contentType: false,
        processData: false,

        success: function (data, textStatus, jqXHR) {
            // alert(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("didn't work!");
        }
    });
    restablecerDatosModulo($(button), id);
}

function restablecerDatosModulo(button, id) {
    modulo = $(button).parent().find('input[name=nombreModulo]');
    modulo.attr('disabled', true);
    var boton = $(button);
    var a = id;
    a = 'onclick="activarDatosModulo(this,' + a + ' )"';
    boton.replaceWith('<button ' + a + '>Editar</button>');
}

function activarDatosTema(button, id) {
    tema = $(button).parent().find('input[name=nameTheme]');
    tema.attr('disabled', false);
    var boton = $(button);
    boton.replaceWith('<button onclick="enviarDatosTema(this, ' + id + ')">Guardar Cambios</button>');
}

function enviarDatosTema(button, id) {
    tema = $(button).parent().find('input[name=nameTheme]');
    temaValue = tema[0].value;
    alert('miguel' + temaValue);

    var formpost = new FormData();

    formpost.append('action', "saveInfoTema");
    formpost.append('nombre', temaValue);
    formpost.append('idTema', id);

    $.ajax({
        type: 'POST',
        url: "http://localhost/qbano/temas/editar/".id,
        data:
        formpost
        ,
        contentType: false,
        processData: false,

        success: function (data, textStatus, jqXHR) {
            alert(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("didn't work!");
        }
    });
    restablecerDatosTema($(button), id);
}

function restablecerDatosTema(button, id) {
    tema = $(button).parent().find('input[name=nameTheme]');
    tema.attr('disabled', true);
    var boton = $(button);
    var a = id;
    a = 'onclick="activarDatosTema(this,' + a + ' )"';
    boton.replaceWith('<button ' + a + '>Editar</button>');
}

var countDiapositivas = 0;
var viewsdiapositivas = [];
var viewActivo = 0;
var isDiapositivaBlock = true;
var idTemaDiapositiva;


function diapositivaInit() {

    var caja = $('#multimediaviews');
    caja.empty();
    listDiapositivasEdit = listDiapositivasEdit;
    console.log('lista diapo' + listDiapositivasEdit + 'tamaño' + listDiapositivasEdit.length);
    for (var i = 0; i < listDiapositivasEdit.length; i++) {
        viewsdiapositivas[i] = listDiapositivasEdit[i].contenido;
        idTemaDiapositiva = listDiapositivasEdit[i].id_tema;
        countDiapositivas = listDiapositivasEdit.length - 1;
        caja.append(`<article id="viewdiapo_` + i + `" class="multimediaviews-View" onclick="changedViewDiapositiva(this)" style="cursor:pointer">
                ` + (i + 1) + `
            </article>`);
    }
    caja.append(`<article id="viewadd" class="multimediaviews-View" >
                <a onclick="addDiapositiva()">añadir</a>
            </article>`);
    // alert(viewsdiapositivas.length);
}

function getContentActiveEditor() {
    // console.log(tinyMCE.activeEditor.getContent());
    return tinyMCE.activeEditor.getContent();
}

function updateviewsdiapositivas() {
    viewsdiapositivas[viewActivo] = getContentActiveEditor();
}

function changedViewDiapositiva(str) {
    updateviewsdiapositivas();
    var id = $(str).attr("id");
    id = id.split('_');
    id = id[1];
    alert(id);
    viewActivo = id;
    refreshEditor();
    tinymce.get('multimediaeditor').setContent(viewsdiapositivas[id]);
}

function refreshEditor() {
    tinymce.get('multimediaeditor').setContent('');
    tinymce.remove();
    tinymce.init({
        selector: 'textarea.multimediaeditor',
        height: 500,
        theme: 'modern',
        readonly: isDiapositivaBlock,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });
}

function addDiapositiva() {
    if (isDiapositivaBlock == false) {
        countDiapositivas++;
        viewsdiapositivas[viewActivo] = getContentActiveEditor();
        viewActivo = countDiapositivas;
        var view = `<article id="viewdiapo_` + countDiapositivas + `" class="multimediaviews-View" onclick="changedViewDiapositiva(this)" style="cursor:pointer">
                `+ (countDiapositivas + 1) + `
            </article>`;
        var viewadd = $('#multimediaviews').find('#viewadd');
        refreshEditor();
        // console.log('array diapo');
        // console.log(viewsdiapositivas);
        viewadd.remove();
        $('#multimediaviews').append(view);
        $('#multimediaviews').append(viewadd);
    }
}

function activarDiapositiva(button) {
    isDiapositivaBlock = false;
    refreshEditor();
    tinymce.get('multimediaeditor').setContent(viewsdiapositivas[viewActivo]);
    console.log(button);
    $(button).replaceWith('<button onclick="desactivarDiapositiva(this)">Guardar Cambios</button>');
}
function desactivarDiapositiva(button) {
    isDiapositivaBlock = true;
    updateviewsdiapositivas();
    refreshEditor();
    tinymce.get('multimediaeditor').setContent(viewsdiapositivas[viewActivo]);

    var formpost = new FormData();
    formpost.append('action', "saveInfoDiapositivas");
    formpost.append('listadiapositivas', JSON.stringify(viewsdiapositivas));
    formpost.append('idTema', idTemaDiapositiva);
    alert('tamaño de la lista de envio');
    alert(viewsdiapositivas.length);

    $.ajax({
        type: 'POST',
        url: "http://localhost/qbano/temas/editar/" + idTemaDiapositiva,
        data:
        formpost
        ,
        contentType: false,
        processData: false,

        success: function (data, textStatus, jqXHR) {
            alert(data);
            $(button).replaceWith('<button onclick="activarDiapositiva(this)">Editar</button> ');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("didn't work!");
        }
    });

}
