//Función para recoger los datos del formulario y enviarlos por post
function enviarDatosCursos() {

        //div donde se mostrará lo resultados
        //divResultado = document.getElementById('resultado');
        //recogemos los valores de los inputs
        nomIn = $('input[name=nombreCurso]');
        nombreCurso = nomIn[0].value;
        descripcionIn = $('textarea[name=descripcion]');
        descripcion = descripcionIn[0].value;
        certificaIn = $('input[name=certifica]')
        certifica = certificaIn[0].value;
        fechainicioIn = $('input[name=fechainicio]')
        fechainicio = fechainicioIn[0].value;
        fechafinIn = $('input[name=fechafin]')
        fechafin = fechafinIn[0].value;
        fechapublicacionIn = $('input[name=fechapublicacion]')
        fechapublicacion = fechapublicacionIn[0].value;
        tipoCursoIn = $('input[name=tipoCurso]')
        tipoCurso = tipoCursoIn[0].checked;
        nombreModuloIn = $('input[name=nombremodulo]')
        nombreModulo = nombreModuloIn[0].value;
        nombreTemmaIn = $('input[name=nombretema]')
        nombreTema = nombreTemmaIn[0].value;
        sendToIn = $('input[name="sendTo[]"]')
        sendTo = new Array();
        inputFileImageIn = $('#avatarcurso');
        // console.log(inputFileImageIn);
        inputFileImage = inputFileImageIn.prop('files')[0]
        filedraftIn = $('#filedraft');
        filedraft = filedraftIn.prop('files')[0]
        fileguiaformatoIn = $('#fileguiaformato');
        fileguiaformato = fileguiaformatoIn.prop('files')[0]
        fileevaluacionIn = $('#fileevaluacion');
        fileevaluacion = fileevaluacionIn.prop('files')[0]
        filetextoayudaIn = $('#filetextoayuda');
        filetextoayuda = filetextoayudaIn.prop('files')[0]
        diapositivasTemasIn = $('.multimediaeditor');
        // console.log(diapositivasTemasIn[0].value);
        for (var i = 0; i < sendToIn.length; i++) {
                if (sendToIn[i].checked) {
                        sendTo.push(sendToIn[i].value);
                }
        }

        var testContainer = $('#testContainer').children('.question');
        var strquestions = []
        // alert(testContainer.length);
        for (var i = 0; i < testContainer.length; i++) {
                var typeQuestion = $(testContainer[i]).find('.typeQuestion')[0].value;
                // alert('tipo'+typeQuestion);
                var textQuestion = $(testContainer[i]).find('input[name=question]')[0].value;
                // alert('text'+textQuestion);
                var ValueResponsesQuestion;
                var responsesQuestion;
                if (typeQuestion == 1) {
                        responsesQuestion = $(testContainer[i]).find('.questionAsk');
                } else if (typeQuestion == 2) {
                        responsesQuestion = $(testContainer[i]).find('.testName');
                        ValueResponsesQuestion = $(testContainer[i]).find('input[name="checkboxquestiontrue[]"]');
                        // console.log(ValueResponsesQuestion);

                }
                // alert('res'+responsesQuestion.length);
                var responsesQuestionList = []
                if (typeQuestion == 1) {
                        if (responsesQuestion[0].checked == true) {
                                // console.log('yeah');
                                strquestions.push({ typeQuestion: typeQuestion, textQuestion: textQuestion, responsesQuestionList: 0 });
                        }
                        if (responsesQuestion[1].checked == true) {
                                // console.log('no');
                                strquestions.push({ typeQuestion: typeQuestion, textQuestion: textQuestion, responsesQuestionList: 1 });
                        }
                } else if (typeQuestion == 2) {
                        for (var j = 0; j < responsesQuestion.length; j++) {
                                responsesQuestionList.push({ responsesQuestion: responsesQuestion[j].firstChild.value, valueResponsesQuestion: (ValueResponsesQuestion[j].checked) ? 1 : 0 });
                        }
                        strquestions.push({ typeQuestion: typeQuestion, textQuestion: textQuestion, responsesQuestionList: responsesQuestionList });
                }
        }
        updateviewsdiapositivas();
        var listaDiapositivas = []
        for (var i = 0; i < viewsdiapositivas.length; i++) {

                // alert('content');
                // alert(viewsdiapositivas[i]);
                listaDiapositivas.push({ diapositiva: viewsdiapositivas[i] });
        }
        // console.log('diapositivas');
        // console.log(listaDiapositivas);


        if (tipoCurso == true) {
                tipoCurso = 1;
        } else {
                tipoCurso = 2;
        }


        if (!validateQuestions() & !isEmpty(nombreTemmaIn) & !isEmpty(nombreModuloIn) & !isEmpty(tipoCursoIn) & !isEmpty(fechapublicacionIn) & !isEmpty(fechafinIn) & !isEmpty(fechainicioIn) & !isArrayEmpty(sendToIn)
                & !isEmpty(certificaIn) & !isEmpty(descripcionIn) & !isEmpty(nomIn)) {

                alert('pos');
                var formpost = new FormData();

                formpost.append('nombreCurso', nombreCurso);
                formpost.append('descripcion', descripcion);
                formpost.append('certifica', certifica);
                formpost.append('fechainicio', fechainicio);
                formpost.append('fechafin', fechafin);
                formpost.append('fechapublicacion', fechapublicacion);
                formpost.append('tipoCurso', tipoCurso);
                formpost.append('nombreModulo', nombreModulo);
                formpost.append('nombreTema', nombreTema);
                formpost.append('sendTo', JSON.stringify(sendTo));
                formpost.append('responsesQuestion', JSON.stringify(strquestions));
                formpost.append('inputFileImage', inputFileImage);
                formpost.append('filedraft', filedraft);
                formpost.append('fileguiaformato', fileguiaformato);
                formpost.append('fileevaluacion', fileevaluacion);
                formpost.append('filetextoayuda', filetextoayuda);
                formpost.append('listaDiapositivas', JSON.stringify(listaDiapositivas));

                $.ajax({
                        type: 'POST',
                        url: "",
                        data:
                        formpost
                        ,
                        contentType: false,
                        processData: false,

                        success: function (data, textStatus, jqXHR) {
                                $.ajax({
                                        url: '../scriptphp/getultimocurso.php',
                                        type: 'GET',
                                        // data: 'twitterUsername=jquery4u',
                                        success: function (data) {
                                                //called when successful
                                                var datos = data.split("-");
                                                location.href = "http://localhost/qbano/administrativo/editar/" + datos[0];
                                                console.log('bien get' + datos);
                                        },
                                        error: function (e) {
                                                //called when there is an error
                                                //console.log(e.message);
                                                console.log('mal get');
                                        }
                                });
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                                alert("didn't work!");
                        }
                });
        }

        function isEmpty(input) {
                var str = input[0].value;
                str = str.trim();
                // alert(str);
                if (str == null || str == "" || str == undefined) {
                        input[0].focus();
                        var parent = $(input[0]).parent(".inputFormValidation");
                        $(parent).children("span").remove();
                        $(parent).append('<span style="color:red;display:block">Por Favor Rellene Este Campo</span>');
                        return true;
                }
        }

        function isArrayEmpty(arr) {
                lst = new Array();

                for (var i = 0; i < arr.length; i++) {
                        if (arr[i].checked) {
                                lst.push(arr[i].value);
                        }
                }
                if (lst == null || lst.length <= 0) {
                        arr[0].focus();
                        var parent = $(arr).parents(".inputFormValidation");
                        $(parent).children("span").remove();
                        $(parent).append('<span style="color:red;display:block">Por Favor Seleccione Una Opcion</span>');
                }
        }

        function validateQuestions() {
                var cajaTest = $('#testContainer');
                cajapregunta = cajaTest.find('.question');
                console.log('preguntaaas');
                console.log(cajapregunta);
                for (var i = 0; i < cajapregunta.length; i++) {
                        cajapreguntaitem = cajapregunta[i];
                        console.log(cajapreguntaitem);
                        tipopregunta = $(cajapreguntaitem).find('select[name=typequestion]');
                        tipopregunta = tipopregunta[0];
                        tipopregunta = tipopregunta.value;
                        console.log('tipopregunta');
                        console.log(tipopregunta);
                        pregunta = $(cajapreguntaitem).find('input[name=question]');
                        console.log('text pregunta')
                        console.log(pregunta[0]);
                        console.log(pregunta[0].value);
                        respuesta = $(cajapreguntaitem).find('.questionAsk');
                        console.log('respuestas')
                        console.log(respuesta);


                        // console.log(cajapregunta[i]);

                        if (pregunta[0].value == null || pregunta[0].value == undefined || (pregunta[0].value).trim() == "") {
                                console.log('valor texto pregunta')
                                return true;
                        }

                        console.log(respuesta[0].checked);
                        console.log(respuesta[1].checked);
                        if (tipopregunta == 1) {
                                if (!respuesta[0].checked && !respuesta[1].checked) {
                                        console.log('llene uno de los campos');
                                        return true;
                                }
                        } else if (tipopregunta == 2) {
                                strrespuesta = $(cajapreguntaitem).find('input[name=strrespuesta]');
                                console.log(strrespuesta);
                                for(var x=0;x<strrespuesta.length;x++){
                                        if (strrespuesta[x].value == null || strrespuesta[x].value == undefined || (strrespuesta[x].value).trim() == "") {
                                                console.log('valor texto respuesta')
                                                return true;
                                        }
                                }
                                console.log('tipo 2');
                                if (!respuesta[0].checked && !respuesta[1].checked
                                        && !respuesta[2].checked && !respuesta[3].checked) {
                                        console.log('llene uno de los campos');
                                        return true;
                                }
                        }
                        console.log('--------------------------------------------------------');
                }
                return false;
        }
}

function isSelectedTypeQuestion(str) {
        var CajaQuestion = str.parentNode;
        if (str.value == 1) {
                var id = $(CajaQuestion).attr('id');
                id = id.split('_');
                id = id[1];
                // alert(id);
                $(CajaQuestion).children(".testTo").remove();
                var html = `<div class="testTo">
                <ul class="testList">
                        <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_0[]" checked ><p class="testName">Si</p></li>
                        <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_0[]" ><p class="testName">No</p></li>
                </ul>
              </div>`
                $(CajaQuestion).append(html);
        } else if (str.value == 2) {
                $(CajaQuestion).children(".testTo").remove();
                var html = `<div class="testTo">
                <ul class="testList">
                    <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]" ><p class="testName"><input type="text" name="strrespuesta" placeholder="Respuesta"/></p></li>
                    <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]" ><p class="testName"><input type="text" name="strrespuesta" placeholder="Respuesta"/></p></li>
                    <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]" ><p class="testName"><input type="text" name="strrespuesta" placeholder="Respuesta"/></p></li>
                    <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]" ><p class="testName"><input type="text" name="strrespuesta" placeholder="Respuesta"/></p></li>
                </ul>
              </div>`
                $(CajaQuestion).append(html);
        }
}

var contq = 0;
function addQuestion() {
        contq++;
        if (contq < 5) {
                var html = `<div class="question" id="questionTheme_` + contq + `">
              <select class="typeQuestion" name="typequestion" onchange="isSelectedTypeQuestion(this)">
                <option value="" disabled>Elegir pregunta</option>
                <option value="1">Falso/Verdadero</option>
                <option value="2">Selección Multiple</option>
              </select>
              <input type="text" placeholder="Escribe tu Pregunta" name="question" >
              <div class="testTo">
                <ul class="testList">
                        <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_`+ contq + `[]" checked><p class="testName">Si</p></li>
                        <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_`+ contq + `[]" ><p class="testName">No</p></li>
                </ul>
              </div>
            </div>`
                $('#testContainer').append(html);
        }
}

function onchangeInputValidation(input) {
        var parent = $(input).parent(".inputFormValidation");
        $(parent).children("span").remove();
}

function onchangeOptionValidation(arr) {
        var parent = $(arr).parents(".inputFormValidation");
        $(parent).children("span").remove();
}

function getContentActiveEditor() {
        // console.log(tinyMCE.activeEditor.getContent());
        return tinyMCE.activeEditor.getContent();
}

function refreshEditor() {
        tinymce.get('multimediaeditor').setContent('');
        tinymce.remove();
        tinymce.init({
                selector: 'textarea.multimediaeditor',
                height: 500,
                theme: 'modern',
                plugins: [
                        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                        'searchreplace wordcount visualblocks visualchars code fullscreen',
                        'insertdatetime media nonbreaking save table contextmenu directionality',
                        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
                ],
                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
                image_advtab: true,
                templates: [
                        { title: 'Test template 1', content: 'Test 1' },
                        { title: 'Test template 2', content: 'Test 2' }
                ],
                content_css: [
                        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                        '//www.tinymce.com/css/codepen.min.css'
                ]
        });
}



var countDiapositivas = 0;
var viewsdiapositivas = [];
var viewActivo = 0;

function updateviewsdiapositivas() {
        viewsdiapositivas[viewActivo] = getContentActiveEditor();
}

function addDiapositiva() {
        countDiapositivas++;
        viewsdiapositivas[viewActivo] = getContentActiveEditor();
        viewActivo = countDiapositivas;
        var view = `<article id="viewdiapo_` + countDiapositivas + `" class="multimediaviews-View" onclick="changedViewDiapositiva(this)" style="cursor:pointer">
                `+ (countDiapositivas + 1) + `
            </article>`;
        var viewadd = $('#multimediaviews').find('#viewadd');
        refreshEditor();
        // console.log('array diapo');
        // console.log(viewsdiapositivas);
        viewadd.remove();
        $('#multimediaviews').append(view);
        $('#multimediaviews').append(viewadd);
}


function changedViewDiapositiva(str) {
        updateviewsdiapositivas();
        var id = $(str).attr("id");
        id = id.split('_');
        id = id[1];
        alert(id);
        viewActivo = id;
        refreshEditor();
        tinymce.get('multimediaeditor').setContent(viewsdiapositivas[id]);
}
