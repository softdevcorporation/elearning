<div class="content">
<div class="box-principal">
<h3 class="titulo">Listado de Cursos<hr></h3>

  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">Listado de Cursos</h3>
    </div>
    <div class="panel-body">
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th>nombre</th>
            <th>descripcion</th>
          </tr>
        </thead>
        <tbody>
          <?php  while($row = $datos->fetch(\PDO::FETCH_ASSOC)){ ?>
          <tr>

                <td><?php echo $row['nombre'];?></td>
                <td><a class="btn btn-warning" href="<?php echo URL;?>secciones/editar/<?php echo $row['id'];?>">Editar</a>
                    <a class="btn btn-danger" href="<?php echo URL;?>secciones/eliminar/<?php echo $row['id'];?>">Eliminar</a>
                </td>

          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
