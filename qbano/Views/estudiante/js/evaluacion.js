function editarEvaluacion(id){
  console.log(id);
  var testContainer = $('#testContainer').children('.question');
  var strquestions = []
  // alert(testContainer.length);
  for (var i = 0; i < testContainer.length; i++) {
          var typeQuestion = $(testContainer[i]).find('.typeQuestion')[0].value;
          // alert('tipo'+typeQuestion);
          var textQuestion = $(testContainer[i]).find('input[name=question]')[0].value;
          var idQuestion= $(testContainer[i]).find('input[name=identificadorPregunta]')[0].value;
          console.log(idQuestion);
          // alert('text'+textQuestion);
          var ValueResponsesQuestion;
          var responsesQuestion;
          if (typeQuestion == 1) {
                  responsesQuestion = $(testContainer[i]).find('.questionAsk');
          } else if (typeQuestion == 2) {
                  responsesQuestion = $(testContainer[i]).find('.testName');
                  ValueResponsesQuestion = $(testContainer[i]).find('input[name="checkboxquestiontrue[]"]');
                  // console.log(ValueResponsesQuestion);

          }
          // alert('res'+responsesQuestion.length);
          var responsesQuestionList = []
          if (typeQuestion == 1) {
                  if (responsesQuestion[0].checked == true) {
                          // console.log('yeah');
                          strquestions.push({ idQuestion:idQuestion ,typeQuestion: typeQuestion, textQuestion: textQuestion, responsesQuestionList: 1 });
                  }
                  if (responsesQuestion[1].checked == true) {
                          // console.log('no');
                          strquestions.push({ idQuestion:idQuestion, typeQuestion: typeQuestion, textQuestion: textQuestion, responsesQuestionList: 0 });
                  }
          } else if (typeQuestion == 2) {
                  for (var j = 0; j < responsesQuestion.length; j++) {
                          responsesQuestionList.push({ responsesQuestion: responsesQuestion[j].firstChild.value, valueResponsesQuestion: (ValueResponsesQuestion[j].checked) ? 1 : 0 });
                  }
                  strquestions.push({ idQuestion:idQuestion, typeQuestion: typeQuestion, textQuestion: textQuestion, responsesQuestionList: responsesQuestionList });
          }
  }

  console.log(strquestions);

  var formpost = new FormData();

  formpost.append('action', 'guardarRespuestasEvaluacion');
  formpost.append('idTema', id);
  formpost.append('responsesQuestion', JSON.stringify(strquestions));

  $.ajax({
          url: '',
          type: 'POST',
          data: formpost,
          contentType: false,
          processData: false,
          success: function (data) {
            console.log(data);
            alert('Has Finalizado la Evaluacion.');
            location.href = URL+"estudiante/ver/" + id;
          },
          error: function (e) {
                  //called when there is an error
                  //console.log(e.message);
                  console.log('mal get');
          }
  });
}