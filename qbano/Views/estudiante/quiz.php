
<?php
$tema=$datos;
?>
<div style="float: right; padding-right: 4em; font-size:2em;" class="containerTimeQuiz">
  <p><span id="minutos">0</span>:<span id="segundos">0</span></p>
<!-- <input type="button" onclick="detenerse()" value="deternse"/> -->
</div>
<h2 style="text-align: center;">Evaluación de <?php echo $tema['nombre']?></h2>

  <div class="test" id="testContainer">

<?php
$idTema=$tema['id_tema'];
$evaluacion=$cursos->getEvaluacion($tema['id_tema']);
    $preguntas =  $cursos->getPreguntas($evaluacion['id_evaluacion']);
    $contadorPreguntas=0;

    while($row = $preguntas->fetch(\PDO::FETCH_ASSOC)){
    ?>
    <div class="question" id="questionTheme_0">
    <input name="identificadorPregunta" style="display:none" value="<?php echo $row['id_pregunta'] ?>"/>
      <select class="typeQuestion" name="typequestion" onchange="isSelectedTypeQuestion(this)" disabled style="display:none;">
        <option value="" disabled>Elegir pregunta</option>
        <?php
          if($row['id_tipo_pregunta']==1){
         ?>
        <option value="1" selected >Falso/Verdadero</option>
        <option value="2">Selección Multiple</option>
        <?php
          }else if($row['id_tipo_pregunta']==2){
        ?>
        <option value="1" >Falso/Verdadero</option>
        <option value="2" selected>Selección Multiple</option>
        <?php
          }
        ?>
      </select>
      <input type="text" placeholder="Escribe tu Pregunta" name="question" value="<?php echo $row['descripcion'] ?>" class="form-control" style="border:none;background: transparent;
          text-transform: capitalize;" disabled />
      <div class="testTo" style="margin:0;">
        <ul class="testList">
        <?php
        if($row['id_tipo_pregunta']==1){
              $respuestas =  $cursos->getRespuestas($row['id_pregunta']);
              $res = $respuestas->fetch(\PDO::FETCH_ASSOC);
              if($res['isVerdadera']==0){
          ?>
            <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_<?php echo $row['id_pregunta'] ?>[]"  /><p class="testName">Si</p></li>
            <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_<?php echo $row['id_pregunta'] ?>[]" /><p class="testName">No</p></li>
              <?php
              }else if($res['isVerdadera']==1){
               ?>
            <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_<?php echo $row['id_pregunta'] ?>[]"  /><p class="testName">Si</p></li>
            <li><input class="questionAsk" type="radio" name="checkboxquestiontrue_<?php echo $row['id_pregunta'] ?>[]"  /><p class="testName">No</p></li>
          <?php
              }
          }else if($row['id_tipo_pregunta']==2){
            $respuestas =  $cursos->getRespuestas($row['id_pregunta']);
            while($row = $respuestas->fetch(\PDO::FETCH_ASSOC)){
              if($row['isVerdadera']==true){
          ?>

            <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]"  /><p class="testName"><input type="text" placeholder="Respuesta" value="<?php echo $row['descripcion'] ?>" disabled/></p></li>

        <?php
              }else if($row['isVerdadera']==false){
                ?>
            <li><input class="questionAsk" type="checkbox" name="checkboxquestiontrue[]"  /><p class="testName"><input type="text" placeholder="Respuesta" value="<?php echo $row['descripcion'] ?>" disabled/></p></li>
       <?php
              }
            }
          }
        ?>
        </ul>

        </div>
      </div>


      <?php
      $contadorPreguntas++;
          }
?>
<button onclick="editarEvaluacion(<?php echo $idTema?>)" class="btn btn-success" style="margin-top:1em;">Enviar Respuestas</button>
</div>
  <script type="text/javascript" src="<?php echo URL; ?>Config/configuracion.js"></script>
<script type="text/javascript" src="<?php echo URL; ?>Views/estudiante/js/evaluacion.js"></script>
<script type="text/javascript">
	//setInterval
	var cronometro;

	function detenerse()
	{
		clearInterval(cronometro);
	}

	function carga()
	{
		contador_s =0;
		contador_m =0;
		s = document.getElementById("segundos");
		m = document.getElementById("minutos");

		cronometro = setInterval(

			function(){
        if (contador_m>0) {
          editarEvaluacion(<?php echo $idTema?>);
          <?php echo 'realizado;' ?>
        }
				if(contador_s==60)
				{
					contador_s=0;
					contador_m++;
					m.innerHTML = contador_m;

					if(contador_m==60)
					{
						contador_m=0;
					}
				}

				s.innerHTML = contador_s;
				contador_s++;

			}
			,1000);

	}

  carga();

	</script>
