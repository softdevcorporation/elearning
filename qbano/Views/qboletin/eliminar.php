  <script type="text/javascript" src="<?php echo URL; ?>Views/qboletin/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo URL; ?>Views/qboletin/plugins/tinymce/tinymce.min.js"></script>
  <script type="text/javascript" src="<?php echo URL; ?>Views/qboletin/plugins/tinymce/qboletin-tinymce.js"></script>
  <script type="text/javascript" src="<?php echo URL; ?>Views/qboletin/plugins/tinymce/qboletinSecond-tinymce.js"></script>

<?php
   $pub=$publicaciones->ver();
   $pub2=$publicaciones->verSecond();
   $delete=$publicaciones->eliminar($id);
?>

    <section class="containerQboletin">
      <div class="primariqbolwetin">

        <div class="qboletinPrincipal">
          <textarea id="viewQboletin" class="viewQboletin" name="viewQboletin" ><?php echo $pub['contenido'] ?></textarea>
        </div>

        <hr>

        <?php
            $con=0;
            while($row = $pub2->fetch(\PDO::FETCH_ASSOC))
              {
                $con++;
         ?>

        <div class="qboletinSecondary">
          <textarea id="viewQboletinSecond" class="viewQboletinSecond" name="viewQboletinSecond"><?php echo $row['contenido'] ?></textarea>
        </div>

        <div class="btn">
          <a class="btn btn-danger" id="btn_eliminar" href="<?php echo URL; ?>qboletin/eliminar/<?php echo $row['id_qboletin']; ?>">X</a>
        </div>

        <?php
          }
        ?>

      </div>
    </section>
