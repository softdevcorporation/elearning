<script type="text/javascript" src="<?php echo URL; ?>Views/qboletin/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo URL; ?>Views/qboletin/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo URL; ?>Views/qboletin/plugins/tinymce/init-tinymce.js"></script>
<script type="text/javascript" src="<?php echo URL; ?>Views/qboletin/js/adminpublicar.js"></script>
<?php
  $publicaciones=$publicaciones->agregarpublicacion();
?>
<a href="<?php echo URL; ?>qboletin/eliminar" class="controlVolver btn btn-info">Eliminar Publicaciones</a>

    <h2 style="text-align:center">Crear Publicaciones</h2>
   <form action="" method="post" enctype="multipart/form-data" onsubmit="enviarDatosQboletin(); return false">
    <section class="containerQboletin">

  </label>
      <div class="qboletinPrincipal">
        <textarea id="editorqboletin" class="editorqboletin" name="postQboletin"></textarea>
        <div>
          <span>Tipo Publicacion: </span>
        <select id="tipoPublicacion" class="tipoPublicacion" name="tipoPublicacion" >
          <option value="" disabled>Tipo Publicacion</option>
          <option value="1">Principal</option>
          <option value="0">Secundaria</option>
        </select>
      </div>
        <input type="hidden" name="p1" value="a">
        <button type="submit" class="btn btn-info" style="margin-top:1.5em">Guardar</button>
      </div>
      <hr>
      </section>
    </form>
     <input name="image" type="file" id="upload" class="hidden" onchange="" style="display:none">


<script>
tinymce.init({
    selector: "textarea.editorqboletin",
    theme: "modern",
    max_height: 350,
    max_width: 350,
    min_height: 350,
    min_width: 350,
    // 799 X 275 centrado
    language : "es_MX",
    paste_data_images: true,
    statusbar: false,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars  fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "template paste textcolor colorpicker textpattern"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons | fondo",
    image_advtab: true,
    file_picker_callback: function(callback, value, meta) {
      if (meta.filetype == 'image') {
        $('#upload').trigger('click');
        $('#upload').on('change', function() {
          var file = this.files[0];
          console.log(file);
          var reader = new FileReader();
          reader.onload = function(e) {
            callback(e.target.result, {
              alt: ''
            });
          };
          reader.readAsDataURL(file);
          console.log(reader);
        });
      }
    },
    templates: [{
      title: 'Test template 1',
      content: 'Test 1'
    }, {
      title: 'Test template 2',
      content: 'Test 2'
    }],
    setup: function (editor) {
      editor.addButton('fondo', {
        text: 'Fondo',
        icon: false,
        onclick: function () {
          editor.windowManager.open({
            title: 'Colorpicker',
            body: [{
          type: 'colorpicker',
          name: 'colorpicker',
              }],
              onsubmit: function(e) {
                      // Insert content when the window form is submitted
                      editor.dom.setStyle('tinymce', 'background-color', e.data.colorpicker);
                  },
            width: 400,
            height: 200,
          });
          }
      });
    },
  });
</script>
