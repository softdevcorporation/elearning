tinymce.init({
  selector: 'textarea.viewQboletinSecond',
  height: 350,
  readonly : true,
  toolbar: false,
  menubar: false,
  statusbar: false,
  theme: 'modern',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });
