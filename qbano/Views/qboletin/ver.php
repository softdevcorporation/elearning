
    <script type="text/javascript" src="<?php echo URL; ?>Views/qboletin/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>js/jquery-ias.min.js"></script>
   <script>
  var ias = $.ias({
  container:  ".container",
  item:       ".item",
  pagination: "#pagination",
  next:       ".next a"
});

ias.extension(new IASSpinnerExtension());            // shows a spinner (a.k.a. loader)
ias.extension(new IASTriggerExtension({offset: 3})); // shows a trigger after page 3
ias.extension(new IASNoneLeftExtension({
  text: 'No hay mas contenido para mostrar !'      // override text when no pages left
}));
  </script>

<?php
   $pub=$publicaciones->listar();
  $pub2=$publicaciones->verSecond();
?>

    <section class="containerQboletin" id="container">
      <h2 class="title">Qboletin</h2>
      <div class="qboletinPrincipal" >
        <?php
            while($row1 = $pub->fetch(\PDO::FETCH_ASSOC))
              {

         ?>
         <section class="qboletin-Item">
            <?php echo $row1['contenido'] ?>
        </section>
        <?php

      }
         ?>
      </div>
      <hr>
<h2></h2>
      <div class="qboletinSecondary item">
        <?php

            while($row2 = $pub2->fetch(\PDO::FETCH_ASSOC))
              {


         ?>
         <section class="qboletin-Item">
            <?php echo $row2['contenido'] ?>
        </section>
        <?php

      }
         ?>

      </div>

    </section>
    <div id="pagination">
  <a href="page1.html">1</a>
  <a href="page2.html" class="next">2</a>
</div>
</div>


  <script type="text/javascript" src="<?php echo URL; ?>Views/qboletin/plugins/tinymce/tinymce.min.js"></script>
  <script type="text/javascript" src="<?php echo URL; ?>Views/qboletin/plugins/tinymce/qboletin-tinymce.js"></script>
  <script type="text/javascript" src="<?php echo URL; ?>Views/qboletin/plugins/tinymce/qboletinSecond-tinymce.js"></script>
 