<?php namespace Controllers;
use Models\TipoEvaluacion as TipoEvaluacion;
class tipoEvaluacionesController{
  //atributos
  private $tipoEvaluacion;

  //metodos
  public function __construct(){
    $this->tipoEvaluacion = new Curso();
  }

  public function index(){
    $datos = $this->tipoEvaluacion->listar();
    return $datos;
  }


public function ver($id){
  $this->tipoEvaluacion->set("id",$id);
  $datos = $this->tipoEvaluacion->view();
  return $datos;
}

public function eliminar($id){
  $this->tipoEvaluacion->set("id",$id);
  $datos = $this->tipoEvaluacion->delete();
  header("location: " . URL . "estudiantes");
  return $datos;
}

  
}


$tipoEvaluaciones = new tipoEvaluacionesController();
?>
