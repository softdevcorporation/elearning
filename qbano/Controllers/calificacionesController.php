<?php namespace Controllers;
use Models\Calificacion as Calificacion;
use Models\Evaluacion as Evaluacion;

class calificacionesController{
  //atributos
  private $calificacion;

  //metodos
  public function __construct(){
    $this->calificacion = new Calificacion();
  }

  public function index(){
    $datos = $this->calificacion->listar();
    return $datos;
  }

  public function agregar($id){
    echo $id;
    if(!$_POST){
      echo 'no post';
    }else{
      
        echo 'save';
        $this->calificacion->set('isAprovado',$_POST['isaprobado']);
        $this->calificacion->set('numeroIntento',$_POST['numerointento']);
        $evaluacion=new Evaluacion();
        $evaluacion->set('id',$id);
        $this->calificacion->set('evaluacion',$evaluacion);
        $this->calificacion->add();
      
      }
  }


public function ver($id){
  $this->calificacion->set("id",$id);
  $datos = $this->calificacion->view();
  return $datos;
}

public function eliminar($id){
  $this->calificacion->set("id",$id);
  $datos = $this->calificacion->delete();
  return $datos;
}


}


$modulos = new calificacionesController();
?>
