<?php namespace Controllers;
use Models\Tema as Tema;
use Models\Evaluacion as Evaluacion;
use Models\Pregunta as Pregunta;
use Models\RespuestaPregunta as RespuestaPregunta;
use Models\DiapositivaTema as DiapositivaTema;
use Models\Modulo as Modulo;
use Models\Calificacion as Calificacion;

class temasController{
  //atributos
  private $tema;
  private $evalucion;
  private $pregunta;
  private $respuetas;
  private $diapositiva;
  private $modulo;


  //metodos
  public function __construct(){
    $this->tema = new Tema();
    $this->evaluacion=new Evaluacion();
    $this->pregunta=new Pregunta();
    $this->respuestas=new RespuestaPregunta();
    $this->diapositiva=new DiapositivaTema();
    $this->modulo=new Modulo();
  }

  public function index(){
    $datos = $this->tema->listar();
    return $datos;
  }

  public function agregar($id){
    if(!$_POST){
      //echo 'no post';
      $modulo=new Modulo();
      $modulo->set("id",$id);
      $datos = $modulo->view();
      return $datos;
  }else{
    //echo $id;
      $modulo=new Modulo();
      $modulo->set("id",$id);
      $tema=new Tema();
      $tema->set("nombre", $_POST['nombreTema']);
      $tema->set("modulo", $modulo);
      $evaluacion=new Evaluacion();
      $evaluacion->set("tema",$tema);
      $responsesQuestion=json_decode(stripslashes($_POST['responsesQuestion']));
      $listaDiapositivas=json_decode($_POST['listaDiapositivas']);
          if($tema->add()){
            if($evaluacion->add() && $this->guardarDiapositivas($listaDiapositivas,$tema)){
                if($this->guardarPreguntasTema($responsesQuestion,$evaluacion)){

                }
            }
          }

          $datos = $modulo->view();
          return $datos;

      }
  }


  protected function guardarDiapositivas($listaDiapositivas,$tema){
      for($i=0;$i<count($listaDiapositivas);$i++){
        $contDiapositiva=$listaDiapositivas[$i]->diapositiva;
        $contDiapositiva=str_replace('\"','\\\"',$contDiapositiva);
        $contDiapositiva=str_replace('\'','\\\'',$contDiapositiva);
        $diapositiva=new DiapositivaTema();
        $diapositiva->set("tema",$tema);
        $diapositiva->set("contenido",$contDiapositiva);
        if(!$diapositiva->add()){
            return false;
        }

      }
      return true;
        // return true;
    }


    protected function guardarPreguntasTema($listaPreguntas,$evalucion){
      for($i=0;$i<count($listaPreguntas);$i++){
                                    //  echo 'cont '.$i;
                            $strpregunta=$listaPreguntas[$i]->textQuestion;
                            $strtipopregunta=$listaPreguntas[$i]->typeQuestion;
                            $responsesQuestionList=$listaPreguntas[$i]->responsesQuestionList;
                            // echo '<script>alert("hola miguel")</script>'.$strpregunta.$strtipopregunta;
                            $pregunta=new Pregunta();
                            $pregunta->set("descripcion", $strpregunta);
                            $pregunta->set("idTipoPregunta", $strtipopregunta);
                            $pregunta->set("evaluacion", $evalucion);
                            $pregunta->set("listaRespuestas", $responsesQuestionList);
                            if(!$pregunta->add()){
                              return false;
                            }

      }
      return true;
    }


public function ver($id){
  echo 'control tema';
  $this->tema->set("id",$id);
  $datos = $this->tema->view();
  return $datos;
}

public function eliminar($id){
  $this->tema->set("id",$id);
  $datos = $this->tema->delete();
  header("location: " . URL . "estudiantes");
  return $datos;
}

  public function editar($id){
    if(!$_POST){
      //print('id '.$id);
      $this->tema->set("id",$id);
      $datos=$this->tema->view();

      return $datos;
    // $this->curso->set("id",$id);
    // $datos = $this->curso->view();
    // return $datos;
  }else{
      //  echo 'post  temas controller editar';

     if($_POST['action']=='saveInfoDiapositivas'){
      //echo 'aca';
      $listadiapositivas=json_decode($_POST['listadiapositivas']);
      $tema=new Tema();
      $tema->set("id", $_POST['idTema']);
      $diapositiva=new DiapositivaTema();
      $listadiapositivasdb=$diapositiva->listarPorTema($tema->get("id"));


      $cont=0;
      while($diapo=$listadiapositivasdb->fetch(\PDO::FETCH_ASSOC)){
        // print_r($diapo['id_diapositiva_tema']);
        // print_r($diapo['contenido']);
        $diapositiva->set("contenido", $listadiapositivas[$cont]);
        $diapositiva->set("id", $diapo['id_diapositiva_tema']);
        $diapositiva->edit();
        $cont++;
      }

      if($cont < count($listadiapositivas)){
          $diapositiva->set("contenido", $listadiapositivas[$cont]);
          $diapositiva->set("tema", $tema);
          $diapositiva->add();
          $cont++;
      }
    } else if($_POST['action']=='saveInfoTema'){
      $tema=new Tema();
      // echo  $_POST['nombre'];
      // echo  $_POST['idTema'];
      $tema->set("nombre", $_POST['nombre']);
      $tema->set("id", $_POST['idTema']);
      $tema->edit();
    }else if($_POST['action']=='editarEvaluacion'){
      //echo 'evaluaciones';
      $idEvaluacion=$this->getEvaluacion($_POST['idTema']);
      $evaluacion=new Evaluacion();
      $tema=new Tema();
      $tema->set("id", $_POST['idTema']);
      //echo 'id_tema'.$_POST['idTema'];
      $evaluacion->set("tema",$tema);
      //print_r($idEvaluacion);
      $evaluacion->set("id",$idEvaluacion['id_evaluacion']);
      $evaluacion->set("id",$idEvaluacion['id_evaluacion']);
      $pregunta= new Pregunta();
      $pregunta->set("evaluacion",$evaluacion);
      $pregunta->deteleByEvaluacion();
      //print_r($evaluacion);
      $responsesQuestion=json_decode(stripslashes($_POST['responsesQuestion']));
      $this->guardarPreguntasTema($responsesQuestion,$evaluacion);
    }

      $this->tema->set("id",$id);
      $datos=$this->tema->view();

      return $datos;

  }
}

public function getEvaluacion($idTema){
  $datos= $this->evaluacion->listarPorTema($idTema);
  return $datos;
}


public function getPreguntas($idEvaluacion){
  $datos= $this->pregunta->listarPorEvaluacion($idEvaluacion);
  return $datos;
}

public function getRespuestas($idPregunta){
  $datos= $this->respuestas->listarPorPregunta($idPregunta);
  return $datos;
}


public function getDiapositivas($idTema){
  $datos= $this->diapositiva->listarPorTema($idTema);
  return $datos;
}


public function getModulo($idModulo){
  $modulo=new Modulo();
  $modulo->set("id",$idModulo);
  $datos= $modulo->view();
  return $datos;
}

public function getTemas($idModulo){
  $datos= $this->tema->listarPorModulo($idModulo);
  return $datos;
}

public function getCalificaciones($idTema){
  $calificacion=new Calificacion();
  $evaluacion=$this->getEvaluacion($idTema);
  $datos= $calificacion->listarPorEvaluacion($evaluacion['id_evaluacion']);
  return $datos;
}


}


$temas = new temasController();
?>
