<?php namespace Controllers;
use Models\TipoCurso as TipoCurso;
class tipoCursosController{
  //atributos
  private $tipoCurso;

  //metodos
  public function __construct(){
    $this->tipoCurso = new TipoCurso();
  }

  public function index(){
    $datos = $this->tipoCurso->listar();
    return $datos;
  }


public function ver($id){
  $this->tipoCurso->set("id",$id);
  $datos = $this->tipoCurso->view();
  return $datos;
}

public function eliminar($id){
  $this->tipoCurso->set("id",$id);
  $datos = $this->tipoCurso->delete();
  header("location: " . URL . "estudiantes");
  return $datos;
}

  
}


$tipoCursos = new tipoCursosController();
?>
