<?php namespace Controllers;
use Models\TipoUsuariosCursos as TipoUsuariosCursos;
class tipoUsuariosCursosController{
  //atributos
  private $tipoUsuarioCurso;

  //metodos
  public function __construct(){
    $this->tipoUsuarioCurso = new Curso();
  }

  public function index(){
    $datos = $this->tipoUsuarioCurso->listar();
    return $datos;
  }


public function ver($id){
  $this->tipoUsuarioCurso->set("id",$id);
  $datos = $this->tipoUsuarioCurso->view();
  return $datos;
}

public function eliminar($id){
  $this->tipoUsuarioCurso->set("id",$id);
  $datos = $this->tipoUsuarioCurso->delete();
  header("location: " . URL . "estudiantes");
  return $datos;
}

  
}


$tipoUsuariosCursos = new tipoUsuariosCursosController();
?>
