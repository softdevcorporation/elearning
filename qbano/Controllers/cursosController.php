<?php namespace Controllers;
use Models\Curso as Curso;
use Models\TipoUsuarioCurso as TipoUsuarioCurso;
use Models\TipoCurso as TipoCurso;
use Models\Modulo as Modulo;
use Models\Tema as Tema;
use Models\Evaluacion as Evaluacion;
use Models\Pregunta as Pregunta;
use Models\RespuestaPregunta as RespuestaPregunta;
use Models\DiapositivaTema as DiapositivaTema;
use Models\Calificacion as Calificacion;
use Models\Roll as Roll;
use Models\Cargo as Cargo;

class cursosController{
  private $curso;
  private $tipoUsuariocurso;
  private $tipoCurso;
  private $modulo;
  private $tema;
  private $evalucion;
  private $pregunta;
  private $respuetas;
  private $diapositiva;
  private $roll;
  private $cargo;

  //metodos
  public function __construct(){
    $this->curso = new Curso();
    $this->tipoUsuariocurso =new TipoUsuariocurso();
    $this->tipoCurso =new TipoCurso();
    $this->modulo = new Modulo();
    $this->tema= new Tema();
    $this->evaluacion=new Evaluacion();
    $this->pregunta=new Pregunta();
    $this->respuestas=new RespuestaPregunta();
    $this->diapositiva=new DiapositivaTema();
    $this->roll=new Roll();
    $this->cargo=new Cargo();
  }

  public function index(){
    if(!$_POST){
    //  echo 'no post';
    // $datos = $this->secciones->listar();
    // return $datos;
  }else{
    if(isset($_POST['action'])){
    if($_POST['action']=='cambiarEstado'){
        $this->curso->set("id",$_POST['id']);
        $this->curso->set("estado",$_POST['estado']);
        $this->curso->cambiarEstado();
      }else if($_POST['action']=='cambiarPermisosRol'){
        $this->roll->set("id",$_POST['idRol']);
        $this->roll->set("permiso",$_POST['permiso']);
        $this->roll->edit();
      }
    }
  }


    $datos = $this->curso->listar();
    return $datos;
    }

    public function index2(){
      $datos = $this->curso->buscar();
      return $datos;
      }

  public function agregar(){
    if(!$_POST){
      //echo 'no post';
    // $datos = $this->secciones->listar();
    // return $datos;
  }else{
echo "paso 1";
      $this->curso->set("nombre", $_POST['nombreCurso']);
      $this->curso->set("descripcion", $_POST['descripcion']);
      $this->curso->set("isCertificado", $_POST['certifica']);
      $this->curso->set("fechaInicio", $_POST['fechainicio']);
      $this->curso->set("fechaTerminacion", $_POST['fechafin']);
      $this->curso->set("fechaPublicacion", $_POST['fechapublicacion']);
      $this->curso->set("idTipoCurso", $_POST['tipoCurso']);
      $this->curso->set("estado", $_POST['estado']);
      $modulo=new Modulo();
      $modulo->set("nombre", $_POST['nombreModulo']);
      $modulo->set("curso", $this->curso);
      $tema=new Tema();
      $tema->set("nombre", $_POST['nombreTema']);
      $tema->set("modulo", $modulo);
      $evaluacion=new Evaluacion();
      $evaluacion->set("tema",$tema);
      $this->curso->set("sendTo", json_decode(stripslashes($_POST['sendTo'])));
      $responsesQuestion=json_decode(stripslashes($_POST['responsesQuestion']));
      if(isset($_FILES['inputFileImage'])){
        $this->curso->set("imagen", $_FILES['inputFileImage']);
      }
      if(isset($_FILES['filedraft'])){
        $this->curso->set("draft", $_FILES['filedraft']);
      }
      if(isset($_FILES['fileguiaformato'])){
        $this->curso->set("guia", $_FILES['fileguiaformato']);
      }
      if(isset($_FILES['fileevaluacion'])){
        $this->curso->set("evaluacion", $_FILES['fileevaluacion']);
      }
      if(isset($_FILES['filetextoayuda'])){
        $this->curso->set("textoAyuda", $_FILES['filetextoayuda']);
      }
      $listaDiapositivas=json_decode($_POST['listaDiapositivas']);
      if($this->curso->add()){
echo "curso creado ............................";
        if($modulo->add()){
          if($tema->add()){
            if($evaluacion->add() && $this->guardarDiapositivas($listaDiapositivas,$tema)){
                if($this->guardarPreguntasTema($responsesQuestion,$evaluacion)){

                  if($this->curso->get("idTipoCurso")==1){
                        echo '<p class="CurrentSaveCurso">1</p>';
                  }else if($this->curso->get("idTipoCurso")==2){
                        echo '<p class="CurrentSaveCurso">1</p>';
                  }else{
                        echo '<p class="CurrentSaveCurso">1</p>';
                  }
                }
            }
          }
        }
      }




      }
  }

  public function editar($id){
    if(!$_POST){
      //echo ' nopost editar';
    $this->curso->set("id",$id);
    $datos = $this->curso->view();
    return $datos;
  }else{
    //echo 'post  curso controller editar'.$id;
          $this->curso->set("id",$id);

    if($_POST['action']=='saveInfoCurso'){
      $this->curso->set("nombre", $_POST['nombreCurso']);
      $this->curso->set("descripcion", $_POST['descripcion']);
      $this->curso->set("isCertificado", $_POST['certifica']);
      $this->curso->set("fechaInicio", $_POST['fechainicio']);
      $this->curso->set("fechaTerminacion", $_POST['fechafin']);
      $this->curso->set("fechaPublicacion", $_POST['fechapublicacion']);
      $this->curso->editInfoCurso();
      echo 'iniio archivo';
      if(isset($_FILES['draft'])){
        echo "paso archivo";
        $this->curso->set("draft", $_FILES['draft']);
        $this->curso->eviarDocumento($this->curso->get("draft"), 0);
      }
      if(isset($_FILES['guia'])){
        $this->curso->set("guia", $_FILES['guia']);
        $this->curso->eviarDocumento($this->curso->get("guia"), 1);
      }
      if(isset($_FILES['evaluacion'])){
        $this->curso->set("evaluacion", $_FILES['evaluacion']);
        $this->curso->eviarDocumento($this->curso->get("evaluacion"), 2);
      }
      if(isset($_FILES['filetextoayuda'])){
        $this->curso->set("ayuda", $_FILES['ayuda']);
        $this->curso->eviarDocumento($this->curso->get("ayuda"), 3);
      }

    }else if($_POST['action']=='saveInfoModulo'){
      $modulo=new Modulo();
      $modulo->set("nombre", $_POST['modulo']);
      $modulo->set("id", $_POST['idModulo']);
      $modulo->edit();
    }else if($_POST['action']=='saveInfoTema'){
      $tema=new Tema();
      echo  $_POST['nombre'];
      echo  $_POST['idTema'];
      $tema->set("nombre", $_POST['nombre']);
      $tema->set("id", $_POST['idTema']);
      $tema->edit();
    }else if($_POST['action']=='editarEvaluacion'){
      echo 'evaluaciones';
      $idEvaluacion=$this->getEvaluacion($_POST['idTema']);
      $evaluacion=new Evaluacion();
      $tema=new Tema();
      $tema->set("id", $_POST['idTema']);
      echo 'id_tema'.$_POST['idTema'];
      $evaluacion->set("tema",$tema);
      print_r($idEvaluacion);
      $evaluacion->set("id",$idEvaluacion['id_evaluacion']);
      $pregunta= new Pregunta();
      $pregunta->set("evaluacion",$evaluacion);
      $pregunta->deteleByEvaluacion();
      //print_r($evaluacion);
      $responsesQuestion=json_decode(stripslashes($_POST['responsesQuestion']));
      $this->guardarPreguntasTema($responsesQuestion,$evaluacion);
    }

    $datos = $this->curso->view();
    return $datos;
  }
}

public function cambiarEstado($id){
  $this->curso->set("id",$id);
  $this->curso->cambiarEstado();
  $datos = $this->curso->view();
  return $datos;

}

public function ver($id){
 $this->curso->set("id",$id);
    $datos = $this->curso->view();
    return $datos;
}

public function eliminar($id){
  $this->curso->set("id",$id);
  $datos = $this->curso->delete();
  header("location: " . URL . "estudiantes");
  return $datos;
}

protected function guardarDiapositivas($listaDiapositivas,$tema){
    for($i=0;$i<count($listaDiapositivas);$i++){
      $contDiapositiva=$listaDiapositivas[$i]->diapositiva;
      $contDiapositiva=str_replace('\"','\\\"',$contDiapositiva);
      $contDiapositiva=str_replace('\'','\\\'',$contDiapositiva);
      $diapositiva=new DiapositivaTema();
      $diapositiva->set("tema",$tema);
      $diapositiva->set("contenido",$contDiapositiva);
      if(!$diapositiva->add()){
          return false;
      }

    }
    return true;
      // return true;
  }

protected function guardarPreguntasTema($listaPreguntas,$evalucion){
  print_r($listaPreguntas);
  for($i=0;$i<count($listaPreguntas);$i++){
                                //  echo 'cont '.$i;
                        $strpregunta=$listaPreguntas[$i]->textQuestion;
                        $strtipopregunta=$listaPreguntas[$i]->typeQuestion;
                        $responsesQuestionList=$listaPreguntas[$i]->responsesQuestionList;
                        // echo '<script>alert("hola miguel")</script>'.$strpregunta.$strtipopregunta;
                        $pregunta=new Pregunta();
                        $pregunta->set("descripcion", $strpregunta);
                        $pregunta->set("idTipoPregunta", $strtipopregunta);
                        $pregunta->set("evaluacion", $evalucion);
                        $pregunta->set("listaRespuestas", $responsesQuestionList);
                        if(!$pregunta->add()){
                          return false;
                        }

  }
  return true;
}




public function isCursoTerminado($id){
    //echo 'cargar curso';
    //print_r($this->getModules($id)->fetch(\PDO::FETCH_ASSOC));
    $modulos=$this->getModules($id)->fetch(\PDO::FETCH_ASSOC);
    //echo $modulos['id_modulo'];
    //print_r($this->getTemas($modulos['id_modulo'])->fetch(\PDO::FETCH_ASSOC));
    $temas=$this->getTemas($modulos['id_modulo'])->fetch(\PDO::FETCH_ASSOC);
    //echo $temas['id_tema'];
    $calificaciones=$this->getCalificaciones($temas['id_tema'])->fetch(\PDO::FETCH_ASSOC);
    //print_r($calificaciones);
    $finalizados=$calificaciones['numero_intentos'];
    //echo 'finalizados'.$finalizados;
    return $finalizados;

}

public function listarTipoUsuarios(){
  $datos= $this->tipoUsuariocurso->listar();
  return $datos;

}

public function listarCargos(){
  $datos= $this->cargo->listar();
  return $datos;

}

public function listarTipoCurso(){
  $datos= $this->tipoCurso->listar();
  return $datos;

}

public function getModules($idCurso){
  $datos= $this->modulo->listarPorCurso($idCurso);
  return $datos;
}

public function getTemas($idModulo){
  $datos= $this->tema->listarPorModulo($idModulo);
  return $datos;
}

public function getEvaluacion($idTema){
  $datos= $this->evaluacion->listarPorTema($idTema);
  return $datos;
}


public function getPreguntas($idEvaluacion){
  $datos= $this->pregunta->listarPorEvaluacion($idEvaluacion);
  return $datos;
}

public function getRespuestas($idPregunta){
  $datos= $this->respuestas->listarPorPregunta($idPregunta);
  return $datos;
}

public function getDiapositivas($idTema){
  $datos= $this->diapositiva->listarPorTema($idTema);
  return $datos;
}

public function getCalificaciones($idTema){
  $calificacion=new Calificacion();
  $evaluacion=$this->getEvaluacion($idTema);
  $datos= $calificacion->listarPorEvaluacion($evaluacion['id_evaluacion']);
  return $datos;
}

public function getRol($idRol){
  $this->roll->set("id",$idRol);
  $datos=$this->roll->view();
  return $datos;
}

public function existPreguntas($idTema){
  $evaluacion=$this->getEvaluacion($idTema);
  $preguntas=$this->getPreguntas($evaluacion['id_evaluacion']);
  $preguntas=$preguntas->fetch(\PDO::FETCH_ASSOC);
  if($preguntas==null){
    return false;
  }
  return true;
}

public function existCalificacion($idTema){
  $evaluacion=$this->getEvaluacion($idTema);
  $calificacion=$this->getCalificaciones($evaluacion['id_evaluacion']);
  print_r($calificacion);
  $calificacion=$calificacion->fetch(\PDO::FETCH_ASSOC);
  if($calificacion==null){
    return false;
  }
  return true;
}


public function quiz($id){
  //echo 'estoy presentando un quiz del tema '.$id.'<br>';
  $tema = new Tema();
  $tema->set("id",$id);
 if(!$_POST){
  $idEvaluacion=$this->getEvaluacion($id);
  //print_r($idEvaluacion);
  //echo '<br>';
  $calificacion=new Calificacion();
  $evaluacion=new Evaluacion();
  $evaluacion->set("id",$idEvaluacion['id_evaluacion']);
  $calificacion->set("evaluacion",$evaluacion);
  $datoscalificacion=$calificacion->findByEvaluacion();

  if($datoscalificacion==null){
    $calificacion->set("isAprobado",0);
    $calificacion->set("numeroIntento",1);
    $calificacion->add();
  }else{
    //echo 'edit';
    $calificacion->set("id",$datoscalificacion['id_calificacion']);
    $calificacion->set("isAprobado",0);
    $intentos=$datoscalificacion['numero_intentos']+1;
    $calificacion->set("numeroIntento",$intentos);
    $calificacion->edit();
  }

  //print_r($datoscalificacion);
  //$calificacion=


  }else{
    if($_POST['action']=='guardarRespuestasEvaluacion'){
      //echo 'se guardo la evaluacion';
      $responsesQuestion=json_decode(stripslashes($_POST['responsesQuestion']));
      $idTema=$_POST['idTema'];

$arrayComparo=array();
for($i=0;$i<count($responsesQuestion);$i++){
  if($responsesQuestion[$i]->typeQuestion==1){
    //echo 'tipo 1';
  $arrayComparo[]=$responsesQuestion[$i]->responsesQuestionList;
  }else if($responsesQuestion[$i]->typeQuestion==2){
    //echo 'tipo 2';
    $arrayComparoInterno=array();
  for($j=0;$j<count($responsesQuestion[$i]->responsesQuestionList);$j++){
    $arrayComparoInterno[]=$responsesQuestion[$i]->responsesQuestionList[$j]->valueResponsesQuestion;
  }
    $arrayComparo[]=$arrayComparoInterno;
  }

}
      $numeroAciertos=0;
      for($i=0;$i<count($responsesQuestion);$i++){
        $idPregunta=$responsesQuestion[$i]->idQuestion;
        $resultado=$this->compararRespuestas($idPregunta,$arrayComparo[$i]);
        if($resultado==true){
          $numeroAciertos++;
        }
        ///print_r($responsesQuestion);
      }

      $idEvaluacion=$this->getEvaluacion($id);
  print_r($idEvaluacion);
  echo '<br>';
  $calificacionFin=new Calificacion();
  $evaluacion=new Evaluacion();
  $evaluacion->set("id",$idEvaluacion['id_evaluacion']);
  $calificacionFin->set("evaluacion",$evaluacion);
  $datoscalificacion=$calificacionFin->findByEvaluacion();
  $calificacionFin->set("id",$datoscalificacion['id_calificacion']);
        $calificacionFin->set("numeroAciertos",$numeroAciertos);
        $evalua=$this->getEvaluacion($id);
        print_r($evalua['id_evaluacion']);
        $pregun=$this->getPreguntas($evalua['id_evaluacion']);
        $contTemporal=0;
        while($pregun->fetch(\PDO::FETCH_ASSOC)){
          $contTemporal++;
        }
        print_r($contTemporal);
        $porcentaje=5/$contTemporal;
        $nota=$numeroAciertos*$porcentaje;
        print($nota);
        $calificacionFin->set("nota",$nota);
        $calificacionFin->editNota();



    }
  }



  $datos = $tema->view();
  return $datos;
}

public function compararRespuestas($idPregunta,$listaComparo){
  //echo '<br>'.$idPregunta;
  $respuestas=$this->getRespuestas($idPregunta);
  $arrayRespuestas=array();
  while($datos = $respuestas->fetch(\PDO::FETCH_ASSOC)){
          $arrayRespuestas[]=$datos;

    }

    if(count($listaComparo)==count($arrayRespuestas)){

      $array[]=array();
      $contAciertoInterno=0;
      $isAcerto=false;
      for($j=0;$j<count($listaComparo);$j++){
        $temres=$arrayRespuestas[$j];
        if(count($arrayRespuestas)>1){

          $comTemporal=$listaComparo[$j];

          if($comTemporal==$temres['isVerdadera']){
            $contAciertoInterno++;
          }else{
          }
          if($j==3){
            if($contAciertoInterno==4){
              //echo 'Respuesta Acertada';
              $isAcerto=true;
            }else{
              //echo 'Respuesta Incorrecta';
              $isAcerto=false;
            }
          }
        }else if(count($arrayRespuestas)==1){
          if($temres['isVerdadera']==$listaComparo){
            //echo 'Respuesta Acertada';
            $isAcerto=true;
          }else{
            //echo 'Respuesta Incorrecta';
            $isAcerto=false;
          }
        }
      }


      if($isAcerto==true){
            echo ' Respuesta Acertada';

      }else{
            echo ' Respuesta Incorrecta';

      }
      return $isAcerto;

    }
}

public function colorpicker(){
  
}


}

$cursos = new cursosController();
?>
