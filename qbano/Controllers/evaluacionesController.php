<?php namespace Controllers;
use Models\Evaluacion as Evaluacion;
class evaluacionesController{
  //atributos
  private $evaluacion;

  //metodos
  public function __construct(){
    $this->evaluacion = new Evaluacion();
  }

  public function index(){
    $datos = $this->evaluacion->listar();
    return $datos;
  }


public function ver($id){
  $this->evaluacion->set("id",$id);
  $datos = $this->evaluacion->view();
  return $datos;
}

public function eliminar($id){
  $this->evaluacion->set("id",$id);
  $datos = $this->evaluacion->delete();
  header("location: " . URL . "estudiantes");
  return $datos;
}

  
}


$evaluaciones = new evaluacionesController();
?>
