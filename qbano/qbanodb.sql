-- MySQL Script generated by MySQL Workbench
-- Sat Mar 18 14:25:55 2017
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema qbanodb
-- -----------------------------------------------------
create database qbanodb;
-- -----------------------------------------------------
-- Schema qbanodb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `qbanodb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `qbanodb` ;

-- -----------------------------------------------------
-- Table `qbanodb`.`tipo_cursos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qbanodb`.`tipo_cursos` (
  `id_tipo_curso` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`id_tipo_curso`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `qbanodb`.`cursos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qbanodb`.`cursos` (
  `id_curso` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `descripcion` VARCHAR(200) NULL,
  `fecha_inicio` DATE NULL,
  `fecha_terminacion` DATE NULL,
  `fecha_publicacion` DATE NULL,
  `imagen` VARCHAR(300) NULL,
  `id_tipo_curso` INT NOT NULL,
  `is_certificated` TINYINT(1) NULL,
  `draft_url` VARCHAR(300) NULL,
  `guia_url` VARCHAR(300) NULL,
  `evaluacion_url` VARCHAR(300) NULL,
  `textoayuda_url` VARCHAR(300) NULL,
  `estado` TINYINT(1) NULL,
  PRIMARY KEY (`id_curso`),
  CONSTRAINT `fk_cursos_tipo_cursos`
    FOREIGN KEY (`id_tipo_curso`)
    REFERENCES `qbanodb`.`tipo_cursos` (`id_tipo_curso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_cursos_tipo_cursos_idx` ON `qbanodb`.`cursos` (`id_tipo_curso` ASC);


-- -----------------------------------------------------
-- Table `qbanodb`.`tipo_usuarios_cursos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qbanodb`.`tipo_usuarios_cursos` (
  `id_tipo_usuario_curso` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`id_tipo_usuario_curso`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `qbanodb`.`cursos_has_tipo_usuarios_cursos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qbanodb`.`cursos_has_tipo_usuarios_cursos` (
  `id_curso` INT NOT NULL,
  `id_tipo_usuario_curso` INT NOT NULL,
  CONSTRAINT `fk_cursos_has_tipo_usuarios_cursos_cursos1`
    FOREIGN KEY (`id_curso`)
    REFERENCES `qbanodb`.`cursos` (`id_curso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cursos_has_tipo_usuarios_cursos_tipo_usuarios_cursos1`
    FOREIGN KEY (`id_tipo_usuario_curso`)
    REFERENCES `qbanodb`.`tipo_usuarios_cursos` (`id_tipo_usuario_curso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_cursos_has_tipo_usuarios_cursos_tipo_usuarios_cursos1_idx` ON `qbanodb`.`cursos_has_tipo_usuarios_cursos` (`id_tipo_usuario_curso` ASC);

CREATE INDEX `fk_cursos_has_tipo_usuarios_cursos_cursos1_idx` ON `qbanodb`.`cursos_has_tipo_usuarios_cursos` (`id_curso` ASC);


-- -----------------------------------------------------
-- Table `qbanodb`.`modulos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qbanodb`.`modulos` (
  `id_modulo` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `id_curso` INT NOT NULL,
  PRIMARY KEY (`id_modulo`),
  CONSTRAINT `fk_modulos_cursos1`
    FOREIGN KEY (`id_curso`)
    REFERENCES `qbanodb`.`cursos` (`id_curso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_modulos_cursos1_idx` ON `qbanodb`.`modulos` (`id_curso` ASC);


-- -----------------------------------------------------
-- Table `qbanodb`.`temas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qbanodb`.`temas` (
  `id_tema` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `id_modulo` INT NOT NULL,
  PRIMARY KEY (`id_tema`),
  CONSTRAINT `fk_temas_modulos1`
    FOREIGN KEY (`id_modulo`)
    REFERENCES `qbanodb`.`modulos` (`id_modulo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_temas_modulos1_idx` ON `qbanodb`.`temas` (`id_modulo` ASC);


-- -----------------------------------------------------
-- Table `qbanodb`.`evaluaciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qbanodb`.`evaluaciones` (
  `id_evaluacion` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `calificacion` DOUBLE NULL,
  `id_tema` INT NOT NULL,
  PRIMARY KEY (`id_evaluacion`),
  CONSTRAINT `fk_evaluciones_temas1`
    FOREIGN KEY (`id_tema`)
    REFERENCES `qbanodb`.`temas` (`id_tema`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_evaluciones_temas1_idx` ON `qbanodb`.`evaluaciones` (`id_tema` ASC);


-- -----------------------------------------------------
-- Table `qbanodb`.`tipo_preguntas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qbanodb`.`tipo_preguntas` (
  `id_tipo_pregunta` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`id_tipo_pregunta`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `qbanodb`.`preguntas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qbanodb`.`preguntas` (
  `id_pregunta` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(300) NULL,
  `id_tipo_pregunta` INT NOT NULL,
  `id_evaluacion` INT NOT NULL,
  PRIMARY KEY (`id_pregunta`),
  CONSTRAINT `fk_preguntas_tipo_preguntas1`
    FOREIGN KEY (`id_tipo_pregunta`)
    REFERENCES `qbanodb`.`tipo_preguntas` (`id_tipo_pregunta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_preguntas_evaluaciones1`
    FOREIGN KEY (`id_evaluacion`)
    REFERENCES `qbanodb`.`evaluaciones` (`id_evaluacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_preguntas_tipo_preguntas1_idx` ON `qbanodb`.`preguntas` (`id_tipo_pregunta` ASC);

CREATE INDEX `fk_preguntas_evaluaciones1_idx` ON `qbanodb`.`preguntas` (`id_evaluacion` ASC);


-- -----------------------------------------------------
-- Table `qbanodb`.`repuestas_preguntas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qbanodb`.`repuestas_preguntas` (
  `id_repuesta_pregunta` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(200) NULL,
  `isVerdadera` TINYINT(1) NULL,
  `id_pregunta` INT NOT NULL,
  PRIMARY KEY (`id_repuesta_pregunta`),
  CONSTRAINT `fk_repuestas_preguntas_preguntas1`
    FOREIGN KEY (`id_pregunta`)
    REFERENCES `qbanodb`.`preguntas` (`id_pregunta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_repuestas_preguntas_preguntas1_idx` ON `qbanodb`.`repuestas_preguntas` (`id_pregunta` ASC);


-- -----------------------------------------------------
-- Table `qbanodb`.`diapositivas_tema`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qbanodb`.`diapositivas_tema` (
  `id_diapositiva_tema` INT NOT NULL AUTO_INCREMENT,
  `contenido` LONGTEXT NULL,
  `id_tema` INT NOT NULL,
  PRIMARY KEY (`id_diapositiva_tema`),
  CONSTRAINT `fk_diapositivas_tema_temas1`
    FOREIGN KEY (`id_tema`)
    REFERENCES `qbanodb`.`temas` (`id_tema`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_diapositivas_tema_temas1_idx` ON `qbanodb`.`diapositivas_tema` (`id_tema` ASC);


-- -----------------------------------------------------
-- Table `qbanodb`.`qboletin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qbanodb`.`qboletin` (
  `id_qboletin` INT NOT NULL AUTO_INCREMENT,
  `contenido` LONGTEXT NULL,
  `isPrincipal` TINYINT(1) NULL,
  `fecha_registro` DATE NULL,
  PRIMARY KEY (`id_qboletin`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `qbanodb`.`calificaciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qbanodb`.`calificaciones` (
  `id_calificacion` INT NOT NULL AUTO_INCREMENT,
  `id_evaluacion` INT NULL,
  `is_aprobada` TINYINT(1) NULL,
  `numero_intentos` INT NULL,
  `calificacion` DOUBLE NULL,
  `numero_aciertos` INT(11) NULL,
  PRIMARY KEY (`id_calificacion`),
  CONSTRAINT `fk_calificaciones_evaluaciones1`
    FOREIGN KEY (`id_evaluacion`)
    REFERENCES `qbanodb`.`evaluaciones` (`id_evaluacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_calificaciones_evaluaciones1_idx` ON `qbanodb`.`calificaciones` (`id_evaluacion` ASC);


-- -----------------------------------------------------
-- Table `qbanodb`.`repuestas_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qbanodb`.`repuestas_usuario` (
  `id_repuesta_usuario` INT NOT NULL AUTO_INCREMENT,
  `id_calificacion` INT NULL,
  `id_repuesta_pregunta` INT NULL,
  `id_pregunta` INT NULL,
  PRIMARY KEY (`id_repuesta_usuario`),
  CONSTRAINT `fk_calificaciones_has_repuestas_preguntas_calificaciones1`
    FOREIGN KEY (`id_calificacion`)
    REFERENCES `qbanodb`.`calificaciones` (`id_calificacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_calificaciones_has_repuestas_preguntas_repuestas_preguntas1`
    FOREIGN KEY (`id_repuesta_pregunta`)
    REFERENCES `qbanodb`.`repuestas_preguntas` (`id_repuesta_pregunta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_repuestas_usuario_preguntas1`
    FOREIGN KEY (`id_pregunta`)
    REFERENCES `qbanodb`.`preguntas` (`id_pregunta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_calificaciones_has_repuestas_preguntas_repuestas_pregunt_idx` ON `qbanodb`.`repuestas_usuario` (`id_repuesta_pregunta` ASC);

CREATE INDEX `fk_calificaciones_has_repuestas_preguntas_calificaciones1_idx` ON `qbanodb`.`repuestas_usuario` (`id_calificacion` ASC);

CREATE INDEX `fk_repuestas_usuario_preguntas1_idx` ON `qbanodb`.`repuestas_usuario` (`id_pregunta` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `qbanodb`.`tipo_cursos`
-- -----------------------------------------------------
START TRANSACTION;
USE `qbanodb`;
INSERT INTO `qbanodb`.`tipo_cursos` (`id_tipo_curso`, `nombre`) VALUES (1, 'express');
INSERT INTO `qbanodb`.`tipo_cursos` (`id_tipo_curso`, `nombre`) VALUES (2, 'unidad');

COMMIT;


-- -----------------------------------------------------
-- Data for table `qbanodb`.`tipo_usuarios_cursos`
-- -----------------------------------------------------
START TRANSACTION;
USE `qbanodb`;
INSERT INTO `qbanodb`.`tipo_usuarios_cursos` (`id_tipo_usuario_curso`, `nombre`) VALUES (1, 'jefe');
INSERT INTO `qbanodb`.`tipo_usuarios_cursos` (`id_tipo_usuario_curso`, `nombre`) VALUES (2, 'administrador');
INSERT INTO `qbanodb`.`tipo_usuarios_cursos` (`id_tipo_usuario_curso`, `nombre`) VALUES (3, 'entrenadores');
INSERT INTO `qbanodb`.`tipo_usuarios_cursos` (`id_tipo_usuario_curso`, `nombre`) VALUES (4, 'estudiante');

COMMIT;


-- -----------------------------------------------------
-- Data for table `qbanodb`.`tipo_preguntas`
-- -----------------------------------------------------
START TRANSACTION;
USE `qbanodb`;
INSERT INTO `qbanodb`.`tipo_preguntas` (`id_tipo_pregunta`, `nombre`) VALUES (1, 'falso/verdadero');
INSERT INTO `qbanodb`.`tipo_preguntas` (`id_tipo_pregunta`, `nombre`) VALUES (2, 'multiple');

COMMIT;

