<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<title></title>
<link type='text/css' rel='stylesheet' href='style.css' />
<?php 
	include_once('class/phpmydatagrid.class.php'); 
	include"conexion.php";

	echo set_DG_Header("js/","css/", " /");

	?>

<script type="text/javascript" language="javascript">
	function retirar(id_franquiciado,id_empleado,pdv,estado){
	if (estado=="1") return;	// if inactive do nothing // si esta inactivo no haga nada.
	if (confirm('Desea Ingresar el Retiro del empleado seleccionado? Recuerde que al ingresar este retiro el empleado sera inactivo de las plataformas Red Qbano y E-learning'))
		location.href='retiro_empleados.php?id_franquiciado='+id_franquiciado+"&id_empleado="+id_empleado+"&pdv="+pdv;
	}
</script> 

<script type="text/javascript" language="javascript">
	function editar(id_franquiciado,id_empleado,pdv,estado){
	if (estado=="1") return;	// if inactive do nothing // si esta inactivo no haga nada.
	if (confirm('Al empleado se le retiraran sus permisos para estudiar en la plataforma escuela de expertos Qbano'))
		location.href='retiro_empleados_escuela.php?id_franquiciado='+id_franquiciado+"&id_empleado="+id_empleado+"&pdv="+pdv;
	}
</script> 

<style type="text/css">
	.miss_date{
		background:#FF0000;
		border-bottom:3px solid #000000;
		border-right:3px solid #000000;
		font-weight:bold;
	}

	.bold_date{
		font-weight:bold;
	}
</style>

    	<link href="css/bootstrap2.min.css" rel="stylesheet">

<script language="JavaScript"> 

    function openNewWindow(url,h,w,UrlVar,NameControlOValorVar, NameWindow){//La Variable UrlVar debe contener el signo "?" antes del nombre y el signo "=" despues, ej: UrlVar=?CodProd=, para asi armar el parametro a enviar por la url 
    if (UrlVar!='') { 
    url=url + UrlVar + NameControlOValorVar; 
    } 
     
    var l = (screen.width - w) / 2; 
    var t = (screen.height - h) / 2; 
    var Wnd; 
    // 
    Wnd=open(url,NameWindow, "top=" + t + ",left=" + l + ", width=" + w + ", height=" + h + " , status=no,toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=no,copyhistory=no,directories=no"); 
    if (parseInt(navigator.appVersion) >= 4) win.window.focus(); 
    } 

</script> 

<style type="text/css">
    #formFiltro { padding:0px; }
    #formFiltro label{ margin-left:10px; display:block; width:80px; float:left; }
    #formFiltro input{ margin-right:10px; float: left; width:130px; }
    
</style>
<script type="text/javascript" language="javascript">
    /* function to set new filter */
    function setFilter(){
		parameters = "&cedula=" + DG_gvv('cedula') +
			"&p_apellido=" + DG_gvv('p_apellido') +
					"&pdv=" + DG_gvv('pdv') +

						 "&top_search=1";    // This variable MUST be included to success
        DG_Do('filter', parameters);
    }

    function resetFilter(){
		DG_svv('cedula', '');
		DG_svv('pdv', '');
		DG_svv('p_apellido', '');
		DG_Do('filter', "&top_search=1");
        DG_Slide("formFiltro",{duration:.2}).swap()
    }
    
    function activateSearchBox(){
        curDisplay = document.getElementById('formFiltro').style.display;
        DG_Slide("formFiltro",{duration:.2}).swap()
        if (curDisplay=='none') document.getElementById('cedula').focus();
    }
</script> 

<link href="qbano/Views/templates/css/bootstrap2.min.css" rel="stylesheet">
	<link href="qbano/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="qbano/Views/templates/css/light-bootstrap-dashboard_2.css" rel="stylesheet" type="text/css">
    <link href="css/menu.css" rel="stylesheet">

</head>

<body style="background-image:url(images/qbano_fondo.jpg)">
	
	<div class="main-panel">
        		<div class="content">
            		<div class="container-fluid">
                <div class="row">

	<table border="0" id="bg">
	  <tr>
		<td id="content">
			<div id='dg'> 

                <div class="dgTable" style="width: 100%;">
                    <form class="dgToolbar" method="post" name="formFiltro" id="formFiltro" style="display: none; height: 80px;">
                    <table style="margin-top: 10px; width: 100%;">
                        <tr>
							<td style="width: 270px;"><label>Cedula</label><input id="cedula" name="cedula" type="text" /></td>
                            <td style="width: 270px;"><label>Apellido</label><input id="p_apellido" name="p_apellido" type="text" /></td>
							<td><label>PDV</label><?php
                                    $tmpObj = new datagrid();
                                	$tmpObj -> conectadb($local,$usu,$pass,$bd);
                                    $strSQL = "select id_pdv,pdv from puntos_de_venta order by pdv";
                                    $arrData = $tmpObj -> SQL_query($strSQL);
                                    echo "<select id='pdv' name='pdv' style='width:150px'>";
                                    echo "<option value=''>Seleccione</option>";
                                    foreach ($arrData as $pdv){
                                        echo "<option value='" . $pdv['id_pdv'] . "'>" .utf8_encode($pdv['pdv']) . "</option>";
                                    }
                                    echo "</select>";
                                    unset($tmpObj);
                                ?> </td>
							<td>&nbsp;</td>
						    <td>&nbsp;</td>
                            <td>&nbsp;</td> 
						</tr>
                        <tr>
						  <td style="width: 270px;"><label><a class="fbutton" href="javascript:setFilter()"><span class="buttonImage"><img src="images/search.gif" alt="Search" title="Search" class="dgImgLink" border="0" /></span></a>
                                <a class="fbutton" href="javascript:resetFilter()"><span class="buttonImage"><img src="images/cancel_search.gif" alt="Search" title="Search" class="dgImgLink" border="0" /></span></a></label>
                           </td>
						 <td >&nbsp;</td>
                         <td >&nbsp;</td>
						 <td>&nbsp;</td>
	                     <td>&nbsp;</td>
                         <td>&nbsp;</td>
                        </tr>
                    </table>
                    </form>
                </div>
            	<?php include_once("personal_all.php"); ?>
			</div>
	<B><I>Nota: El personal que se encuentra en esta pantalla se debe administrar desde Red Qbano.</I></B>			
            </td>
	  </tr>
	</table>
	<div id="chartHours" class="ct-chart"></div>
                                <div class="footer">
                                    
                                    <hr>
                                    <div class="stats">
                                    </div>
                                </div>
                            </div>
                             </div>
                    </div>

</body>
</html>