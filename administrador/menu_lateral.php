<?php
    require_once '../utilidades.php';
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Qbano - Escuela de Expertos</title>

    <!-- Bootstrap core CSS     -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Table core CSS    -->
    <link href="../assets/css/light-bootstrap-dashboard_2.css" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="../assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div >
    <div class="sidebar" data-color="green" data-image="../assets/img/sidebar-5.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="<?php echo $URLREDQBANO ?>qbano/qboletin/ver" target="mainFrame"><img src="../img/escueladeexpertos.png"></a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="<?php echo $URLREDQBANO ?>qbano/administrativo" target="mainFrame">
                        <i class="pe-7s-graph"></i>
                        <p>Administrador Cursos</p>
                    </a>
                </li>
                <li>
                    <a href="../oferta.php" target="mainFrame">
                        <i class="pe-7s-user"></i>
                        <p>Oferta Cursos</p>
                    </a>
                </li>
                <li>
                    <a href="../presenciales.php" target="mainFrame">
                        <i class="pe-7s-note2"></i>
                        <p>Inscripci&oacute;n Cursos Presenciales</p>
                    </a>
                </li>
                <li>
                    <a href="../control_matriculas.php" target="mainFrame">
                        <i class="pe-7s-news-paper"></i>
                        <p>Control Matriculas</p>
                    </a>
                </li>
				<li>
                    <a href="../control_usuarios.php" target="mainFrame">
                        <i class="pe-7s-map-marker"></i>
                        <p>Control Usuarios</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $URLREDQBANO ?>qbano/qboletin/dashboard" target="mainFrame">
                        <i class="pe-7s-science"></i>
                        <p>Administrador Qboletin</p>
                    </a>
                </li>
                <li>
                    <a href="../personal.php" target="mainFrame">
                        <i class="pe-7s-bell"></i>
                        <p>Personal a Capacitar</p>
                    </a>
                </li>
				<li class="active-pro">
                    &nbsp;
                </li>
            </ul>
    	</div>
    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="../assets/js/jquery-1.10.2.js" type="text/javascript"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="../assets/js/light-bootstrap-dashboard.js"></script>



</html>
