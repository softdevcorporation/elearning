<?php
	@session_start();
	require_once('class/phpmydatagrid.class.php');
	include"conexion.php";
	@$_SESSION['idFranquiciado'];
	require_once('class/phpmydatagrid.class.php');
	include"conexion.php";

	$objGrid = new datagrid('control_usuarios_franq.php','1');
	
	$objGrid-> conectadb($local,$usu,$pass,$bd);
	$objGrid -> language('es');
	
	$objGrid -> setHeader('control_usuarios_franq.php', 'js/dgscripts.js', 'css/dgstyle.css', 'js/dgcalendar.js', 'css/dgcalendar.css',  'mmscripts.js');

	$objGrid-> tabla ("matricula");
	
	$objGrid-> orderby("fecha_matricula","asc");
	
	$objGrid-> keyfield ("id_empleado");
		
	$objGrid-> searchby("id_curso");

	/*********************************** Set filter based on requested parameters ***********************************/
	/********************************* Crear el filtro basandose en los parametros **********************************/
	$p_apellido = getvar("p_apellido", "p_apellidoajax");
	$pdv = getvar("pdv", "pdvajax");
	$id_curso = getvar("id_curso", "id_cursoajax");


	$conditions = array();

	if (!empty($pdv)) $conditions[] = sprintf(" (pdv = %s)", $objGrid -> magic_quote($pdv));
	if (!empty($p_apellido)) $conditions[] = sprintf(" (p_apellido = %s)", $objGrid -> magic_quote($p_apellido));
	if (!empty($id_curso)) $conditions[] = sprintf(" (id_curso = %s)", $objGrid -> magic_quote($id_curso));

	$conditions[] = ("e.id_franquiciado =".$_SESSION['idFranquiciado']);

	$condition = implode(" AND ", $conditions);

	$objGrid -> sqlstatement('select m.id_empleado,m.id_matricula,m.id_curso,m.carg_proceso,m.proceso,m.fecha_matricula,m.presencial,e.nombres,e.p_apellido,e.pdv,e.id_franquiciado,c.id_calificacion,c.id_evaluacion,c.is_aprobada,c.numero_intentos,c.calificacion,c.numero_aciertos,c.id_usuario from matricula m inner join empleados e on m.id_empleado=e.id_empleado inner join calificaciones c on m.id_empleado=c.id_usuario','select count(*) from matricula');

	$objGrid -> where($condition);
	
	$objGrid -> linkparam("&pdvajax={$pdv}&p_apellidoajax={$p_apellido}&id_cursoajax={$id_curso}");
	
	/****************************** Fin del ejemplo *********************************/
    /********************************* End sample ***********************************/

    $objGrid -> srconClic = "activateSearchBox()";  

	$objGrid-> buttons(FALSE,FALSE,false,FALSE,0);

	$objGrid -> nowindow = true;

	$objGrid -> ButtonWidth = '25';
	
	$objGrid -> toolbar = true;
     
	$objGrid-> datarows(20);

	$objGrid -> TituloGrid("Control de Notas y Avances del Estudiante");

	$objGrid -> FormatColumn("id_matricula", "id", 0, 20, 2, "150");
	$objGrid -> FormatColumn("id_empleado", "id", 0, 20 , 2, "150");
	$objGrid -> FormatColumn("id_curso", "Curso", 0, 20, 1, "220", "left","select:select id_curso, nombre from cursos");
	$objGrid -> FormatColumn("nombres", "Nombres", 0, 50, 1, "150", "left");
	$objGrid -> FormatColumn("p_apellido", "Primer Apellido", 0, 30, 1, "150", "left");
	$objGrid -> FormatColumn("fecha_matricula", "Fecha de Matricula", 0, 20, 1, "150", "center");
	$objGrid -> FormatColumn("pdv", "PDV", 0, 20, 1, "150", "left","select:select id_pdv, pdv from puntos_de_venta");
	$objGrid -> FormatColumn("id_evaluacion", "Evaluacion", 0, 50, 1, "150", "left");
	$objGrid -> FormatColumn("is_aprobada", "Aprobado", 0, 50, 1, "150", "left");
	$objGrid -> FormatColumn("numero_intentos", "Intentos", 0, 50, 1, "150", "left");
	$objGrid -> FormatColumn("calificacion", "Calificacion Final", 0, 50, 1, "150", "left");
	$objGrid -> FormatColumn("numero_aciertos", "Cant. Aciertos", 0, 50, 1, "150", "left");
	$objGrid -> FormatColumn("id_usuario", "id_estudiante", 0, 50, 2, "150", "left");

	$objGrid-> Form('control_usuarios_franq', true);
	
	$objGrid-> paginationmode ("links");
	
	$objGrid-> toolbar = true;
	
	$objGrid-> reload = true;
	
	$objGrid-> ajax('silent');
	
	$objGrid-> grid();

	function getvar($var1, $var2, $default=""){
		if ((isset($_GET['top_search'])?$_GET['top_search']:(isset($_POST['top_search'])?$_POST['top_search']:$default))==1){
			return addslashes((isset($_POST[$var1]) and !empty($_POST[$var1]))?$_POST[$var1]:((isset($_GET[$var1]) and !empty($_GET[$var1]))?$_GET[$var1]:$default));
		}else{
			return addslashes((isset($_POST[$var2]) and !empty($_POST[$var2]))?$_POST[$var2]:((isset($_GET[$var2]) and !empty($_GET[$var2]))?$_GET[$var2]:$default));
		}
	}
?>