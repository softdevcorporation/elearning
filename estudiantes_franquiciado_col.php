<?php
	@session_start();
	require_once('class/phpmydatagrid.class.php');
	include"conexion.php";
	@$_SESSION['idFranquiciado'];
	$objGrid = new datagrid('estudiantes_col.php','1');
	
	$objGrid-> conectadb($local,$usu,$pass,$bd);
	$objGrid -> language('es');
	
	$objGrid -> setHeader('estudiantes_franquiciado_col.php', 'js/dgscripts.js', 'css/dgstyle.css', 'js/dgcalendar.js', 'css/dgcalendar.css',  'mmscripts.js');

	$objGrid-> tabla ("matricula");
	
	$objGrid-> orderby("fecha_matricula","asc");
	
	$objGrid-> keyfield ("id_empleado");
		
	$objGrid-> searchby("id_curso");

	/*********************************** Set filter based on requested parameters ***********************************/
	/********************************* Crear el filtro basandose en los parametros **********************************/
	
	$p_apellido = getvar("p_apellido", "p_apellidoajax");
	$pdv = getvar("pdv", "pdvajax");

	$conditions = array();

	if (!empty($pdv)) $conditions[] = sprintf(" (pdv = %s)", $objGrid -> magic_quote($pdv));
	if (!empty($p_apellido)) $conditions[] = sprintf(" (p_apellido = %s)", $objGrid -> magic_quote($p_apellido));


	$conditions[] = ("presencial = 0");
	$conditions[] = ("carg_proceso = 0");
	$conditions[] = ("f.id_franquiciado =".$_SESSION['idFranquiciado']);

	$condition = implode(" AND ", $conditions);

	$objGrid -> sqlstatement('select m.id_empleado,m.id_matricula,m.id_curso,m.proceso,m.fecha_matricula,m.presencial,e.nombres,e.p_apellido,e.pdv,e.id_franquiciado,e.id_franquiciado,f.id_franquiciado from matricula m inner join empleados e on  m.id_empleado=e.id_empleado inner join franquiciado f on  f.id_franquiciado=e.id_franquiciado','select count(*) from matricula where presencial=1');

	$objGrid -> where($condition);
	
	$objGrid -> linkparam("&pdvajax={$pdv}&p_apellidoajax={$p_apellido}");
	
	/****************************** Fin del ejemplo *********************************/
    /********************************* End sample ***********************************/

    $objGrid -> srconClic = "activateSearchBox()";  

	$objGrid-> buttons(FALSE,FALSE,false,FALSE,0);

	$objGrid -> nowindow = true;

	$objGrid -> ButtonWidth = '25';
	$objGrid -> toolbar = true;
     
	//$objGrid -> saveaddnew = true;
	
	$objGrid-> datarows(20);

	//$objGrid -> height = '366px';

	$objGrid -> TituloGrid("Estudiantes Matriculados");

	$objGrid -> FormatColumn("id_matricula", "id", 0, 20, 2, "150");
	$objGrid -> FormatColumn("id_empleado", "id", 0, 20 , 2, "150");
	$objGrid -> FormatColumn("id_curso", "Curso", 0, 20, 1, "150", "left","select:select id_curso, nombre from cursos");
	$objGrid -> FormatColumn("nombres", "Nombres", 0, 50, 1, "150", "left");
	$objGrid -> FormatColumn("p_apellido", "Primer Apellido", 0, 30, 1, "150", "left");
	$objGrid -> FormatColumn("fecha_matricula", "Fecha de Matricula", 0, 20, 1, "90", "left");
	$objGrid -> FormatColumn("pdv", "PDV", 0, 20, 1, "150", "left","select:select id_pdv, pdv from puntos_de_venta");

	$objGrid-> Form('estudiantes_franquiciado_col', true);
	
	$objGrid-> paginationmode ("links");
	
	$objGrid-> toolbar = true;
	
	$objGrid-> reload = true;
	
	$objGrid-> ajax('silent');
	
	$objGrid-> grid();

	function getvar($var1, $var2, $default=""){
		if ((isset($_GET['top_search'])?$_GET['top_search']:(isset($_POST['top_search'])?$_POST['top_search']:$default))==1){
			return addslashes((isset($_POST[$var1]) and !empty($_POST[$var1]))?$_POST[$var1]:((isset($_GET[$var1]) and !empty($_GET[$var1]))?$_GET[$var1]:$default));
		}else{
			return addslashes((isset($_POST[$var2]) and !empty($_POST[$var2]))?$_POST[$var2]:((isset($_GET[$var2]) and !empty($_GET[$var2]))?$_GET[$var2]:$default));
		}
	}
?>