<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<title></title>
<link type='text/css' rel='stylesheet' href='style.css' />
<?php 
	include_once('class/phpmydatagrid.class.php'); 
	include"conexion.php";

	echo set_DG_Header("js/","css/", " /");
?>

<script type="text/javascript" language="javascript">
	function proces(id_matricula,id_empleado,carg_proceso,id_curso){
	if (carg_proceso==1) return;	// if inactive do nothing // si esta inactivo no haga nada.
	//if (confirm('Desea cancela el servicio? Recuerde que este no se visualizara al momento de cancelarlo! '))
		location.href='ingreso_eva_encuesta.php?id_empleado='+id_empleado+"&id_curso="+id_curso;
	}
</script> 
<script language="JavaScript"> 

    function openNewWindow(url,h,w,UrlVar,NameControlOValorVar, NameWindow){//La Variable UrlVar debe contener el signo "?" antes del nombre y el signo "=" despues, ej: UrlVar=?CodProd=, para asi armar el parametro a enviar por la url 
    if (UrlVar!='') { 
    url=url + UrlVar + NameControlOValorVar; 
    } 
     
    var l = (screen.width - w) / 2; 
    var t = (screen.height - h) / 2; 
    var Wnd; 
    // 
    Wnd=open(url,NameWindow, "top=" + t + ",left=" + l + ", width=" + w + ", height=" + h + " , status=no,toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=no,copyhistory=no,directories=no"); 
    if (parseInt(navigator.appVersion) >= 4) win.window.focus(); 
    } 

</script> 

<style type="text/css">
    #formFiltro { padding:0px; }
    #formFiltro label{ margin-left:10px; display:block; width:80px; float:left; }
    #formFiltro input{ margin-right:10px; float: left; width:130px; }
    
</style>
<script type="text/javascript" language="javascript">
    /* function to set new filter */
    function setFilter(){
		parameters = "&pdv=" + DG_gvv('pdv') +
			"&p_apellido=" + DG_gvv('p_apellido') +
		"&id_curso=" + DG_gvv('id_curso') +

						 "&top_search=1";    // This variable MUST be included to success
        DG_Do('filter', parameters);
    }

    function resetFilter(){
		DG_svv('pdv', '');
		DG_svv('p_apellido', '');
		DG_svv('id_curso', '');
		DG_Do('filter', "&top_search=1");
        DG_Slide("formFiltro",{duration:.2}).swap()
    }
    
    function activateSearchBox(){
        curDisplay = document.getElementById('formFiltro').style.display;
        DG_Slide("formFiltro",{duration:.2}).swap()
        if (curDisplay=='none') document.getElementById('p_apellido').focus();
    }
</script>   
<link href="qbano/Views/templates/css/bootstrap2.min.css" rel="stylesheet">
	<link href="qbano/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="qbano/Views/templates/css/light-bootstrap-dashboard_2.css" rel="stylesheet" type="text/css">
    <link href="css/menu.css" rel="stylesheet">

</head>

<body>
	<div class="main-panel">
        		<div class="content">
            		<div class="container-fluid">
                <div class="row">


	<table border="0" id="bg">
	  <tr>
		<td id="content">
			<div id='dg'> 

                <div class="dgTable" style="width: 100%;">
                    <form class="dgToolbar" method="post" name="formFiltro" id="formFiltro" style="display: none; height: 80px;">
                    <table style="margin-top: 10px; width: 100%;">
                        <tr>
							<td style="width: 270px;"><label>Apellido</label><input id="p_apellido" name="p_apellido" type="text" /></td>
                            <td style="width: 270px;"><label>PDV</label><?php
                                    $tmpObj = new datagrid();
                                	$tmpObj -> conectadb($local,$usu,$pass,$bd);
                                    $strSQL = "select id_pdv,pdv from puntos_de_venta order by pdv";
                                    $arrData = $tmpObj -> SQL_query($strSQL);
                                    echo "<select id='pdv' name='pdv' style='width:150px'>";
                                    echo "<option value=''>Seleccione</option>";
                                    foreach ($arrData as $pdv){
                                        echo "<option value='" . $pdv['id_pdv'] . "'>" .utf8_encode($pdv['pdv']) . "</option>";
                                    }
                                    echo "</select>";
                                    unset($tmpObj);
                                ?> 
							</td>
							<td><label>Cursos</label><?php
                                    $tmpObj = new datagrid();
                                	$tmpObj -> conectadb($local,$usu,$pass,$bd);
                                    $strSQL = "select id_curso,nombre from cursos order by nombre";
                                    $arrData = $tmpObj -> SQL_query($strSQL);
                                    echo "<select id='id_curso' name='id_curso' style='width:150px'>";
                                    echo "<option value=''>Seleccione</option>";
                                    foreach ($arrData as $id_curso){
                                        echo "<option value='" . $id_curso['id_curso'] . "'>" .utf8_encode($id_curso['nombre']) . "</option>";
                                    }
                                    echo "</select>";
                                    unset($tmpObj);
                                ?> </td>
							<td>&nbsp;</td>
						    <td>&nbsp;</td>
                            <td>&nbsp;</td> 
						</tr>
                        <tr>
						  <td style="width: 270px;"><label><a class="fbutton" href="javascript:setFilter()"><span class="buttonImage"><img src="images/search.gif" alt="Search" title="Search" class="dgImgLink" border="0" /></span></a>
                                <a class="fbutton" href="javascript:resetFilter()"><span class="buttonImage"><img src="images/cancel_search.gif" alt="Search" title="Search" class="dgImgLink" border="0" /></span></a></label>
                           </td>
						 <td >&nbsp;</td>
                         <td >&nbsp;</td>
						 <td>&nbsp;</td>
	                     <td>&nbsp;</td>
                         <td>&nbsp;</td>
                        </tr>
                    </table>
                    </form>
                </div>
            	<?php include_once("estudiantes_col.php"); ?>
			</div>
			
            </td>
	  </tr>
	</table>

	 <div id="chartHours" class="ct-chart"></div>
                                <div class="footer">
                                    
                                    <hr>
                                    <div class="stats">
                                    </div>
                                </div>
                            </div>
                             </div>
                    </div>
</body>
</html>